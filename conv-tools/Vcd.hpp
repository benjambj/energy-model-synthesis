#pragma once

#include <chrono>
#include <cstdint>
#include <fstream>
#include <map>
#include <memory>
#include <ratio>
#include <set>
#include <stack>
#include <string>
#include <vector>

class VcdSignal
{
 public:
  explicit VcdSignal(
      const std::string &name,
      const std::string &abbrv,
      const uint32_t width)
      : Name{name},
        Abbreviation{abbrv},
        Width(width) { /* empty */ }

  std::string Name, Abbreviation;
  uint32_t Width;
};

bool operator<(const VcdSignal &s1, const VcdSignal &s2);

class VcdModule;
typedef std::shared_ptr<VcdModule> VcdModulePtr;

class VcdModule
{
 public:
  explicit VcdModule(const std::string &name) : Name{name} { /* empty */ }
  std::string Name;
  void AddChild(VcdModulePtr child) {
    Children.push_back(child);
  }
  void AddSignal(VcdSignal signal) {
    Signals.push_back(signal);
  }

  std::vector<VcdModulePtr> Children;
  std::vector<VcdSignal> Signals;
};


class VcdHierarchy
{
 public:
  explicit VcdHierarchy(VcdModulePtr root)
      : Root{root} {
    scopeStack_.push(Root);
  }

  VcdModulePtr Root;
  void DescendScope(const std::string &moduleName) {
    scopeStack_.push(std::make_shared<VcdModule>(moduleName));
  }
  void AscendScope() {
    VcdModulePtr completedModule = scopeStack_.top();
    scopeStack_.pop();
    scopeStack_.top()->AddChild(completedModule);
  }
  void AddSignal(
      const std::string &name,
      const std::string &abbrv,
      uint32_t width) {
    scopeStack_.top()->AddSignal(VcdSignal{name, abbrv, width});
  }
  
  std::vector<VcdModulePtr> FindModulesByName(
      const std::string &moduleName) const;

  VcdModulePtr FindModuleByAbsoluteName(
      const std::string &moduleName) const;

  std::vector<VcdSignal> FindSignalsByName(
      const std::string &moduleName,
      const std::string &signalName) const;

 private:
  std::stack<VcdModulePtr> scopeStack_;
};

typedef std::chrono::duration<double, std::femto> femtoseconds;

enum class SignalKind
{
  Scalar, Bus, Real
};

struct SignalValue {
  SignalKind kind;
  union {
      char scalar;
      std::string bus;
      float real;
  };
  explicit SignalValue(char val)
      : kind{SignalKind::Scalar}, scalar{val} { /* empty */ }
  explicit SignalValue(const std::string &val)
      : kind{SignalKind::Bus}, bus{val} { /* empty */ }
  explicit SignalValue(float val)
      : kind{SignalKind::Real}, real{val} { /* empty */ }

  SignalValue(const SignalValue &other)
  {
    kind = other.kind;
    switch (other.kind) 
    {
      case SignalKind::Scalar:
        scalar = other.scalar;
        break;
      case SignalKind::Bus:
        //The field 'bus' is not even default-initialized since it is in a
        //union, so we must use placement new.
        new (&bus) std::string { other.bus };
        break;
      case SignalKind::Real:
        real = other.real;
        break;
    }
  }

  ~SignalValue()
  {
    if (kind == SignalKind::Bus)
      bus.~basic_string<char>();
  }
  
  SignalValue& operator=(const SignalValue &other)
  {
    switch (other.kind) 
    {
      case SignalKind::Scalar:
        if (this->kind == SignalKind::Bus)
          bus.~basic_string<char>();
        scalar = other.scalar;
        break;
      case SignalKind::Bus:
        if (this->kind == SignalKind::Bus)
          bus = other.bus;
        else
          new (&bus) std::string { other.bus };
        break;
      case SignalKind::Real:
        if (this->kind == SignalKind::Bus)
          bus.~basic_string<char>();
        real = other.real;
        break;
    }
    kind = other.kind;
    return *this;
  }
};

enum class ChangeKind
{
  Time, Signal, End
};

struct Change
{
  ChangeKind kind;
  union {
    struct {
      femtoseconds value;
    } time;
    struct {
      std::string abbreviation;
      SignalValue value;
    } signal;
  };

  explicit Change() : kind{ChangeKind::End} { /* empty */ }
  explicit Change(femtoseconds val)
      : kind{ChangeKind::Time}, time{val} { /* empty */ }
  explicit Change(const std::string abbrev, const SignalValue &value)
      : kind{ChangeKind::Signal},
        signal{abbrev, value} { /* empty */ }
    
 private:
  void MaybeDeleteTime() {
    if (kind == ChangeKind::Time)
    {
      time.value.~duration<double, std::femto>();
    }
  }
  void MaybeDeleteSignal() {
    if (kind == ChangeKind::Signal)
    {
      signal.abbreviation.~basic_string<char>();
      signal.value.~SignalValue();
    }
  }

  //Use placement new, since the targets for assignment may not exist (either
  //because of destruction using MaybeDelete*, or because the variables have
  //never existed since default-initialization does not happen for variables in
  //unions)

 public:
  Change(const Change& other)
  {
    kind = other.kind;
    if (other.kind == ChangeKind::Time)
    {
      new (&time.value) femtoseconds { other.time.value };
    }
    else if (other.kind == ChangeKind::Signal)
    {
      new (&signal.abbreviation) std::string { other.signal.abbreviation };
      new (&signal.value) SignalValue { other.signal.value };
    }
  }
  ~Change()
  {
    MaybeDeleteTime();
    MaybeDeleteSignal();
  }

  Change& operator=(const Change &other) {
    switch (other.kind) 
    {
      case ChangeKind::Time:
        MaybeDeleteSignal();
        if (kind == ChangeKind::Time)
          time.value = other.time.value;
        else
          new (&time.value) femtoseconds { other.time.value };
        break;
      case ChangeKind::Signal:
        MaybeDeleteTime();

        if (kind == ChangeKind::Signal)
        {
          //Use assignment instead of placement new when the variable exists to
          //make sure the old variable value is cleaned up.
          signal = other.signal;
        }
        else
        {
          //Placement new to use copy constructor: assignment operators will not
          //work since the assignment targets do not exist when the current
          //Change is not a Signal.
          new (&signal.abbreviation) std::string { other.signal.abbreviation }; 
          new (&signal.value) SignalValue { other.signal.value };
        }
        break;
      case ChangeKind::End:
        MaybeDeleteTime();
        MaybeDeleteSignal();
        break;
    }
    kind = other.kind;
    return *this;
  }
};

class VcdFilter
{
 public:
  std::vector<VcdSignal> Signals;

  void AddSignal(const VcdSignal &signal) {
    Signals.push_back(signal);
    acceptedAbbrevs_.insert(signal.Abbreviation);
  }

  bool Accepts(const std::string &abbrv) const {
    return acceptedAbbrevs_.find(abbrv) != acceptedAbbrevs_.end();
  }

  bool Empty() const { return acceptedAbbrevs_.empty(); }
 private:
  std::set<std::string> acceptedAbbrevs_;
};

class SignalValues
{
 public:
  explicit SignalValues()
      : signalIndices_{},
        signalNames_{},
        signalValues_{} { /* empty */ }
  explicit SignalValues(const VcdFilter &filter);
  void ApplyChange(const Change &change);
  std::vector<std::string> GetSignalNames() const {
    return signalNames_;
  }
  std::vector<SignalValue> GetSignalValues() const {
    return signalValues_;
  }
 private:
  std::map<std::string, std::size_t> signalIndices_;
  std::vector<std::string> signalNames_;
  std::vector<SignalValue> signalValues_;
};

class VcdFile
{
 public:
  explicit VcdFile(const std::string &filename);

  femtoseconds Timescale;
  VcdHierarchy Hierarchy;

  void GetInitialValues(
      const VcdFilter &filter,
      femtoseconds &startTime,
      SignalValues &values,
      femtoseconds timeInterval);
  Change GetNextChange();
 private:
  std::ifstream in_;

  void ParseDefinitions();
  void ReadNextDirective(
      std::string &directive,
      std::vector<std::string> &words);

  void HandleTimescale(const std::string &value);
  void HandleScope(const std::string &moduleName) {
    Hierarchy.DescendScope(moduleName);
  }
  void HandleUpscope() { Hierarchy.AscendScope(); }
  void HandleSignal(
      const std::string &,
      const uint32_t width,
      const std::string &abbreviation,
      const std::string &name) {
    Hierarchy.AddSignal(name, abbreviation, width);
  }

 private:
  Change storedChange_;
  bool hasStoredChange_;
};

class Vcd
{
 public:
  explicit Vcd(
      VcdFile &file,
      const VcdFilter &filter,
      femtoseconds timeInterval);
  std::vector<VcdSignal> GetSignals() const {
    return filter_.Signals;
  }
  bool GetNextSignalValues(SignalValues &values);

 private:
  VcdFile &vcdfile_;
  VcdFilter filter_;
  const femtoseconds timeInterval_;
  femtoseconds currentTime_;
  SignalValues currentSignalValues_;
  femtoseconds nextValueChangeTime_;
};

bool operator<(const VcdSignal &s1, const VcdSignal &s2);
std::ostream& operator<<(std::ostream& out, const SignalValue& value);

