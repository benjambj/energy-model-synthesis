. ../env-setup.sh

function get_datadir() {
    echo "$DATADIR/$1"
}

function get_resdir() {
    case $1 in
        "training")
            resdir="test-data/$1"
            echo "${resdir}"
            ;;
            "basicmath_small" | "bitcnts" | "qsort_small" | "dijkstra_small" | "search_large" | "sha" | \
            "fft_strided" | "fft" | "bulk" | "queue" | "aes" | "stencil3d" )
            resdir="validation-data/$1"
            if [ ! -d "${resdir}" ]; then
                mkdir "${resdir}";
            fi
            echo "${resdir}"
            ;;
        *)
            echo "Unknown benchmark $benchmark" >&2
            exit 1
    esac
}
