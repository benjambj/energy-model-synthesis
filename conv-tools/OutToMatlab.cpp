#include "MatlabFile.hpp"
#include "Out.hpp"
#include "OutSignalSelection.hpp"

#include <iostream>

OutFilter GetUserModuleSelection(const OutHierarchy &);

int main(int argc, char *argv[])
{
  if (argc != 3)
  {
    std::cerr << "Usage: " << argv[0] << " <OUT file> <Matlab output file>\n";
    std::exit(EXIT_FAILURE);
  }

  std::cout << "Parsing OUT header" << std::endl;
  OutFile outFile { argv[1] };

  auto outFilter = GetUserModuleSelection(outFile.Hierarchy);
  if (outFilter.Empty())
  {
    std::cout << "No modules selected, aborting" << std::endl;
    return 1;
  }

  OutData outData { outFile, outFilter };
  MatlabFile matlabFile { argv[2] };

  std::cout << "Writing matlab file..." << std::endl;
  matlabFile.WritePowerData(outData);

  return 0;
}
