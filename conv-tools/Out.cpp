#include "Out.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <stdexcept>

OutData::OutData(OutFile &file, const OutFilter &filter)
    : MaxIndex{file.MaxIndex},
      Modules{},
      file_(file),
      filter_{filter}
{
  for (const auto &module : file_.Modules)
    if (filter_.Accepts(module.Index))
      Modules.push_back(module);
}

bool OutData::GetNextSample(
    OutSample &sample,
    const std::map<unsigned, std::vector<unsigned>> &memoryInstances)
{
  auto gotSample = file_.GetNextSample(sample);
  while (gotSample &&
         !(filter_.Accepts(sample.Index) ||
           memoryInstances.find(sample.Index) !=
           end(memoryInstances)))
    gotSample = file_.GetNextSample(sample);
  return gotSample;
}

OutFile::OutFile(const std::string &filename)
    : MaxIndex{0},
      Modules{},
      Hierarchy{},
      in_{filename},
      currentTime_{0},
      hierarchySeparator_{'/'}
{
  if (in_.fail())
    throw std::runtime_error("Could not open OUT file " + filename);
  ParseHeader();
}

static inline bool IsComment(const std::string line)
{
  return line[0] == ';';
}

static inline bool IsDirective(const std::string line)
{
  return line[0] == '.';
}

void OutFile::ParseHeader()
{
  std::string nextLine;
  SkipComments(nextLine);
  while (nextLine.empty() || IsDirective(nextLine))
    if (!nextLine.empty())
    {
      ParseDirective(nextLine);
      getline(in_, nextLine);
    }

  ParseTimeChange(nextLine);
}

void OutFile::SkipComments(std::string &firstDirective)
{
  while (getline(in_, firstDirective))
    if (!firstDirective.empty() && !IsComment(firstDirective))
      return;
}

void OutFile::ParseDirective(const std::string &directive)
{
  std::istringstream iss{directive};
  std::string word;
  iss >> word;
  if (word == ".index")
  {
    AddModule(iss);
  }
  else if (word == ".hier_separator")
  {
    iss >> hierarchySeparator_;
  }
  else
  {
    std::cout << "Ignoring directive " << word << std::endl;
  }
}

void OutFile::AddModule(std::istringstream &iss)
{
  std::string moduleName, indexString;
  iss >> moduleName >> indexString;
  //module name format: Pc(<name>). Strip away Pc()
  moduleName = moduleName.substr(3, moduleName.size() - 4);
  unsigned index = static_cast<unsigned>(std::stoul(indexString));
  Modules.emplace_back(index, moduleName);

  auto path = SplitModulePath(moduleName);
  Hierarchy.AddModule(index, path);
  if (index > MaxIndex)
    MaxIndex = index;
}

std::vector<std::string> OutFile::SplitModulePath(
    const std::string &fullModuleName)
{
  std::vector<std::string> pathComponents;
  std::string::size_type currentIndex = 0, nextDelimiter;
  do {
    nextDelimiter = fullModuleName.find(hierarchySeparator_, currentIndex);
    pathComponents.push_back(
        fullModuleName.substr(currentIndex, nextDelimiter - currentIndex));
    currentIndex = nextDelimiter + 1;
  } while (nextDelimiter != std::string::npos);
  return pathComponents;
}

bool OutFile::GetNextSample(OutSample &sample)
{
  std::string line;
  while (getline(in_, line))
  {
    if (line.empty() || IsComment(line))
      continue;
    std::istringstream iss{line};
    std::string word1, word2;
    if (iss >> word1)
    {
      if (iss >> word2)
      {
        ParseValueChange(word1, word2, sample);
        return true;
      }
      ParseTimeChange(word1);
    }
  }
  return false;
}

void OutFile::ParseTimeChange(const std::string &time)
{
  currentTime_ = stoull(time);
}

void OutFile::ParseValueChange(const std::string &index,
                               const std::string &value,
                               OutSample &sample)
{
  sample.Index = static_cast<unsigned>(stoull(index));
  sample.Time = currentTime_;
  sample.Value = stod(value);
}

void OutHierarchy::AddModule(
    unsigned index,
    const std::vector<std::string> &modulePath)
{
  auto parent = find_if(begin(Roots), end(Roots), [&](OutModuleNodePtr m) {
      return m->ModuleName == modulePath.front();
    });
  if (parent == end(Roots))
    Roots.push_back(std::make_shared<OutModuleNode>(index, modulePath.front()));
  else
    (*parent)->AddChild(index, modulePath, 1);
}

OutModuleNode::OutModuleNode(
    unsigned index, const std::vector<std::string> &modulePath,
    std::size_t pathIndex)
    : Index{index},
      ModuleName{modulePath[pathIndex]},
      Children{}
{
  if (pathIndex < modulePath.size() - 1)
    AddChild(std::make_shared<OutModuleNode>(index, modulePath, pathIndex + 1));
}

void OutModuleNode::AddChild(
    unsigned index,
    const std::vector<std::string> &modulePath,
    std::size_t pathIndex)
{
  if (pathIndex == modulePath.size() - 1)
    AddChild(std::make_shared<OutModuleNode>(index, modulePath[pathIndex]));
  else
  {
    auto it = find_if(begin(Children), end(Children), [&](OutModuleNodePtr m) {
        return m->ModuleName == modulePath[pathIndex];
      });
    if (it == end(Children))
    {
      auto childTree =
          std::make_shared<OutModuleNode>(index, modulePath, pathIndex);
      Children.push_back(childTree);
    }
    else
    {
      (*it)->AddChild(index, modulePath, pathIndex + 1);
    }
  }
}
