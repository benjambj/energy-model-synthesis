#include "DataFrame.hpp"
#include "Vcd.hpp"

#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <stdexcept>

bool operator<(const VcdSignal &s1, const VcdSignal &s2)
{
  return s1.Abbreviation < s2.Abbreviation;
}

static char nibbleToHex(const std::string nibbleBinStr)
{
  assert(nibbleBinStr.size() <= 4);
  
  const char chars[] = {
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
  };

  size_t index = 0;
  for (size_t i = 0; i < nibbleBinStr.size(); ++i)
  {
    int num = nibbleBinStr[i] - '0';
    assert(num >= 0);
    index += static_cast<unsigned>(num) << (nibbleBinStr.size() - 1 - i);
  }
  return chars[index];
}

static std::string busHexValue(const std::string &binStr)
{
  std::string hexStr = "0x";
  unsigned bits = static_cast<unsigned>(binStr.size());
  assert(bits == binStr.size());
  unsigned index = 0;
  unsigned extraBits = bits % 4;
  if (extraBits != 0)
  {
    hexStr += nibbleToHex(binStr.substr(index, extraBits));
    index += extraBits;
  }
  for (; index < binStr.size(); index += 4)
  {
    hexStr += nibbleToHex(binStr.substr(index, 4));
  }
  return hexStr;
}

std::ostream& operator<<(std::ostream& out, const SignalValue& value)
{
  switch (value.kind) 
  {
    case SignalKind::Scalar:
      return out << value.scalar;
    case SignalKind::Bus:
      {
        if (any_of(begin(value.bus), end(value.bus),
                   [](const char ch) { return ch != '0' && ch != '1'; }))
          return out << "NA";
        return out << std::hex << busHexValue(value.bus) << std::dec;
      }
    case SignalKind::Real:
      return out << value.real;
  }
}

Vcd::Vcd(VcdFile &file, const VcdFilter &filter, femtoseconds timeInterval)
    : vcdfile_(file),
      filter_{filter},
      timeInterval_{timeInterval},
      currentTime_{0},
      currentSignalValues_{filter_},
      nextValueChangeTime_{0}
{
  vcdfile_.GetInitialValues(filter_, currentTime_,
                            currentSignalValues_, timeInterval);
}

bool Vcd::GetNextSignalValues(SignalValues &values)
{
  femtoseconds nextIntervalStart = currentTime_ + timeInterval_;
  // nextIntervalStart =
  //     std::floor(nextIntervalStart / timeInterval_) * timeInterval_;
  if (nextValueChangeTime_ > currentTime_)
  {
    //Status quo in the current interval, return current signal values and
    //advance time
    currentTime_ += timeInterval_;
    values = currentSignalValues_;
    return true;
  }

  auto change = vcdfile_.GetNextChange();
  if (change.kind == ChangeKind::End)
  {
    return false;
  }
  
  bool stillInCurrentInterval = true;
  while (stillInCurrentInterval)
  {
    while (change.kind == ChangeKind::Signal) 
    {
      if (filter_.Accepts(change.signal.abbreviation))
          currentSignalValues_.ApplyChange(change);

      change = vcdfile_.GetNextChange();
    }

    if (change.kind == ChangeKind::Time)
      nextValueChangeTime_ = change.time.value;
    
    stillInCurrentInterval =
        change.kind != ChangeKind::End &&
        nextValueChangeTime_ <= currentTime_;
    if (stillInCurrentInterval)
    {
      change = vcdfile_.GetNextChange();
    }
  }

  currentTime_ = nextIntervalStart;
  values = currentSignalValues_;
  return true;
}

SignalValues::SignalValues(const VcdFilter &filter)
    : signalIndices_{},
      signalNames_{filter.Signals.size()},
      signalValues_{filter.Signals.size(), SignalValue { 'x' }}
{
  for (std::size_t i = 0; i < signalNames_.size(); ++i)
  {
    signalNames_[i] = filter.Signals[i].Name;
    signalIndices_[filter.Signals[i].Abbreviation] = i;
  }
}

void SignalValues::ApplyChange(const Change &change)
{
  assert(change.kind == ChangeKind::Signal);
  std::size_t signalIndex = signalIndices_[change.signal.abbreviation];
  signalValues_[signalIndex] = change.signal.value;
}

VcdFile::VcdFile(const std::string &filename) :
    Timescale{},
    Hierarchy{std::make_shared<VcdModule>("/")},
    in_{filename},
    storedChange_{},
    hasStoredChange_{false}
{
  if (in_.fail())
  {
    throw std::runtime_error("Error opening VCD file " + filename);
  }
  ParseDefinitions();
}

void VcdFile::ParseDefinitions()
{
  bool inDefinitionSection = true;
  unsigned directives = 0;
  while (inDefinitionSection)
  {
    std::string directive;
    std::vector<std::string> words;
    ReadNextDirective(directive, words);
    if (directive == "$timescale")
    {
      assert(words.size() == 1);
      HandleTimescale(words[0]);
    }
    else if (directive == "$scope")
    {
      assert(words.size() == 2);
      assert(words[0] == "module");
      HandleScope(words[1]);
    }
    else if (directive == "$var")
    {
      assert(words.size() == 4 || words.size() == 5);
      auto bitsize = stoi(words[1]);
      assert(bitsize >= 0);
      HandleSignal(words[0], static_cast<uint32_t>(bitsize), words[2], words[3]);
    }
    else if (directive == "$upscope")
    {
      assert(words.size() == 0);
      HandleUpscope();
    }
    else if (directive == "$enddefinitions")
    {
      assert(in_.get() == '\n'); //Discard newline
      inDefinitionSection = false;
    }
    else if (directive == "$date")
    {
      std::cout << "VCD date: ";
      for (const auto &word : words)
        std::cout << word << " ";
      std::cout << '\n';
    }
    else if (directive == "$version" ||
             directive == "$comment")
    {
      std::cout << "Ignoring directive " << directive << "...\n";
    }
    else
    {
      throw std::runtime_error("Unknown directive: " + directive);
    }
    directives++;
    if (directives % 10000 == 0)
    {
      std::cout << "Handled " << directives << " directives" <<  std::endl;
    }
  }
}

void VcdFile::ReadNextDirective(
    std::string &directive,
    std::vector<std::string> &words)
{
  in_ >> directive;
  std::string word;
  in_ >> word;
  while (word != "$end") {
    words.push_back(word);
    in_ >> word;
  }
}

void VcdFile::HandleTimescale(const std::string &valueStr)
{
  std::size_t unitPos;
  unsigned long value = std::stoul(valueStr, &unitPos);
  auto unit = valueStr.substr(unitPos);

  if (unit == "s")
  {
    Timescale = std::chrono::duration_cast<femtoseconds>(
        std::chrono::seconds(value));
  }
  else if (unit == "ms")
  {
    Timescale = std::chrono::duration_cast<femtoseconds>(
        std::chrono::milliseconds(value));
  }
  else if (unit == "us")
  {
    Timescale = std::chrono::duration_cast<femtoseconds>(
        std::chrono::microseconds(value));
  }
  else if (unit == "ns")
  {
    Timescale = std::chrono::duration_cast<femtoseconds>(
        std::chrono::nanoseconds(value));
  }
  else if (unit == "ps")
  {
    Timescale = std::chrono::duration_cast<femtoseconds>(
        std::chrono::duration<unsigned long, std::pico>(value));
  }
  else if (unit == "fs")
  {
    Timescale = femtoseconds(static_cast<double>(value));
  }
  else
  {
    throw std::runtime_error("Unsupported timescale unit: " + unit);
  }
}

void VcdFile::GetInitialValues(
    const VcdFilter &filter,
    femtoseconds &startTime,
    SignalValues &values,
    femtoseconds timeInterval)
{
  while (std::isspace(in_.peek()))
    in_.get();
  auto hasStartTime = (in_.peek() == '#');
  if (hasStartTime)
  {
    auto change = GetNextChange();
    assert(change.kind == ChangeKind::Time);
    startTime = change.time.value;
  }

  std::string line;
  assert(getline(in_, line));
  assert(line == "$dumpvars");
  auto change = GetNextChange();
  while (change.kind != ChangeKind::End)
  {
    if (filter.Accepts(change.signal.abbreviation))
      values.ApplyChange(change);
    change = GetNextChange();
  }
  if (!hasStartTime)
  {
    storedChange_ = GetNextChange();
    hasStoredChange_ = true;
    assert(storedChange_.kind == ChangeKind::Time);
    startTime = storedChange_.time.value - timeInterval;
  }
}

Change VcdFile::GetNextChange()
{
  if (hasStoredChange_)
  {
    hasStoredChange_ = false;
    return storedChange_;
  }

  std::string line;

  do {
    getline(in_, line);
  } while (line.empty() && !in_.fail());
  if (in_.fail() || line == "$end")
  {
    return Change{};
  }

  switch (std::tolower(line[0])) 
  {
    case '#':
      {
        auto time = strtoull(&line.c_str()[1], NULL, 10);
        return Change { time * Timescale };
      }
    case 'b':
      {
        auto spacepos = line.find(' ');
        auto busValue = line.substr(1, spacepos - 1);
        auto abbreviation = line.substr(spacepos + 1);
        return Change { abbreviation, SignalValue { busValue } };
      }
    case 'r':
      {
        auto spacepos = line.find(' ');
        float real;
        auto argsAssigned = std::sscanf(line.data(), "%g", &real);
        assert(argsAssigned == 1);
        auto abbreviation = line.substr(spacepos + 1);
        return Change { abbreviation, SignalValue { real } };
      }
    default:
      return Change { line.substr(1), SignalValue { line[0] } };
  }
}

std::vector<VcdSignal> VcdHierarchy::FindSignalsByName(
    const std::string &moduleName,
    const std::string &signalName) const
{
  std::vector<VcdSignal> signals;
  std::stack<VcdModulePtr> moduleStack;
  moduleStack.push(Root);
  while (!moduleStack.empty())
  {
    auto module = moduleStack.top();
    moduleStack.pop();
    if (module->Name == moduleName)
    {
      for (const auto &signal : module->Signals)
        if (signal.Name == signalName)
          signals.push_back(signal);
    }
    for (const auto &child : module->Children)
      moduleStack.push(child);
  }
  return signals;
}

std::vector<VcdModulePtr> VcdHierarchy::FindModulesByName(
    const std::string &moduleName) const
{
  std::vector<VcdModulePtr> modules;
  std::stack<VcdModulePtr> moduleStack;
  moduleStack.push(Root);
  while (!moduleStack.empty())
  {
    auto module = moduleStack.top();
    moduleStack.pop();
    if (module->Name == moduleName)
    {
      modules.push_back(module);
    }
    for (const auto &child : module->Children)
      moduleStack.push(child);
  }
  return modules;
}

VcdModulePtr VcdHierarchy::FindModuleByAbsoluteName(
    const std::string &moduleName) const
{
  std::cout << "Searching for module " << moduleName << std::endl;
  auto curModStart = 0ul;
  auto curModEnd = moduleName.find('.');
  auto curModName =
      moduleName.substr(curModStart, static_cast<unsigned long>
                        (curModEnd - curModStart));
  auto module = Root;
  if (Root->Name == curModName) {
    curModStart =
        (curModEnd == std::string::npos ? curModEnd : curModEnd + 1);
    curModEnd = moduleName.find('.', curModStart);
  }
  while (curModStart != std::string::npos)
  {
    curModName =
        moduleName.substr(curModStart, static_cast<unsigned long>(
            curModEnd - curModStart));
    auto c = std::find_if(
        begin(module->Children), end(module->Children),
        [&](const VcdModulePtr &child){ return child->Name == curModName; });
    if (c == end(module->Children))
      throw std::runtime_error("Could not find child " + curModName +
                               " of module " + module->Name);
    module = *c;
    curModStart =
        (curModEnd == std::string::npos ? curModEnd : curModEnd + 1);
    curModEnd = moduleName.find('.', curModStart);
  }
  return module;
}
