#pragma once

#include <cassert>
#include <cstdlib>
#include <map>
#include <type_traits>
#ifndef NDEBUG
#include <iostream>
#endif
#include <menu.h>
#include <utility>

namespace ncop
{

template<typename T, typename R>
inline bool Success(R result)
{
  return result == E_OK;
}

template<typename T, typename R>
inline std::string Diagnose(R result)
{
  auto diagnosis =  T::Diagnosis.find(result);
  if (diagnosis == T::Diagnosis.end())
    return "Unknown error";
  return diagnosis->second;
}

template<typename T, typename U, typename... Args>
inline auto Assert(U success, Args&&... args) -> decltype(T::invoke(args...))
{
  auto result = T::invoke(std::forward<Args>(args)...);
#ifdef NDEBUG
  return result;
#else
  if (success(result))
    return result;

  nocbreak();
  endwin();

  std::cerr << "Error in operation " << T::OpName << ": "
            << Diagnose<T>(result) << std::endl;
  std::abort();
#endif
}

template<typename T, typename... Args>
inline auto AssertSuccess(Args&&... args) -> decltype(T::invoke(args...))
{
  return Assert<T>(Success<T, decltype(T::invoke(args...))>,
                   std::forward<Args>(args)...);

}

#define Operation(name, rettype, ...)                                   \
  struct name##Struct {                                                 \
    static rettype invoke __VA_ARGS__                                   \
    static const std::map<rettype, std::string> Diagnosis;              \
    constexpr const static char * const OpName = #name;                 \
  };                                                                    \
  template<typename... Args>                                            \
  rettype name(Args&& ...args) {                                        \
    return AssertSuccess<name##Struct>(std::forward<Args>(args)...);    \
  }                                                                     \
  template<typename U, typename... Args>                                \
  typename std::result_of<U(rettype)>::type                             \
  name(U success, Args&& ...args) {                                     \
    return Assert<name##Struct>(success, std::forward<Args>(args)...);  \
  }                                                                     \
  template<typename... Args>                                            \
  rettype Raw##name(Args&& ...args) {                                   \
    return name##Struct::invoke(std::forward<Args>(args)...);           \
  }                                                                     \
  const std::map<rettype, std::string> name##Struct::Diagnosis = 

#define OpSuccess(name, ...) \
  template<> \
  bool Success<name##Struct>__VA_ARGS__

#define OpDiagnosis(name, ...) \
  template<> \
  std::string Diagnose<name##Struct>__VA_ARGS__

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"
#pragma clang diagnostic ignored "-Wexit-time-destructors"

Operation(UnpostMenu,
          int, (MENU *menu) { return unpost_menu(menu); }) {
  { E_BAD_ARGUMENT, "Not a valid menu pointer" },
  { E_BAD_STATE, "Menu in userexit routine" },
  { E_NOT_POSTED, "Menu is not posted" }
};

Operation(PostMenu,
          int, (MENU *menu) { return post_menu(menu); }) {
  { E_BAD_ARGUMENT, "Invalid menu pointer" },
  { E_SYSTEM_ERROR, "Error in lower layer" },
  { E_NOT_CONNECTED, "No items connected to menu" },
  { E_BAD_STATE, "Menu in userexit routine" },
  { E_POSTED, "Menu is already posted" }
};
Operation(SetMenuItems,
          int,
          (MENU *menu, ITEM **items) { return set_menu_items(menu, items); }) {
  { E_POSTED, "Menu is already posted" },
  { E_CONNECTED, "One or more items are already connected to another menu." },
  { E_BAD_ARGUMENT, "An incorrect menu or item array was passed to the function" },
};
Operation(SetMenuOptions,
          int, (MENU *menu, Menu_Options options) {
            return set_menu_opts(menu, options);
          }) {
  { E_BAD_ARGUMENT, "Invalid menu options" },
  { E_POSTED, "Menu is already posted" },
};
Operation(MenuDriver,
          int,
          (MENU *menu, int request) { return menu_driver(menu, request); }) {
  { E_BAD_ARGUMENT, "Invalid menu pointer" },
  { E_BAD_STATE, "Menu is in user hook routine" },
  { E_NOT_POSTED, "Menu is not posted" },
  { E_SYSTEM_ERROR, "A system error occurred (see errno)" },
  { E_UNKNOWN_COMMAND, "An unknown request code was provided" },
  { E_NO_MATCH, "No matches found" },
  { E_REQUEST_DENIED, "The request was denied (impossible to perform)" }
};

Operation(CurrentItem,
          ITEM *, (MENU *menu) { return current_item(menu); }) {
  { nullptr, "No items are set in the menu" }
};
OpSuccess(CurrentItem,
          (ITEM *result) { return result != nullptr; })

Operation(ItemIndex,
          int, (ITEM *item) { return item_index(item); }) {
  { ERR, "Invalid item pointer" }
};
OpSuccess(ItemIndex,
          (int result) { return result != ERR; })

Operation(SetCurrentItem,
          int,
          (MENU *menu, ITEM *item) { return set_current_item(menu, item); }) {
  { E_BAD_STATE, "Cannot set item while menu is in the driver" },
  { E_BAD_ARGUMENT, "Invalid argument, perhaps item does not belong to menu?" }
};

Operation(SetMenuWin,
          int, (MENU *menu, WINDOW *win) { return set_menu_win(menu, win); }) {
  { E_POSTED, "Menu is already posted" }
};
Operation(SetMenuSub,
          int, (MENU *menu, WINDOW *sub) { return set_menu_sub(menu, sub); }) {
  { E_POSTED, "Menu is already posted" }
};
Operation(SetMenuFormat,
          int, (MENU *menu, int rows, int cols) {
            return set_menu_format(menu, rows, cols);
          }) {
  { E_BAD_ARGUMENT, "Invalid values passed" },
  { E_NOT_CONNECTED, "There are no items connected" },
  { E_POSTED, "The menu is already posted" }
};

Operation(SetMenuBack,
          int,
          (MENU *menu, chtype back) { return set_menu_back(menu, back); }) {
  { E_BAD_ARGUMENT, "Invalid colour pair provided" }
};
Operation(SetMenuFore,
          int,
          (MENU *menu, chtype back) { return set_menu_back(menu, back); }) {
  { E_BAD_ARGUMENT, "Invalid colour pair provided" }
};
Operation(SetMenuGrey,
          int,
          (MENU *menu, chtype back) { return set_menu_back(menu, back); }) {
  { E_BAD_ARGUMENT, "Invalid colour pair provided" }
};

Operation(SetItemUserptr,
          int,
          (ITEM *item, void *ptr) { return set_item_userptr(item, ptr); }) {
};
Operation(GetItemUserptr,
          void *, (ITEM *item) { return item_userptr(item); }) {
};
OpSuccess(GetItemUserptr,
          (void *) { return true; });


Operation(PosMenuCursor,
          int, (MENU *menu) { return pos_menu_cursor(menu); }) {
  { E_BAD_ARGUMENT, "Invalid menu argument" },
  { E_NOT_POSTED, "The menu is not posted" },
  { E_SYSTEM_ERROR, "System error occurred (see errno)" }
};

Operation(SetItemValue,
          int,
          (ITEM *item, bool value) { return set_item_value(item, value); }) {
  { E_SYSTEM_ERROR, "A system error occurred (see errno)" },
  { E_REQUEST_DENIED,
        "Request denied (item is disabled or belongs to a single-value menu)" }
};

#pragma clang diagnostic pop

} //end namespace ncop
