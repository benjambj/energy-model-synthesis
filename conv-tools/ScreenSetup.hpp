struct ScreenSetup
{
  ScreenSetup()
  {
    initscr();
    start_color();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_BLACK, COLOR_BLACK);
  }
      
  ~ScreenSetup()
  {
    keypad(stdscr, FALSE);
    echo();
    nocbreak();
    endwin();
  }
};

