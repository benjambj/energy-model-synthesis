#include "MatlabFile.hpp"

#include <algorithm>

MatlabFile::MatlabFile(const std::string &filename)
    : out_{filename}
{
  if (out_.fail())
    throw std::runtime_error("Error opening matlab file " + filename);
}

void MatlabFile::WritePowerData(OutData &data)
{
  matlabVariables_ =
      std::vector<MatlabVariable>(data.MaxIndex + 1, MatlabVariable{});
  for (const auto &module : data.Modules)
    matlabVariables_[module.Index].Name = module.Name;

  OutSample sample;
  while (data.GetNextSample(sample, {}))
  {
    matlabVariables_[sample.Index].AddSample(sample.Time, sample.Value);
  }

  out_ << "hold on;\n";
  out_ << "__colors = 'cmkrbg';\n";
  out_ << "__color_i = 1;\n";
  for (const auto &var : matlabVariables_)
    if (!var.Name.empty() && !var.Samples.empty())
    {
      WriteVariableDefinition(var);
      WriteVariablePlotCommand(var);
    }
  
}

void MatlabFile::WriteVariableDefinition(const MatlabVariable &var)
{
  out_ << Sanitize(var.Name) << " = [ ";
  for (const auto &sample : var.Samples)
    out_ << sample.Time << ", " << sample.Value << ";\n";
  out_ << "];\n";
}

void MatlabFile::WriteVariablePlotCommand(const MatlabVariable &var)
{
  out_ << "plot("
       << Sanitize(var.Name) << "(:,1), "
       << Sanitize(var.Name) << "(:,2), "
       << "strcat(__colors(mod(__color_i - 1, length(__colors)) + 1), "
       <<        "';" << ModuleLeafName(var.Name) << ";'));\n";

  out_ << "__color_i = __color_i + 1;\n";
}

std::string MatlabFile::Sanitize(const std::string &moduleName) const
{
  std::string sanitizedName(moduleName.size(), ' ');;
  transform(begin(moduleName), end(moduleName),
            begin(sanitizedName), [](const char ch) {
              if (std::string("/-").find(ch) != std::string::npos)
                return '_';
              return ch;
            });
  return sanitizedName;
}

std::string MatlabFile::ModuleLeafName(const std::string &moduleName) const
{
  //XXX: Assuming hierarchy separator is '/'
  auto pos = moduleName.rfind('/');
  if (pos == std::string::npos)
    return EscapeUnderscores(moduleName);
  return EscapeUnderscores(moduleName.substr(pos + 1));
}

std::string MatlabFile::EscapeUnderscores(const std::string &moduleName) const
{
  std::string ret;
  for (auto ch : moduleName)
  {
    if (ch == '_')
      ret += '\\';
    ret += ch;
  }
  return ret;
}
