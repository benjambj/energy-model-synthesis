#include "DataFrame.hpp"
#include "Out.hpp"
#include "OutSignalSelection.hpp"

#include <iostream>

OutFilter GetUserModuleSelection(const OutHierarchy &);

static unsigned GetTimeInterval()
{
  std::cout << "Enter desired resolution (multiple of timescale): ";
  std::string line;
  getline(std::cin, line);
  unsigned long num;
  try {
    num = stoul(line);
  } catch (const std::exception &) {
    num = 0;
  }
  while (num == 0 || num >= 1ul << 32)
  {
    std::cout << "Invalid number entered, must be integer >= 1\n";
    std::cout << "Enter desired resolution (multiple of timescale): ";
    getline(std::cin, line);
    try {
      num = stoul(line);
    } catch (const std::exception &) {
      num = 0;
    }
  }
  return static_cast<unsigned>(num);
}

static std::pair<bool, unsigned> FindModuleIn(
    const std::string &module,
    const OutHierarchy &hierarchy)
{
  auto root = hierarchy.TraverseModulePath(module, '.',
                                           [&](OutModuleNodePtr) {});
  if (root == nullptr)
    return std::make_pair(false, 0);
  return std::make_pair(true, root->Index);
}

static OutFilter GetModuleSelectionFromStdin(
    const OutHierarchy &hierarchy)
{
  OutFilter filter;
  std::string module;
  while (getline(std::cin, module))
  {
    auto mod = FindModuleIn(module, hierarchy);
    if (mod.first)
    {
      std::cout << "Added module #" << mod.second
                << " (" << module << ")" << std::endl;
      filter.AddIndex(mod.second);
    }
    else
      std::cerr << "Warning: Could not find module " << module << std::endl;
  }
  return filter;
}

int main(int argc, char *argv[])
{
  if (argc != 3 && argc != 4 && argc != 5)
  {
    std::cerr << "Usage: " << argv[0]
              << " <OUT file> <Data frame file> "
              << "[<external memory instance file>]\n";
    std::exit(EXIT_FAILURE);
  }
  auto useTermInfo = (argc == 4);

  std::cout << "Parsing OUT header" << std::endl;
  OutFile outFile { argv[1] };

  auto resolution = GetTimeInterval();

  auto outFilter =
      useTermInfo ? GetModuleSelectionFromStdin(outFile.Hierarchy)
                  : GetUserModuleSelection(outFile.Hierarchy);
  if (outFilter.Empty())
  {
    std::cout << "No modules selected, aborting" << std::endl;
    return 1;
  }

  OutData outData { outFile, outFilter };
  std::cout << "Writing data frame file..." << std::endl;

  if (useTermInfo)
  {
    DataFrameFile dataFrameFile { argv[2], argv[3],
          outFile.Hierarchy };
    dataFrameFile.WritePowerData(outData, resolution);
  }
  else
  {
    DataFrameFile dataFrameFile { argv[2] };
    dataFrameFile.WritePowerData(outData, resolution);    
  }

  return 0;
}
