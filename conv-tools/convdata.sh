#!/bin/bash
shopt -s extglob

. ../config.sh

function numbered_resdir() {
    file=$(basename $2)
    n=${file##$(basename $1)} ## bmarknameNNN.out ==> NNN.out
    n=${n/.*/} ## NNN.out/NNN.vcd ==> NNN
    resdir="$1$n"
    if [ ! -d "${resdir}" ]; then
        mkdir -p ${resdir}
    fi
    echo ${resdir}
}

function benchdata() {
    if [ -z $1 ] || [ -z $2 ]; then
        echo "Usage: $0 <benchmark> <module> [<signal file>]" >&2
        exit 1
    fi
    set -e
    benchmark=$1
    module=$2
    resdir=`get_resdir $benchmark`
    datadir=`get_datadir $benchmark`
    activityfile="$datadir/${benchmark}.vcd"
    datafile="${resdir}/${module}_signals.dat"
    if [ ! -z $3 ]; then
       signalfile=$3;
       use_signalfile="y"
    else
        signalfiles=`find test-data -type f -name ${module}_signals.dat`
        signalfile=${signalfiles## }
        if [ ! -z "${signalfile}" ]; then
            read -p \
            "Do you want to fetch the same signals as in file ${signalfile}? [y/n] " \
            use_signalfile
        fi
    fi
    
    if [ "${use_signalfile}" = "y" ] && [ ! -z "${signalfile}" ]; then
        write_benchdata "${activityfile}" "${datafile}" "${signalfile}"
    else
        write_benchdata "${activityfile}" "${datafile}"
    fi
}

function write_benchdata() {
    if [ ! -z $3 ]; then
       echo -n "Writing activity from ${1} to ${2}, "
       echo    "using the signals in ${3}"
       ./ConvertVcd "${1}" "${2}" "${3}"
    else
        echo "Writing activity from ${1} to ${2}"
        ./ConvertVcd "${1}" "${2}"
    fi
}    

function all_benchdata() {
    if [[ -z $1 || -z $2 || -z $3 ]]; then
        echo "Usage: $0 <benchmark> <module> <dependency file>" >&2
        exit 1
    fi
    benchmark=$1
    module=$2
    depfile=$3
    resdir=`get_resdir $benchmark`
    datadir=`get_datadir $benchmark`
    activityfiles=($datadir/${benchmark}*.vcd)
    for activityfile in ${activityfiles[*]}; do
        if [[ "$activityfile" == ${datadir}/*+([[:digit:]]).vcd ]]; then
            num_resdir=`numbered_resdir $resdir $activityfile`
            datafile="${num_resdir}/${module}_signals.dat"
        else
            datafile="${resdir}/${module}_signals.dat"            
        fi
        write_benchdata "$activityfile" "$datafile" "$depfile"
    done
}

function write_powerdata() {
    if [[ -z $1 || -z $2 ]]; then
        echo "ERROR: internal write_powerdata called with wrong #args"
        exit 1
    fi
    powerfile=$1
    resdir=$2
    datafile="${resdir}/power.dat"
    mkdir -p $(dirname $datafile)
    echo "Writing power from ${powerfile} to ${datafile}"
    if [ -z $3 ]; then
        ./OutToDataFrame "${powerfile}" "${datafile}"
    else
        memfile=$3
        ./OutToDataFrame "${powerfile}" "${datafile}" "${memfile}"
    fi
}

function powerdata() {
    if [ -z $1 ]; then
        echo "Usage: $0 <benchmark>" >&2
        exit 1
    fi
    set -e
    benchmark=$1
    resdir=`get_resdir $benchmark`
    datadir=`get_datadir $benchmark`
    powerfiles=(${datadir}/*.out)
    if [[ ${#powerfiles[*]} != 1 ]]; then
        myinput=$(cat)
    fi
    for powerfile in ${powerfiles[*]}; do
        if [[ "$powerfile" == ${datadir}/*+([[:digit:]]).out ]]; then
            num_resdir=`numbered_resdir $resdir $powerfile`
            if [ -z "$myinput" ]; then
                write_powerdata "$powerfile" "$num_resdir" "$2"
            else
                echo "$myinput" | write_powerdata "$powerfile" "$num_resdir" "$2"
            fi
        else
            if [ -z "$myinput" ]; then
                write_powerdata "$powerfile" "$resdir" "$2"
            else
                echo "$myinput" | write_powerdata "$powerfile" "$resdir" "$2"
            fi
        fi
    done
    # for powerfile in ; do
    #     numbered_resdir=`numbered_resdir $resdir $powerfile`
    #     write_powerdata "$powerfile" "$numbered_resdir" "$2"
    # done
    # for powerfile in ${datadir}/*+([^[:digit:]]).out; do
    #     write_powerdata "$powerfile" "$resdir" "$2"
    # done
}

function validations() {
    if [ -z "$@" ]; then
        echo "Usage: $0 <benchmarks>+" >&2
        exit 1
    fi
        for bm in $@; do
            for modulefile in `find test-data -type f -name '*_signals.dat'`; do
                modulename=`basename ${modulefile}`
                modulename=${modulename%%_signals.dat}
                benchdata "${bm}" "${modulename}" "${modulefile}"
            done
        done
}

function termbenchdata() {
    if [[ -z $1 || -z $2 ]]; then
        echo "Usage: $0 <benchmark> <timescale>" >&2
        exit 1
    fi
    timescale=$2
    termdir=`get_datadir terminfo`
    for depfile in $termdir/*.deps; do
        module=`basename $depfile .deps`
        while true; do echo $timescale; done | all_benchdata $1 $module $depfile
    done
}

function termpowerdata() {
    if [[ -z $1 || -z $2 ]]; then
        echo "Usage: $0 <benchmark> <timescale>" >&2
        exit 1
    fi
    timescale=$2
    termdir=`get_datadir terminfo`

    (echo $timescale 
     for dep in $termdir/*.deps; do
         echo $(basename $dep .deps)
     done) | powerdata $1 $termdir/*.mem
}

case `basename $0`  in
    "convdata.sh")
        benchdata $@
        powerdata $@
        ;;
    "benchdata.sh")
        benchdata $@
        ;;
    "powerdata.sh")
        powerdata $@
        ;;
    "validations.sh")
        validations $@
        ;;
    "termbenchdata.sh")
        termbenchdata $@
        ;;
    "termpowerdata.sh")
        termpowerdata $@
        ;;
   *)
        echo "Unknown conversion version $0"
        exit 1
esac

