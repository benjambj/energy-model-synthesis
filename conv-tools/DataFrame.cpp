#include "DataFrame.hpp"
#include "Vcd.hpp"

#include <cassert>
#include <iostream>

DataFrameFile::DataFrameFile(
    const std::string &filename,
    const std::string &memfile,
    const OutHierarchy &hierarchy)
    : out_{filename},
      memfile_{memfile}
{
  if (out_.fail())
    throw std::runtime_error("Error opening VCD file " + filename);
  if (memfile_.fail())
    throw std::runtime_error(
        "Error opening memory instance file " + memfile);

  RecordMemoryInstanceInheritance(hierarchy);
}

void DataFrameFile::RecordMemoryInstanceInheritance(
    const OutHierarchy &hierarchy)
{
  std::string memInstance;
  while (std::getline(memfile_, memInstance))
  {
    std::vector<OutModuleNodePtr> parents;
    auto memMod = hierarchy.TraverseModulePath(
        memInstance, '.', [&](OutModuleNodePtr mod) {
          parents.push_back(mod);
        });
    
    assert(memMod != nullptr);
    parents.pop_back(); //Remove module as its own parent.
    for (auto parent: parents)
    {
      memInstanceDescendants_[parent->Index].push_back(memMod->Index);
      memInstances_[memMod->Index].push_back(parent->Index);
    }
  }
}

DataFrameFile::DataFrameFile(const std::string &filename) 
    : out_{filename},
      memfile_{}
{
  if (out_.fail())
  {
    throw std::runtime_error("Error opening VCD file " + filename);
  }
}

void DataFrameFile::WriteVcdSignals(Vcd &vcd)
{
  for (const auto &signal: vcd.GetSignals()) {
    out_ << '\t' << signal.Name;
  }
  out_ << '\n';

  unsigned i = 1;
  SignalValues values;
  while (vcd.GetNextSignalValues(values)) 
  {
    out_ << i++;
    for (const auto &value : values.GetSignalValues()) {
      out_ << '\t' << value;
    }
    out_ << '\n';
  }
}

void DataFrameFile::WritePowerData(OutData &data, unsigned timescale)
{
  for (const auto &module : data.Modules)
    out_ << '\t' << Sanitize(module.Name);
  out_ << '\n';

  unsigned i = 1;
  
  OutDataSamples samples{data, timescale, memInstances_,
        memInstanceDescendants_};
  for (const auto &sample : samples)
  {
    out_ << i++;
    for (const auto &value : sample.GetValues())
      out_ << '\t' << value;
    out_ << '\n';
  }

}

std::string DataFrameFile::Sanitize(const std::string &moduleName) const
{
  std::string sanitizedName;
  for (auto ch : moduleName)
    if (std::string("/-").find(ch) != std::string::npos)
      sanitizedName += '.';
    else
      sanitizedName += ch;
  return sanitizedName;
}

bool OutDataSamples::GetNextSample(bool firstSample)
{
  if (isEnd_)
    return false;

  //Round off next interval start calculation to 
  unsigned long nextIntervalStart = currentTime_ + timeInterval_;
  //  nextIntervalStart -= nextIntervalStart % timeInterval_;
  while (!isEnd_ && nextSample_.Time < nextIntervalStart)
  {
    ApplyChange(nextSample_);
    isEnd_ = !data_.GetNextSample(nextSample_, memInstances_);
  }
  CommitChanges();
  currentTime_ = nextIntervalStart;
  if (currentTime_ < nextSample_.Time && firstSample) {
    //Skip ahead to the beginning of the sample periods.
    currentTime_ = nextSample_.Time;
  }
  return true;
}

void OutDataSamples::ApplyChange(const OutSample &sample)
{
  auto isMem =
      memInstances_.find(sample.Index) != std::end(memInstances_);
  
  if (isMem)
  {
    auto powerDiff =
        sample.Value - currentPowerConsumption_[sample.Index];
    currentPowerConsumption_[sample.Index] = sample.Value;
    // std::cout << "Setting power consumption of memory #"
    //           << sample.Index << " to " << sample.Value << std::endl;
    for (auto parent : memInstances_[sample.Index])
      if (lastSampleTime_[parent] == sample.Time)
      {
        // std::cout << "Adjusting #" << parent
        //           << " due to change in #" << sample.Index  << std::endl;

        currentPowerConsumption_[parent] -= powerDiff;
        //Memory instance power consumption was updated after the
        //parent. Just adjust the parent power, calculated using this
        //memory instance's old power, by the power diff.
      }
  }
  else
  {
    unsigned long interval =
        sample.Time - std::max(currentTime_,
                               lastSampleTime_[sample.Index]);
    energyConsumptionInInterval_[sample.Index] +=
        currentPowerConsumption_[sample.Index] * interval;
    currentPowerConsumption_[sample.Index] = sample.Value;
    // std::cout << "Setting power consumption of module #"
    //           << sample.Index << " to " << sample.Value << std::endl;
    for (auto memDescendant : memDescendants_[sample.Index])
    {
      // std::cout << "Subtracting #" << memDescendant << "(" << currentPowerConsumption_[memDescendant] << ")"
      //           << " from #" << sample.Index  << std::endl;
      currentPowerConsumption_[sample.Index] -=
          currentPowerConsumption_[memDescendant];
    }
  }
  lastSampleTime_[sample.Index] = sample.Time;
}

void OutDataSamples::CommitChanges()
{
  for (const auto &module : data_.Modules)
  {
    unsigned long nextIntervalStart = currentTime_ + timeInterval_;
    //    nextIntervalStart -= nextIntervalStart % timeInterval_;
    unsigned long interval = 
        nextIntervalStart - std::max(currentTime_,
                                     lastSampleTime_[module.Index]);
    // std::cout << "Module #" << module.Index << ":\n";
    // std::cout << "\tcur power: " << currentPowerConsumption_[module.Index] << std::endl;
    // std::cout << "\tinterval: " << interval << std::endl;
    energyConsumptionInInterval_[module.Index] +=
        currentPowerConsumption_[module.Index] * interval;
    // std::cout << "\tenergy:  " << energyConsumptionInInterval_[module.Index] << std::endl;
    // std::cout << "\ttime interval: " << timeInterval_ << std::endl;
    values_[module.Index] =
        energyConsumptionInInterval_[module.Index] / timeInterval_;
    // std::cout << "\tvalue: " << values_[module.Index] << std::endl;
    energyConsumptionInInterval_[module.Index] = 0;
  }
    
}

OutDataSampleIterator& OutDataSampleIterator::operator++()
{
  if (!samples_->GetNextSample())
    samples_ = nullptr;
  return *this;
}
