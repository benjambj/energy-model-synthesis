#pragma once

#include <algorithm>
#include <fstream>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>


struct OutModule
{
  unsigned Index;
  std::string Name;
  explicit OutModule() : Index{}, Name{} { /* empty */ }
  explicit OutModule(const OutModule &other)
      : Index{other.Index}, Name{other.Name} { /* empty */ }
  explicit OutModule(unsigned index, const std::string &name)
      : Index{index}, Name{name} { /* empty */ }
};

struct OutSample
{
  unsigned Index;
  unsigned long Time;
  double Value;
};

class OutModuleNode;
typedef std::shared_ptr<OutModuleNode> OutModuleNodePtr;
class OutModuleNode
{
 public:
  explicit OutModuleNode(unsigned index, std::string moduleName)
      : Index{index}, ModuleName{moduleName} { /* empty */ }
  explicit OutModuleNode(
      unsigned index,
      const std::vector<std::string> &modulePath,
      std::size_t pathIndex);
  unsigned Index;
  std::string ModuleName;
  std::vector<OutModuleNodePtr> Children;
  void AddChild(OutModuleNodePtr child) { Children.push_back(child); }
  void AddChild(
      unsigned index,
      const std::vector<std::string> &modulePath,
      std::size_t pathIndex);
};


class OutHierarchy
{
 public:
  std::vector<OutModuleNodePtr> Roots;
  void AddModule(unsigned index, const std::vector<std::string> &modulePath);

  template<typename T>
  OutModuleNodePtr TraverseModulePath(
      const std::string &moduleName,
      char hierarchySeparator,
      T op) const;
};

class OutFile
{
 public:
  explicit OutFile(const std::string &filename);
  unsigned MaxIndex;
  std::vector<OutModule> Modules;
  bool GetNextSample(OutSample &sample);
  OutHierarchy Hierarchy;
 private:
  void ParseHeader();
  void SkipComments(std::string &firstDirective);
  void ParseDirective(const std::string &directive);
  void AddModule(std::istringstream &iss);
  std::vector<std::string> SplitModulePath(const std::string &fullModuleName);
  void ParseValueChange(
      const std::string &index,
      const std::string &value,
      OutSample &sample);
  void ParseTimeChange(const std::string &time);

  std::ifstream in_;
  unsigned long currentTime_;
  char hierarchySeparator_;
};

class OutFilter
{
 public:
  explicit OutFilter() : indices_{} { /* empty */ }
  OutFilter(const std::initializer_list<unsigned> &indices)
      : indices_{indices} { /* empty */ }
  void AddIndex(unsigned index) {
    indices_.insert(index);
  }
  bool Accepts(unsigned index) {
    return indices_.find(index) != indices_.end();
  }
  bool Empty() const { return indices_.empty(); }
 private:
  std::set<unsigned> indices_;
};


class OutData
{
 public:
  explicit OutData(OutFile &file, const OutFilter &filter);
  bool GetNextSample(
      OutSample &sample,
      const std::map<unsigned, std::vector<unsigned>> &memInstances);

  unsigned MaxIndex;
  std::vector<OutModule> Modules;

 private:
  OutFile &file_;
  OutFilter filter_;
};

template<typename T>
OutModuleNodePtr OutHierarchy::TraverseModulePath(
    const std::string &moduleName,
    char hierarchySeparator,
    T op) const
{
  auto curModStart = 0ul;
  auto curModEnd = moduleName.find(hierarchySeparator, curModStart);
  auto curModName =
      moduleName.substr(curModStart, curModEnd - curModStart);

  auto rootPtr = std::find_if(begin(Roots), end(Roots),
                              [&](OutModuleNodePtr root) {
                                return root->ModuleName == curModName;
                              });
  if (rootPtr == end(Roots))
    return nullptr;

  auto curMod = *rootPtr;
  op(curMod);

  curModStart =
      (curModEnd == std::string::npos) ? curModEnd : curModEnd + 1;
  curModEnd = moduleName.find(hierarchySeparator, curModStart);

  while (curModStart != std::string::npos)
  {
    curModName =
        moduleName.substr(curModStart, curModEnd - curModStart);

    auto nextMod = std::find_if(
        begin(curMod->Children), end(curMod->Children),
        [&](OutModuleNodePtr child) {
          return child->ModuleName == curModName; });
    if (nextMod == end(curMod->Children))
      return nullptr;

    curMod = *nextMod;
    op(curMod);

    curModStart =
        (curModEnd == std::string::npos) ? curModEnd : curModEnd + 1;
    curModEnd = moduleName.find('.', curModStart);
  }
  return curMod;
}
