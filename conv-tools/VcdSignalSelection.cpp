#include "MenuLayout.hpp"
#include "NcursesMenu.hpp"
#include "ScreenSetup.hpp"
#include "VcdSignalSelection.hpp"

static MenuLayout CreateModuleMenuLayout()
{
  auto window = newwin(LINES - 7, COLS / 2 - 6, 5, 3);
  keypad(window, TRUE);
  auto layout = MenuLayout::Builder()
      .Window(window)
      .Subwindow(derwin(window, LINES - 11, COLS / 2 - 8, 3, 1))
      .Rows(static_cast<unsigned>(LINES) - 11)
      .Columns(1)
      .Build();
  layout.SetTitle("Modules", COLOR_PAIR(0));
  return layout;
}

static MenuLayout CreateSignalMenuLayout()
{
  auto window = newwin(LINES - 7, COLS / 2 - 6, 5, COLS / 2 + 3);
  keypad(window, TRUE);
  auto layout = MenuLayout::Builder()
      .Window(window)
      .Subwindow(derwin(window, LINES - 11, COLS / 2 - 8, 3, 1))
      .Rows(static_cast<unsigned>(LINES) - 11)
      .Columns(1)
      .Build();
  layout.SetTitle("Signals", COLOR_PAIR(0));
  return layout;
}

template<>
struct MenuItemTraits<VcdSignal>
{
  static std::string Name(const VcdSignal &signal) {
    return signal.Name;
  }
  static std::string Description(const VcdSignal &signal) {
    if (signal.Width == 0)
      return "";

    std::ostringstream oss;
    oss << signal.Width;
    auto desc = oss.str();
    if (desc.size() < 5)
      desc = std::string(5 - desc.size(), ' ') + desc;
    desc += " bit";
    desc += signal.Width > 1 ? 's' : ' ';
    desc += " (id: " + signal.Abbreviation + ")";
    return desc;
  }
  typedef std::string IdType;
  static IdType IdOf(const VcdSignal &signal) {
    return signal.Abbreviation;
  }
  static void ToggleMark(const VcdSignal &) { /* empty */ }
};

template<>
struct MenuItemTraits<VcdModulePtr>
{
  static std::string Name(VcdModulePtr module) {
    return module->Name;
  }
  static std::string Description(VcdModulePtr module) {
    return std::to_string(module->Signals.size()) + " signals";
  }
  static std::vector<VcdModulePtr> GetChildren(VcdModulePtr module) {
    return module->Children;
  }

  typedef std::string IdType;
  static IdType IdOf(VcdModulePtr module) {
    return module->Name;
  }
};

static void PrintNumberSelected(int lines, int cols, unsigned num)
{
  move(lines, cols);
  clrtoeol();
  std::ostringstream oss;
  oss << "Selected signals: " << num;
  mvprintw(lines, cols, oss.str().c_str());
  refresh();
}

VcdFilter GetUserSignalSelection(const VcdHierarchy &hierarchy)
{
  VcdFilter filter;
  
  ScreenSetup setup;

  auto moduleLayout = CreateModuleMenuLayout();
  auto signalLayout = CreateSignalMenuLayout();

  nc::Menu<VcdModulePtr> moduleMenu { { hierarchy.Root }, moduleLayout };

  VcdSignal dummySignal { "<No signals in module>", "", 0 };
  auto signals = hierarchy.Root->Signals;
  if (signals.empty())
    signals = { dummySignal };
  nc::Menu<VcdSignal> signalMenu { signals, signalLayout };

  mvprintw(LINES - 2, 0, "Press Q to abort, enter to accept");
  refresh();

  moduleMenu.Post();
  signalMenu.Post();

  bool moduleMenuActive = true;
  signalLayout.DrawBorder(COLOR_PAIR(0));
  moduleLayout.DrawBorder(COLOR_PAIR(1));
  wrefresh(moduleLayout.Subwindow);
  while (true)
  {
    int ch =
        wgetch(moduleMenuActive ? moduleLayout.Window : signalLayout.Window);
    switch (ch) 
    {
      case 'q':
      case 'Q':
        //Abort
        return {};

      case '\n':
        //Accept current menu 
        for (const auto &signal : signalMenu.GetSelectedItems())
          filter.AddSignal(signal);
        return filter;

      case KEY_DOWN:
        if (moduleMenuActive)
        {
          moduleMenu.DownOneItem();
          signals = moduleMenu.CurrentItem()->Signals;
          if (signals.empty())
            signals = { dummySignal };
          signalMenu.SetItems(signals);
        }
        else signalMenu.DownOneItem();
        break;

      case KEY_UP:
        if (moduleMenuActive)
        {
          moduleMenu.UpOneItem();
          signals = moduleMenu.CurrentItem()->Signals;
          if (signals.empty())
            signals = { dummySignal };
          signalMenu.SetItems(signals);
        }
        else signalMenu.UpOneItem();
        break;

      case KEY_RIGHT:
        if (moduleMenuActive)
          moduleMenu.ExpandItem();
        break;

      case KEY_LEFT:
        if (moduleMenuActive)
          moduleMenu.CollapseItem();
        break;

      case ' ':
        if (moduleMenuActive) signalMenu.MarkAllItems();
        else signalMenu.MarkItem();

        PrintNumberSelected(LINES - 2, COLS / 2,
                            signalMenu.GetNumSelectedItems());
        break;

      case '\t':
        moduleMenuActive = !moduleMenuActive;
        if (moduleMenuActive)
        {
          signalLayout.DrawBorder(COLOR_PAIR(0)); // WHITE / BLACK
          moduleLayout.DrawBorder(COLOR_PAIR(1)); // RED / BLACK
          wrefresh(moduleLayout.Subwindow);
        }
        else
        {
          moduleLayout.DrawBorder(COLOR_PAIR(0)); // WHITE / BLACK
          signalLayout.DrawBorder(COLOR_PAIR(1)); // RED / BLACK  
          wrefresh(signalLayout.Subwindow);
        }
        break;
    }
  }
}
