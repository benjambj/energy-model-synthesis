#include "MenuLayout.hpp"
#include "NcursesMenu.hpp"

struct Soda
{
  Soda(const Soda &s) : Name{s.Name}, Price{s.Price} { /* empty */ }
  Soda(const std::string &name, unsigned price)
      : Name{name}, Price{price} { /* empty */ }
  std::string Name;
  unsigned Price;
};

template<>
struct MenuItemTraits<Soda>
{
  static std::string Name(const Soda &soda) {
    return soda.Name;
  }
  static std::string Description(const Soda &soda) {
    return "Price: " + std::to_string(soda.Price);
  }
  static std::vector<Soda> GetChildren(const Soda &) {
    return { Soda { "My Child!", 88 } };
  }
  static void ToggleMark(const Soda&) {}
  typedef std::string IdType;
  static IdType IdOf(const Soda &soda) { return soda.Name; }
};

static void PrintSelection(const std::vector<Soda> &selectedSodas)
{
  std::ostringstream oss;
  oss << "Selected " << selectedSodas.size() << " sodas, thereamongst:\n";
  move(LINES - 4, 0);
  clrtobot();
  for (unsigned i = 0; i < 3 && i < selectedSodas.size(); ++i)
    oss << selectedSodas[i].Name << "(price " << selectedSodas[i].Price << ")\n";

  mvprintw(LINES - 4, 0, oss.str().c_str());
  refresh();
}

int main()
{
  initscr();
  start_color();
  cbreak();
  noecho();
  keypad(stdscr, TRUE);
  init_pair(1, COLOR_RED, COLOR_BLACK);

  /* Create the window to be associated with the menu */
  //  auto my_menu_win = newwin(14, 40, 4, 4);
  auto my_menu_win = newwin(LINES - 7, COLS / 2 - 3, 5, 3);
  keypad(my_menu_win, TRUE);

  std::vector<Soda> sodas = {
    Soda { "Solo", 17 },
    Soda { "Cola", 19 }
  };

  auto layout = MenuLayout::Builder()
      .Window(my_menu_win)
      .Subwindow(derwin(my_menu_win, LINES - 10, COLS / 2 - 5, 3, 1))
      .Rows(LINES - 10)
      .Columns(1)
      .Build();

  layout.SetTitle("My Menu", COLOR_PAIR(1));
  layout.DrawBorder(COLOR_PAIR(1));

  nc::Menu<Soda> sodaMenu{sodas, layout};

  mvprintw(LINES - 2, 0, "F1 to exit");

  refresh();

  sodaMenu.Post();

  bool acceptingInput = true;
  while (acceptingInput)
  {
    int ch = wgetch(my_menu_win);
    switch (ch) 
    {
      case 'q':
      case 'Q':
        acceptingInput = false;
        break;

      case '\n':
        sodaMenu.AddItemAtCurrentLocation(Soda{"EKSTRABRUS", 3939});
        break;
        
      case KEY_DOWN:
        sodaMenu.DownOneItem();
        break;

      case KEY_UP:
        sodaMenu.UpOneItem();
        break;

      case KEY_RIGHT:
        sodaMenu.ExpandItem();
        break;

      case KEY_LEFT:
        sodaMenu.CollapseItem();
        break;

      case ' ':
        sodaMenu.MarkItem();
        break;
      case 'M':
        sodaMenu.MarkAllItems();
        break;

      case 'p':
        PrintSelection(sodaMenu.GetSelectedItems());
        break;

      case 'r':
        sodaMenu.SetItems(sodas);
        break;
        
      default:
        sodaMenu.Driver(ch);
        break;
    }
  }
  
  endwin();
  return 0;
}

