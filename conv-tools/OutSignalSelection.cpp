#include "MenuLayout.hpp"
#include "NcursesMenu.hpp"
#include "Out.hpp"
#include "OutSignalSelection.hpp"
#include "ScreenSetup.hpp"

template<>
struct MenuItemTraits<OutModuleNodePtr>
{
  static std::string Name(OutModuleNodePtr module) {
    return module->ModuleName;
  }
  static std::string Description(OutModuleNodePtr module) {
    if (module->Index == 0)
      return "";
    auto numChildren = module->Children.size();
    if (numChildren == 0)
      return "Leaf module";
    if (numChildren == 1)
      return "1 child";
    return std::to_string(numChildren) + " children";
  }
  static std::vector<OutModuleNodePtr> GetChildren(OutModuleNodePtr module) {
    return module->Children;
  }
  static void ToggleMark(OutModuleNodePtr) { /* empty */ }
  typedef unsigned IdType;
  static IdType IdOf(OutModuleNodePtr module) {
    return module->Index;
  }
};

template<>
struct MenuItemTraits<std::string>
{
  static std::string Name(const std::string &s) { return s; }
  static std::string Description(const std::string &) { return ""; }
  //  static std::vector<std::string> GetChildren(const std::string&) { return {}; }
  typedef std::string IdType;
  static IdType IdOf(const std::string &s) { return s; }
};

static MenuLayout CreateModuleMenuLayout()
{
  auto window = newwin(LINES - 7, COLS / 2 - 6, 5, 3);
  keypad(window, TRUE);
  auto layout = MenuLayout::Builder()
      .Window(window)
      .Subwindow(derwin(window, LINES - 11, COLS / 2 - 8, 3, 1))
      .Rows(static_cast<unsigned>(LINES) - 11)
      .Columns(1)
      .Build();
  layout.SetTitle("Modules", COLOR_PAIR(0));
  return layout;
}

static MenuLayout CreateSelectedModulesMenuLayout()
{
  auto window = newwin(LINES - 7, COLS / 2 - 6, 5, COLS / 2 + 3);
  keypad(window, TRUE);
  auto layout = MenuLayout::Builder()
      .Window(window)
      .Subwindow(derwin(window, LINES - 11, COLS / 2 - 8, 3, 1))
      .Rows(static_cast<unsigned>(LINES) - 11)
      .Columns(1)
      .Build();
  layout.SetTitle("Selected modules", COLOR_PAIR(0));
  return layout;
}

static void PrintNumberSelected(int lines, int cols, unsigned num)
{
  move(lines, cols);
  clrtoeol();
  std::ostringstream oss;
  oss << "#Modules selected: " << num;
  mvprintw(lines, cols, oss.str().c_str());
  refresh();
}

OutFilter GetUserModuleSelection(const OutHierarchy & hierarchy)
{
  OutFilter filter;
  ScreenSetup setup;
  
  auto moduleLayout = CreateModuleMenuLayout();
  auto selectedModulesMenuLayout = CreateSelectedModulesMenuLayout();

  nc::Menu<OutModuleNodePtr> moduleMenu { hierarchy.Roots, moduleLayout };
  auto dummyModule = "<No modules selected>";
  std::vector<std::string> selectedModules { dummyModule };
  nc::Menu<std::string> selectedModulesMenu {
    selectedModules, selectedModulesMenuLayout };
  
  mvprintw(LINES - 2, 0, "Press Q to abort, enter to accept");
  refresh();

  moduleMenu.Post();
  selectedModulesMenu.Post();

  selectedModulesMenuLayout.DrawBorder(COLOR_PAIR(0));
  moduleLayout.DrawBorder(COLOR_PAIR(1));
  wrefresh(moduleLayout.Subwindow);
  while(true)
  {
    int ch = wgetch(moduleLayout.Window);
    switch (ch) 
    {
      case 'q':
      case 'Q':
        //Abort
        return OutFilter{};

      case '\n':
        //Accept current selection
        for (const auto &module : moduleMenu.GetSelectedItems())
          filter.AddIndex(module->Index);
        return filter;

      case KEY_DOWN:
        moduleMenu.DownOneItem();
        break;

      case KEY_UP:
        moduleMenu.UpOneItem();
        break;

      case KEY_RIGHT:
        moduleMenu.ExpandItem();
        break;

      case KEY_LEFT:
        moduleMenu.CollapseItem();
        break;
        
      case ' ':
        moduleMenu.MarkItem();
        selectedModules.clear();
        for (const auto &module : moduleMenu.GetSelectedItems())
          selectedModules.push_back(module->ModuleName);
        if (selectedModules.empty())
          selectedModules = { dummyModule };
        selectedModulesMenu.SetItems(selectedModules);
        PrintNumberSelected(LINES - 2, COLS / 2,
                            moduleMenu.GetNumSelectedItems());
       break;
    }
  }
}

