#include "MenuLayout.hpp"

#include <cassert>
#include <cstring>

MenuLayout::MenuLayout(const MenuLayout &other)
      : Window(other.Window), Subwindow(other.Subwindow),
        Rows(other.Rows), Columns(other.Columns),
        DefaultColourPair(other.DefaultColourPair),
        SelectedColourPair(other.SelectedColourPair),
        DisabledColourPair(other.DisabledColourPair) { /* empty */ }

MenuLayout::MenuLayout(WINDOW *window, WINDOW *subwindow,
                       unsigned rows, unsigned columns,
                       chtype defaultColourPair, chtype selectedColourPair,
                       chtype disabledColourPair)
    : Window(window), Subwindow(subwindow),
        Rows(rows), Columns(columns),
        DefaultColourPair(defaultColourPair),
        SelectedColourPair(selectedColourPair),
        DisabledColourPair(disabledColourPair) { /* empty */ }

MenuLayout::Builder::Builder(
    WINDOW *window, WINDOW *subwindow,
    unsigned rows, unsigned columns,
    chtype defaultColourPair,
    chtype selectedColourPair,
    chtype disabledColourPair)
    : window_(window), subwindow_(subwindow),
      rows_(rows), columns_(columns),
      defaultColourPair_(defaultColourPair),
      selectedColourPair_(selectedColourPair),
      disabledColourPair_(disabledColourPair) { /* empty */ }

auto MenuLayout::Builder::
Window(WINDOW *window) -> Builder&
{
  window_ = window;
  return *this;
}
                      
auto MenuLayout::Builder::
Subwindow(WINDOW *subwindow) -> Builder&
{
  subwindow_ = subwindow;
  return *this;
}
                      
auto MenuLayout::Builder::
Rows(unsigned rows) -> Builder& 

{
  rows_ = rows;
  return *this;
}
                      
auto MenuLayout::Builder::
Columns(unsigned columns) -> Builder&
{
  columns_ = columns;
  return *this;
}
                      
auto MenuLayout::Builder::
DefaultColourPair(chtype defaultColourPair) -> Builder&
{
  defaultColourPair_ = defaultColourPair;
  return *this;
}
                      
auto MenuLayout::Builder::
SelectedColourPair(chtype selectedColourPair) -> Builder&
{
  selectedColourPair_ = selectedColourPair;
  return *this;
}
                      
auto MenuLayout::Builder::
DisabledColourPair(chtype disabledColourPair) -> Builder&
{
  disabledColourPair_ = disabledColourPair;
  return *this;
}

namespace  
{

static void print_in_middle(WINDOW *win, int starty, int startx, unsigned width,
                            const char *string, chtype color)
{	int length, x, y;
	float temp;

	if(win == NULL)
		win = stdscr;
	getyx(win, y, x);
	if(startx != 0)
		x = startx;
	if(starty != 0)
		y = starty;
	if(width == 0)
		width = 80;

	length = static_cast<int>(std::strlen(string));
	temp = (static_cast<int>(width) - length)/ 2;
	x = startx + static_cast<int>(temp);
	wattron(win, color);
	mvwprintw(win, y, x, "%s", string);
	wattroff(win, color);
	refresh();
}

// static unsigned GetHeight(WINDOW *win)
// {
//   auto ret = getmaxy(win);
//   assert(ret >= 0);
//   return static_cast<unsigned>(ret);
// }
 
static unsigned GetWidth(WINDOW *win)
{
  auto ret = getmaxx(win);
  assert(ret >= 0);
  return static_cast<unsigned>(ret);
}

}

void MenuLayout::SetTitle(const std::string &title, chtype colour)
{
  print_in_middle(Window, 1, 0, GetWidth(Window), title.c_str(), colour);
}

void MenuLayout::DrawBorder(chtype colour)
{
  wattron(Window, colour);
  box(Window, 0, 0);
  mvwaddch(Window, 2, 0, ACS_LTEE);
  mvwhline(Window, 2, 1, ACS_HLINE, GetWidth(Window));
  mvwaddch(Window, 2, GetWidth(Window) - 1, ACS_RTEE);
  wattroff(Window, colour);
  wrefresh(Window);
}
