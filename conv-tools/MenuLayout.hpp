#pragma once

#include <curses.h>
#include <string>



struct MenuLayout {
  WINDOW *Window, *Subwindow;
  unsigned Rows, Columns;
  chtype DefaultColourPair, SelectedColourPair, DisabledColourPair;

  MenuLayout(const MenuLayout &other);

  void SetTitle(const std::string &title, chtype colour);
  void DrawBorder(chtype colour);

 private:
  MenuLayout(WINDOW *window, WINDOW *subwindow,
             unsigned rows, unsigned columns,
             chtype defaultColourPair, chtype selectedColourPair,
             chtype disabledColourPair);

 public:  
  class Builder {
   private:
    WINDOW *window_, *subwindow_;
    unsigned rows_, columns_;
    chtype defaultColourPair_, selectedColourPair_, disabledColourPair_;
   public:
    Builder(WINDOW *window = stdscr, WINDOW *subwindow = stdscr,
                      unsigned rows = 10, unsigned columns = 1,
            chtype defaultColourPair = COLOR_PAIR(0),
            chtype selectedColourPair = COLOR_PAIR(0),
            chtype disabledColourPair = COLOR_PAIR(0));

    MenuLayout Build() {
      return MenuLayout{window_, subwindow_, rows_, columns_,
                        defaultColourPair_, selectedColourPair_,
                        disabledColourPair_};
    }
    Builder& Window(WINDOW *window);
    Builder& Subwindow(WINDOW *window);
    Builder& Rows(unsigned rows);
    Builder& Columns(unsigned columns);
    Builder& DefaultColourPair(chtype defaultColourPair);
    Builder& SelectedColourPair(chtype selectedColourPair);
    Builder& DisabledColourPair(chtype disabledColourPair);
  };

  static MenuLayout Default() {
    return MenuLayout::Builder().Build();
  }
};

