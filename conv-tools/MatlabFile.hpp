#pragma once

#include "Out.hpp"

#include <fstream>
#include <string>
#include <vector>

struct Sample
{
  unsigned long Time;
  double Value;
  explicit Sample(unsigned long time, double value)
      : Time{time}, Value{value} { /* empty */ }
};

struct MatlabVariable
{
  std::string Name;
  std::vector<Sample> Samples;
  explicit MatlabVariable()
      : Name{}, Samples{} { /* empty */ }
  void AddSample(unsigned long time, double value) {
    Samples.emplace_back(time, value);
  }
};

class MatlabFile
{
 public:
  explicit MatlabFile(const std::string &filename);
  void WritePowerData(OutData &data);
 private:
  std::ofstream out_;
  std::vector<MatlabVariable> matlabVariables_;
  void WriteVariableDefinition(const MatlabVariable &var);
  void WriteVariablePlotCommand(const MatlabVariable &var);
  std::string Sanitize(const std::string &moduleName) const;
  std::string ModuleLeafName(const std::string &moduleName) const;
  std::string EscapeUnderscores(const std::string &moduleName) const;
};
