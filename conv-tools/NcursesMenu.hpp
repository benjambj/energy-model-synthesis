#pragma once

#include "MenuLayout.hpp"
#include "NcursesOperations.hpp"

#include <algorithm>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <stdexcept>
#include <vector>

template <typename T>
struct MenuItemTraits
{
  static std::string Name(const T &t) { return t.GetItem(); }
  static std::string Description(const T &t) { return t.GetDescription(); }
  static std::vector<T> GetChildren(const T &t) { return t.GetChildren(); }
  typedef std::string IdType;
};

namespace nc
{

/*
  This class wraps a type T as an item entry in a menu. It manages the lifetime
  of the ITEM* pointer and its name and description strings. 
  
  Requirements for T:
  - Copy-constructible
  - Satisfies requirements from the general MenuItemTraits<T>, or specializes it

    (Types which are to be used at menu items must either have methods GetItem()
    and GetDescription() returning std::string, or supply a specialization of
    the class MenuItemTraits implementing the function Name(const T &t) and
    Description(const T &t).)
 */
template<typename T>
struct Item
{
  explicit Item(const T &t, unsigned childNumber = 0, unsigned level = 0);
  explicit Item(const T &&t);
  ~Item();
  T WrappedItem;
  unsigned NumChildren, ChildNumber, Level;
  bool IsMarked;

  std::string Name, Description;
  ITEM *NcursesItem;

  operator T() { return WrappedItem; }

  typename MenuItemTraits<T>::IdType GetId() {
    return MenuItemTraits<T>::IdOf(WrappedItem);
  }

  void ToggleMark();
 private:
  Item(const Item<T> &item) = delete;
  Item& operator=(const Item<T> &item) = delete;
};

template<typename T>
Item<T>::Item(const T &t, unsigned childNumber, unsigned level)
    : WrappedItem{t},
      NumChildren{0},
      ChildNumber{childNumber},
      Level{level},
      IsMarked{false},
      Name{std::string(level, ' ') + MenuItemTraits<T>::Name(t)},
      Description{MenuItemTraits<T>::Description(t)},
      NcursesItem{new_item(Name.c_str(), Description.c_str())}
{
  ncop::SetItemUserptr(NcursesItem, static_cast<void*>(this));
}

template<typename T>
Item<T>::~Item()
{
  free_item(NcursesItem);
}

template<typename T>
void Item<T>::ToggleMark()
{
  IsMarked = !IsMarked;
  MenuItemTraits<T>::ToggleMark(WrappedItem);
  ncop::SetItemValue(NcursesItem, IsMarked);
}

template<typename T>
Item<T> *Get(ITEM *item)
{
  return static_cast<Item<T>*>(ncop::GetItemUserptr(item));
}

/*
  This class wraps several objects of type T as item entries in a menu. The
  class manages the lifetime of the menu and its dependencies, including the
  lifetimes of the contained Item<T>'s. 
 */
template<typename T>
class Menu
{
 public:
  explicit Menu(const std::vector<T> &items)
      : Menu(items,
             O_SHOWDESC | O_ROWMAJOR | O_IGNORECASE | O_SHOWMATCH | O_NONCYCLIC,
             MenuLayout::Default()) { /* empty */ }
  
  explicit Menu(
      const std::vector<T> &items,
      Menu_Options options)
      : Menu(items,
             options,
             MenuLayout::Default()) { /* empty */ }
  
  explicit Menu(
      const std::vector<T> &items,
      const MenuLayout &layout)
      : Menu(items,
             O_SHOWDESC | O_ROWMAJOR | O_IGNORECASE | O_SHOWMATCH | O_NONCYCLIC,
             layout) { /* empty */ }

  explicit Menu(
      const std::vector<T> &items,
      Menu_Options options,
      const MenuLayout &layout);

  ~Menu();

  void AddItem(const T &item);
  void AddItems(const std::vector<T> &items);
  void SetItems(const std::vector<T> &items);
  void AddItemAtCurrentLocation(const T &item) {
    AddItemsAtCurrentLocation({item});
  }
  void AddItemsAtCurrentLocation(const std::vector<T> &items);
  void SetLocation(std::size_t index);
  void RemoveItemRange(long from, long until);
  void RemoveItemsAt(std::size_t index, std::size_t count) {
    long from = static_cast<long>(index);
    long until = static_cast<long>(index + count);
    assert(from >= 0 && until >= 0);
    RemoveItemRange(from, until);
  }
  void RemoveItems(std::size_t count) {
    RemoveItemsAt(GetCurrentIndex(), count);
  }

  void Unpost() {
    ncop::UnpostMenu(menu_);
  }

  void Post() {
    ncop::PostMenu(menu_);
    RefreshWindow();
  }

  void SetOptions(Menu_Options options) {
    ncop::SetMenuOptions(menu_, options);
  }

  //Returns true if the keypress was reacted on, false otherwise
  bool Driver(int keypress);

  void DownOneItem();
  void UpOneItem();
  void ExpandItem();
  void CollapseItem();
  void MarkItem();
  void MarkAllItems();
  
  unsigned GetNumSelectedItems() const {
    auto numSelected = static_cast<unsigned>(selectedItems_.size());
    assert(numSelected == selectedItems_.size());
    return numSelected;
  }
  std::vector<T> GetSelectedItems() const {
    std::vector<T> items;
    for (const auto &item : selectedItems_)
      items.push_back(item.second->WrappedItem);
    return items;
  }
  T CurrentItem() {
    return GetCurrentItem()->WrappedItem;
  }

 private:
  //Disallow copying
  Menu& operator=(const Menu<T>&) = delete;
  Menu(const Menu<T>&) = delete;

  MenuLayout layout_;
  std::vector<std::shared_ptr<Item<T>>> items_;
  std::vector<ITEM*> ncursesItems_;
  MENU *menu_;
  unsigned restoreIndex_;
  std::map<typename MenuItemTraits<T>::IdType,
           std::shared_ptr<Item<T>>> selectedItems_;

  void RefreshWindow() { wrefresh(layout_.Window); }
  void SetItems(ITEM **items) {
    ncop::SetMenuItems(menu_, items);
  }

  unsigned GetCurrentIndex() {
    return static_cast<unsigned>(
        ncop::ItemIndex((ncop::CurrentItem(menu_))));
  }

  void AddItemToList(const T &item) {
    items_.push_back(std::make_shared<Item<T>>(item));
    ncursesItems_.back() = items_.back()->NcursesItem;
    ncursesItems_.push_back(nullptr);
  }

  void AddItemsToList(const std::vector<T> &items) {
    for (const auto &item : items)
      AddItemToList(item);
  }

  void AddItemsAtCurrentLocation(
      const std::vector<std::shared_ptr<Item<T>>> &items);

  void RemoveChildren(std::shared_ptr<Item<T>> parent);

  void StoreCurrentLocation() {
    restoreIndex_ = GetCurrentIndex();
  }


  void RestoreLocation() {
    for (unsigned i = 0; i < restoreIndex_; ++i)
      ncop::MenuDriver(menu_, REQ_DOWN_ITEM);
    auto res = ncop::MenuDriver(
        [](int err) { return err == E_OK || err == E_REQUEST_DENIED; },
        menu_, REQ_DOWN_ITEM);
    if (res == E_OK)
      ncop::MenuDriver(
          [](int err) { return err == E_OK || err == E_REQUEST_DENIED; },
          menu_, REQ_UP_ITEM);
  }

  void RestoreMarks() {
    for (const auto &item : items_)
    {
      item->IsMarked =
          selectedItems_.find(item->GetId()) != selectedItems_.end();
      ncop::SetItemValue(item->NcursesItem, item->IsMarked);
    }
  }

  void FreezeMenu() {
    StoreCurrentLocation();
    Unpost();
    SetItems(nullptr);
  }

  void UnfreezeMenu() {
    SetItems(ncursesItems_.data());
    
    Post();
    
    RestoreMarks();

    RestoreLocation();

    RefreshWindow();
  }

  std::shared_ptr<Item<T>> GetItem(std::size_t index) {
    return items_.at(index);
  }

  std::shared_ptr<Item<T>> GetCurrentItem() {
    return GetItem(GetCurrentIndex());
  }

  std::shared_ptr<Item<T>> GetParent(std::shared_ptr<Item<T>> item) {        
    assert(item->Level > 0);
    auto index = ncop::ItemIndex(item->NcursesItem);
    assert(index >= 0);
    return GetItem(static_cast<unsigned>(index) - item->ChildNumber - 1);
  }

  void PrintMenuDebugInformation(int col);
};

template <typename T>
Menu<T>::Menu(
    const std::vector<T> &items,
    Menu_Options options,
    const MenuLayout &layout)
    : layout_{layout},
      items_{},
      ncursesItems_{nullptr},
      menu_{nullptr},
      restoreIndex_{0}
{
  for (const auto &item : items)
  {
    AddItemToList(item);
  }
  menu_ = new_menu(ncursesItems_.data());
  assert(menu_ != nullptr);
  SetOptions(options);
  ncop::SetMenuWin(menu_, layout_.Window);
  ncop::SetMenuSub(menu_, layout_.Subwindow);
  ncop::SetMenuFormat(menu_,
                      static_cast<int>(layout_.Rows),
                      static_cast<int>(layout_.Columns));
  ncop::SetMenuFore(menu_, layout_.SelectedColourPair);
  ncop::SetMenuBack(menu_, layout_.DefaultColourPair);
  ncop::SetMenuGrey(menu_, layout_.DisabledColourPair);
}

template<typename T>
Menu<T>::~Menu()
{
  free_menu(menu_);
}

template<typename T>
void Menu<T>::AddItem(const T &item)
{
  FreezeMenu();
  AddItemToList(item);
  UnfreezeMenu();
}

template <typename T>
void Menu<T>::AddItems(const std::vector<T> &items)
{
  FreezeMenu();
  AddItemsToList(items);
  UnfreezeMenu();
}

template<typename T>
void Menu<T>::SetItems(const std::vector<T> &items)
{
  FreezeMenu();
  ncursesItems_.clear();
  ncursesItems_.push_back(nullptr);
  items_.clear();
  restoreIndex_ = 0;
  AddItemsToList(items);
  UnfreezeMenu();
}

template<typename T>
bool Menu<T>::Driver(int keypress)
{
  switch (keypress) 
  {
    case KEY_DOWN:
      DownOneItem();
      return true;
    case KEY_UP:
      UpOneItem();
      return true;
    case KEY_RIGHT:
      ExpandItem();
      return true;
    case KEY_LEFT:
      CollapseItem();
      return true;
    default:
      return false;
  }
}

template<typename T>
void Menu<T>::DownOneItem()
{
  ncop::MenuDriver(
      [](int err) { return err == E_OK || err == E_REQUEST_DENIED; },
      menu_, REQ_DOWN_ITEM);  
  RefreshWindow();
}

template<typename T>
void Menu<T>::UpOneItem()
{
  ncop::MenuDriver(
      [](int err) { return err == E_OK || err == E_REQUEST_DENIED; },
      menu_, REQ_UP_ITEM);
  RefreshWindow();
}

template<typename T>
void Menu<T>::ExpandItem()
{
  auto currentItem = GetCurrentItem();
  if (currentItem->NumChildren > 0)
    return;

  auto children = MenuItemTraits<T>::GetChildren(currentItem->WrappedItem);
  if (children.empty())
    return;
  
  std::vector<std::shared_ptr<Item<T>>> childItems;
  for (std::size_t i = 0; i < children.size(); ++i)
    childItems.push_back(
        std::make_shared<Item<T>>(
            children.at(i), i, currentItem->Level + 1));

  AddItemsAtCurrentLocation(childItems);
  assert(childItems.size() < 1ul << 32);
  currentItem->NumChildren = static_cast<unsigned>(childItems.size());
}

template<typename T>
void Menu<T>::CollapseItem()
{
  auto currentItem = GetCurrentItem();
  if (currentItem->NumChildren > 0)
  {
    RemoveChildren(currentItem);
  }
  else if (currentItem->Level > 0)
  {
    RemoveChildren(GetParent(currentItem));
  }
}

template<typename T>
void Menu<T>::MarkItem()
{
  auto currentItem = GetCurrentItem();
  currentItem->ToggleMark();
  if (currentItem->IsMarked)
    selectedItems_[currentItem->GetId()] = currentItem;
  else
    selectedItems_.erase(currentItem->GetId());
  RestoreMarks();
}
 
template<typename T>
void Menu<T>::MarkAllItems()
{
  bool allMarked = true;
  for (const auto &item : items_)
    if (!item->IsMarked)
    {
      allMarked = false;
      break;
    }
  for (const auto &item : items_)
    if (allMarked)
      selectedItems_.erase(item->GetId());
    else
      selectedItems_[item->GetId()] = item;
  RestoreMarks();
  RefreshWindow();
}


template<typename T>
void Menu<T>::AddItemsAtCurrentLocation(
    const std::vector<std::shared_ptr<Item<T>>> &items)
{
  auto index = GetCurrentIndex();

  FreezeMenu();

  items_.insert(begin(items_) + index + 1, begin(items), end(items));
  std::transform(begin(items), end(items),
                 std::inserter(ncursesItems_, begin(ncursesItems_) + index + 1),
                 [](const std::shared_ptr<Item<T>> item) {
                   return item->NcursesItem;
                 });

  UnfreezeMenu();
}

template<typename T>
void Menu<T>::AddItemsAtCurrentLocation(const std::vector<T> &items)
{
  auto currentItem = GetCurrentItem();
  std::vector<std::shared_ptr<Item<T>>> itemWrappers;
  for (unsigned i = 0; i < items.size(); ++i)
  {
    itemWrappers.push_back(
        std::make_shared<Item<T>>(
            items.at(i),currentItem->ChildNumber + i + 1, currentItem->Level));
  }    
  
  auto parentItem = GetParent(currentItem);
  parentItem->NumChildren += items.size();

  AddItemsAtCurrentLocation(itemWrappers);
}


template<typename T>
void Menu<T>::RemoveChildren(std::shared_ptr<Item<T>> parent)
{
  unsigned numChildren = 0;
  auto parentIndex = ncop::ItemIndex(parent->NcursesItem);
  for (auto it = begin(items_) + parentIndex + 1,
           e = end(items_); it != e && (*it)->Level > parent->Level; ++it)
  {
    numChildren += 1;
  }
  assert(parentIndex >= 0);
  RemoveItemsAt(static_cast<unsigned>(parentIndex) + 1, numChildren);
  parent->NumChildren = 0;
}

template<typename T>
void Menu<T>::RemoveItemRange(long from, long until)
{
  FreezeMenu();
  if (restoreIndex_ >= from)
    restoreIndex_ -= std::min(1 + restoreIndex_ - from, until - from);

  auto start = begin(items_) + from, end = begin(items_) + until;
  items_.erase(start, end);
  auto ncItemStart = begin(ncursesItems_) + from,
      ncItemEnd = begin(ncursesItems_) + until;
  ncursesItems_.erase(ncItemStart, ncItemEnd);
  UnfreezeMenu();
}


template<typename T>
void Menu<T>::PrintMenuDebugInformation(int col)
{
    std::ostringstream oss;
    oss << "toprow: " << menu_->toprow << "\n"
        << "frows: " << menu_->frows << "\n"
        << "arows: " << menu_->arows << "\n"
        << "rows: " << menu_->rows << "\n"
        << "current item: " << ncop::CurrentItem(menu_) << "\n"
        << "current item->y: " << ncop::CurrentItem(menu_)->y << std::endl;
    std::istringstream iss{oss.str()};
    for (int i = 6; i > 0; --i)
    {
      std::string line;
      getline(iss, line);
      mvprintw(LINES - i, col, line.c_str());
    }

    refresh();
}


} //end namespace nc
