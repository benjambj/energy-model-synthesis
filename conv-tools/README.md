# Data Conversion Tools

The scripts and source code in this directory is used to convert data in the following ways:

- VCD files (signal activity) ---> R data frame files, using `ConvertVcd`
- OUT files (power data)      ---> R data frame files, using `OutToDataFrame`

## Configuration

The programs `ConvertVcd` and `OutToDataFrame` should first be created
by running `make` (you will need ncurses installed).

If you are using your own benchmarks, you should modify the
`get_resdir()` function in `./config.sh` to indicate which of your
benchmarks are training benchmarks, and which are to be used for
validation.

Make sure to set the `DATADIR` variable in the file
`<repo-root>/env-setup.sh` to indicate where your VCD files, OUT files
and terminfo files are located.

## The timescale parameter

The conversions support the concept of a custom *timescale*, which can
be used if the simulation granularity is finer than is desirable. This
feature was historically used to smear out simulation data which was
gathered with a finer-than-clock-cycle granularity. When commands
request a `timescale` parameter, the value supplied should be the
clock cycle period (which in general is the same as the simulation
granularity, in which case the timescale parameter has no effect).

## Conversion scripts

The following shell scripts can be used to perform the
conversions. The two first scripts use terminfo files (see the
`<repo-root>/hdl/` folder README) to automatically determine which
modules to get data for, which signals to fetch, and which on-chip
memory instances exist whose power consumption should be
deducted. These scripts also understand sampled data files (through
name convention <file><sample#>, e.g. `sha{1,2,3}.vcd`). These two
scripts are therefore most often used. If terminfo files are not
present, if sampled benchmarks are not used (or stored using other
filename conventions), or if manual control is is required, the other
scripts can be used.

1. `./termbenchdata.sh <benchmark> <timescale>`: Expects a benchmark
name and a [timescale](#markdown-header-the-timescale-parameter). Also
expects a signal dependency file in `$DATADIR` for each
module. Creates a data frame from all VCD files matching
`$DATADIR/<benchmark>*.vcd` (the glob expression is used to support
multiple samples from one benchmark).

2. `./termpowerdata.sh <benchmark> <timescale>`: Expects a benchmark
name and a [timescale](#markdown-header-the-timescale-parameter). Also
expects a memory instance file in `$DATADIR`. Creates a data frame
from from all OUT files matching `$DATADIR/<benchmark>*.out`.

3. `./benchdata.sh <benchmark> <module> [<signal file>]`: Expects a
benchmark name and a module name, and optionally a signal dependency
file name. Prompts the user to select signals if the signal dependency
file is not supplied as an argument. Creates the data frame file
`$(get_resdir <benchmark>)/<module>_signals.dat`
(e.g. `validation-data/aes/Top_signals.dat`) from the VCD file
`$DATADIR/<benchmark>.vcd`.

4. `./powerdata.sh <benchmark> [<memory instance file>]`: Expects a
benchmark name, and optionally a memory instance file. Prompts the
user to enter a timescale. If the memory instance file is supplied, a
list of module names is expected on stdin. Otherwise, the user is
presented with a menu from which modules can be selected. Creates the
data frame file `$(get_resdir <benchmark>)/power.dat`
(e.g. `validation-data/aes/power.dat`) from the OUT file
`$DATADIR/<benchmark>.out`.
(This script also supports sampled benchmark files, i.e. if called as
`./powerdata.sh aes` it will detect the case where the `aes` benchmark
power data is split into files `$DATADIR/aes1.out`,
`$DATADIR/aes2.out` etc.)

5. `./convdata.sh ...`: Runs both `./benchdata.sh` and
`./powerdata.sh` using the arguments (this looks like it has been
broken, since the argument lists to `powerdata.sh` and `benchdata.sh`
appear to be incompatible).

6. `./validations.sh <benchmarks>+`: Expects a non-empty list of
benchmarks. For each benchmark, signals are fetched for each module
file found in the `test-data` directory. (The use case is that if
training data signals is selected manually first, you may after
modelling want to create compatible data frames for validation
benchmarks. Rather than manually selecting the same signals, you run
`./validations.sh <validation benchmark list>` to create data frames
which are compatible with those created for training benchmarks.)
