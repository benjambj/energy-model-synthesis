#include "DataFrame.hpp"
#include "Vcd.hpp"
#include "VcdSignalSelection.hpp"

#include <iostream>

static femtoseconds GetTimeInterval(femtoseconds timescale)
{
  std::string line;
  unsigned long num;
  do {
    std::cout << "Enter desired time interval as multiple of timescale "
              << "(timescale: " << timescale.count() << " fs): ";
    getline(std::cin, line);
    try {
      num = stoul(line);
    } catch (const std::exception &ex) {
      std::cout << "Error: " << ex.what() << std::endl;
      num = 0;
    }

    if (num == 0) {
      std::cout << "Invalid number '" << line
                << "' entered, must be integer >= 1\n";
    } else {
      std::cout << "Read value " << num << " from line "
                << '"' << line << '"' << std::endl;
    }
  } while (num == 0);

  return timescale * num;
}

static bool IsDependencyFile(const std::string &filename)
{
  std::string ext = ".deps";
  return filename.rfind(ext) == filename.size() - ext.size();
}

static VcdFilter CreateFilterBasedOnDependencyFile(
    const std::string &filename,
    const VcdHierarchy &hierarchy)
{
  auto dotpos = filename.rfind('.');
  if (dotpos == std::string::npos)
    throw std::runtime_error(
        "Expected signal file name format <module name>.deps");
  auto seppos = filename.rfind('/');
  auto startpos = seppos == std::string::npos ? 0 : seppos + 1;
  auto moduleName = filename.substr(startpos, dotpos - startpos);
  auto module = hierarchy.FindModuleByAbsoluteName(moduleName);
  
  std::ifstream in{filename};
  if (in.fail())
    throw std::runtime_error("Could not open dependency file " +
                             filename);
  std::string line;
  if (!getline(in, line))
    throw std::runtime_error("Dependency file " + filename +
                             " is empty");
  std::istringstream iss{line};
  std::string signalName;
  VcdFilter filter;
  while (iss >> signalName)
  {
    for (const auto &vcdSignal: module->Signals)
    {
      if (vcdSignal.Name == signalName)
      {
        std::cout << "Including signal " << vcdSignal.Name
                  << " (" << vcdSignal.Abbreviation << ")\n";
        filter.AddSignal(vcdSignal);
      }
    }
  }
  return filter;
}

static VcdFilter CreateFilterBasedOnSignalFile(
    const std::string &filename,
    const VcdHierarchy &hierarchy)
{
  auto dotpos = filename.rfind('.');
  if (dotpos == std::string::npos)
    throw std::runtime_error(
        "Expected signal file name format *<module name>_signals.dat");
  
  if (dotpos < 8)
    throw std::runtime_error(
        "Expected signal file name format *<module name>_signals.dat");
  if (filename.substr(dotpos - 8, 8) != "_signals")
    throw std::runtime_error(
        "Expected signal file name format *<module name>_signals.dat");
  auto seppos = filename.rfind('/');
  auto startpos = seppos == std::string::npos ? 0 : seppos + 1;
  auto moduleName = filename.substr(startpos, dotpos - 8 - startpos);
  auto modules = hierarchy.FindModulesByName(moduleName);
  if (modules.size() == 0)
    throw std::runtime_error(
        "No module named " + moduleName + " was found in the VCD hierarchy");
  if (modules.size() > 1)
    std::cerr << "Warning: found " << modules.size()
              << " modules matching name " << moduleName << ". " 
              << "Too many signals may be retrieved\n";

  std::ifstream in{filename};
  if (in.fail())
    throw std::runtime_error("Error opening signal file " + filename);
  std::string signalLine;
  if (!getline(in, signalLine))
    throw std::runtime_error("Signal file " + filename + " is empty");
  std::istringstream iss{signalLine};
  std::string signalName;
  VcdFilter filter;
  while (iss >> signalName)
  {
    for (const auto module : modules)
      for (const auto &vcdSignal : module->Signals)
        if (vcdSignal.Name == signalName)
        {
          std::cout << "Including signal " << vcdSignal.Name
                    << " (" << vcdSignal.Abbreviation << ")\n";
          filter.AddSignal(vcdSignal);
        }
  }
  return filter;
}

static VcdFilter CreateFilterBasedOnFile(
    const std::string &filename,
    const VcdHierarchy &hierarchy)
{
  if (IsDependencyFile(filename))
    return CreateFilterBasedOnDependencyFile(filename, hierarchy);
  else
    return CreateFilterBasedOnSignalFile(filename, hierarchy);
}

VcdFilter GetUserSignalSelection(const VcdHierarchy &hierarchy);

int main(int argc, char *argv[])
{
  if (argc != 3 && argc != 4)
  {
    std::cerr << "Usage: "
              << argv[0] << " <VCD filename> <Output File> [<signal file>]\n";
    std::exit(EXIT_FAILURE);
  }

  std::cout << "Parsing VCD hiearchy..." << std::endl;
  VcdFile vcdFile { argv[1] };
  DataFrameFile outFile { argv[2] };
  
  auto timescale = GetTimeInterval(vcdFile.Timescale);

  VcdFilter filter;
  if (argc == 4)
    filter = CreateFilterBasedOnFile(argv[3], vcdFile.Hierarchy);
  else
    filter = GetUserSignalSelection(vcdFile.Hierarchy);
  if (filter.Empty())
  {
    std::cout << "No signals selected, aborting" << std::endl;
    return 1;
  }

  std::cout << "Parsing VCD file..." << std::endl;
  Vcd vcd { vcdFile, filter, timescale };
  
  std::cout << "Writing data output file..." << std::endl;
  outFile.WriteVcdSignals(vcd);

  return 0;
}
