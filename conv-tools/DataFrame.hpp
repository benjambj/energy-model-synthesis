#pragma once

#include "Out.hpp"

#include <fstream>
#include <map>
#include <string>
#include <vector>

class Vcd;

class DataFrameFile
{
 public:
  explicit DataFrameFile(const std::string &filename);
  explicit DataFrameFile(
      const std::string &filename,
      const std::string &memfile,
      const OutHierarchy &hierarchy);

  void WriteVcdSignals(Vcd &vcd);
  void WritePowerData(OutData &data, unsigned timescale);
 private:
  std::ofstream out_;
  std::ifstream memfile_;
  std::map<unsigned, std::vector<unsigned>> memInstances_;
  std::map<unsigned, std::vector<unsigned>> memInstanceDescendants_;

  std::string Sanitize(const std::string &moduleName) const;
  void RecordMemoryInstanceInheritance(
      const OutHierarchy &hierarchy);
};


class OutDataSamples;
class OutDataSampleIterator
{
 public:
  explicit OutDataSampleIterator()
      : samples_{nullptr} { /* empty */ }
  explicit OutDataSampleIterator(OutDataSamples *samples)
      : samples_{samples} { /* empty */ }
  bool operator!=(const OutDataSampleIterator &other) {
    return samples_ != other.samples_;
  }
  bool operator==(const OutDataSampleIterator &other) {
    return !(*this != other);
  }
  OutDataSamples& operator*() { return *samples_; }
  OutDataSampleIterator& operator++();
 private:
  OutDataSamples *samples_;
};

class OutDataSamples
{
 private:
  friend class OutDataSampleIterator;
  OutData &data_;
  OutSample nextSample_;
  bool isEnd_;
  unsigned long timeInterval_;
  unsigned long currentTime_;
  std::vector<double> values_;
  std::vector<double> currentPowerConsumption_;
  std::vector<unsigned long> lastSampleTime_;
  std::vector<double> energyConsumptionInInterval_;
  std::map<unsigned, std::vector<unsigned>> memInstances_;
  std::map<unsigned, std::vector<unsigned>> memDescendants_;


  void ApplyChange(const OutSample &sample);
  void CommitChanges();
  bool GetNextSample(bool firstSample=false);

 public:  
  explicit OutDataSamples(
      OutData &data, unsigned timeInterval,
      const std::map<unsigned, std::vector<unsigned>> &memInstances,
      const std::map<unsigned, std::vector<unsigned>> &memDescendants)
      : data_(data),
        nextSample_{},
        isEnd_{!data_.GetNextSample(nextSample_, memInstances)},
        timeInterval_{timeInterval},
        currentTime_{nextSample_.Time},
        values_(data.MaxIndex + 1, 0),
        currentPowerConsumption_(data.MaxIndex + 1, 0),
        lastSampleTime_(data.MaxIndex + 1, 0),
        energyConsumptionInInterval_(data.MaxIndex + 1, 0),
        memInstances_{memInstances},
        memDescendants_{memDescendants}
  {
    GetNextSample(true/* first non-init sample */);
  }

  OutDataSampleIterator begin() {
    return OutDataSampleIterator(isEnd_ ? nullptr : this);
  }
  OutDataSampleIterator end() { return OutDataSampleIterator(); }
  operator bool() { return isEnd_; }
  OutDataSamples& operator++() { GetNextSample(); return *this; }

  std::vector<double> GetValues() const {
    std::vector<double> filteredValues;
    for (const auto &module : data_.Modules)
      filteredValues.push_back(values_[module.Index]);
    return filteredValues;
  }
};

