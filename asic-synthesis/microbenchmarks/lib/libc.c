#include <stdint.h>
#include <stdlib.h>

#define ALIGNMENT 8
static uint32_t next_malloc_addr;
void *
malloc(size_t size)
{
  void *ret = (void*)next_malloc_addr;
  next_malloc_addr += size + (ALIGNMENT - 1);
  next_malloc_addr &= ~(ALIGNMENT - 1);
  return ret;
}

extern void *kernel_end;
void
init_malloc(void)
{
  next_malloc_addr = (uint32_t)kernel_end;
}

int
strcmp(const char *s1, const char *s2)
{
  while (*s1 && *s2)
    {
      int diff = *s1++ - *s2++;
      if (diff)
        return diff;
    }
  return *s1 - *s2;
}

void
memcpy(char *d, const char *s, int l)
{
  while (--l >= 0) *d++ = *s++;
}

int
puts(const char *str)
{
  print_string(str);
}

int
printf(const char *fmt, ...)
{
  print_string(fmt);
}

int
putchar(char ch)
{
  print_char(ch);
}
