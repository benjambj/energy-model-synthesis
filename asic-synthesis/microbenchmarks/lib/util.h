#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef void irq_handler_t(void);

void
register_irq_handler (irq_handler_t *);
void
register_syscall_handler (irq_handler_t *);

void 
print_char(uint8_t c);

void
print_string (const char *str);

void
end_benchmark (bool success);

void
enable_cache (void);

void
set_user_mode (void);
