        .global enable_cache
enable_cache:
        mov r1,  #0x7fffffff 
        mcr 15, 0, r1, cr3, cr0, 0   /* cacheable area*/
        mov r1,  #1
        mcr 15, 0, r1, cr2, cr0, 0   /* cache enable */
	bx lr

	.global set_user_mode
set_user_mode:
	mov r1, lr
        mrs	r0, cpsr
	bic 	r0, #0xff
	orr	r0, #0x10
	msr	cpsr, r0
	ldr     sp, =user_stack_top
        bx r1

/* void end_benchmark(bool success) */	
        .global end_benchmark
end_benchmark:
        mov r1, #0
        str r0, [r1] /* Signals the testbench that the benchmark is done */
        bx lr
