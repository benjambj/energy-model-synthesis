#include "util.h"
void
unexpected_exception (unsigned num);

static void
default_irq_handler (void)
{
  unexpected_exception(6);

}
static irq_handler_t *irq_handler = default_irq_handler;

void
register_irq_handler (irq_handler_t *h)
{
  irq_handler = h;
}

void
call_registered_irq_handler (void)
{
  irq_handler();
}

static void
default_syscall_handler(void)
{
  unexpected_exception(2);
}
static irq_handler_t *syscall_handler = default_syscall_handler;

void
register_syscall_handler (irq_handler_t *h)
{
  syscall_handler = h;
}

void
call_registered_syscall_handler (void)
{
  syscall_handler();
}



#define SYS_OUT_DATA   (volatile uint32_t*)(0xFFFF0000)
#define SYS_INT_STATUS (volatile uint32_t*)(0xFFFF0020)
#define SYS_READY      (volatile uint32_t*)(0xFFFF0050)

void 
print_char(uint8_t c) 
{
  // wait
  while((*SYS_INT_STATUS) & 2);
  // write word
  *SYS_OUT_DATA = c;
}


void
print_string (const char *str)
{
  while(*str != 0) 
    {
      if(*str == '\n') print_char('\r');
      print_char(*str);
      str++;
    }
}

void
unexpected_exception (unsigned num)
{
  const char *exception_names[] = 
    {
      "reset", 
      "undefined",
      "swi",
      "prefetch_abort",
      "data_abort",
      "*RESERVED*",
      "irq",
      "firq"
    };
  print_string("Exception ");
  if (num >= 8)
    print_string("<unknown>");
  else
    print_string(exception_names[num]);

  end_benchmark(false);
}
