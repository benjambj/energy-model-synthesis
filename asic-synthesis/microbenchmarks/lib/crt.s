.global excpvector
excpvector:
	b reset_handler
	b undefined_handler
	b swi_handler
	b prefetch_abort_handler
	b data_abort_handler
	b . /* RESERVED */
	b irq_handler
	b firq_handler

reset_handler:
        ldr sp, =svc_stack_top
        bl main
	
	mov r0, #$done_str
        bl print_string

	mov r0, #1
	bl end_benchmark
        b .
$done_str:       .string "DONE"
	.align 4
	
undefined_handler:
	mov r0, #1
        bl unexpected_exception
        b .

swi_handler:
	push {lr}
	bl call_registered_syscall_handler
        pop {lr}
        movs pc, lr

prefetch_abort_handler:
	mov r0, #3
        bl unexpected_exception
        b .
	
data_abort_handler:
	mov r0, #4
        bl unexpected_exception
        b .

irq_handler:
        ldr sp, =irq_stack_top
	sub lr, lr, #4
	push {lr}
	bl call_registered_irq_handler
	pop {lr}
        movs pc, lr

firq_handler:
	mov r0, #7
        bl unexpected_exception
        b .
        
