#include "util.h"

#include <stdint.h>

uint8_t bytes[128];
uint16_t halfwords[128];
uint32_t words[128];

static void
memory_operations (void)
{
  for (unsigned i = 0; i < 4; ++i)
    {
      bytes[88] = 1;
      uint16_t temp = (256 | 255);
      bytes[13] = (uint8_t)temp;
      halfwords[23] = 23423;
      uint32_t temp2 = 0xcafead;
      halfwords[38] = (uint16_t)temp2;
      words[4] = 0xdaa;
      words[126] = 0xffffffff;
      uint8_t b1 = bytes[88];
      int i1 = ((int8_t*)bytes)[13];
      uint16_t hw1 = halfwords[23];
      int i2 = ((int16_t*)halfwords)[38];
      uint32_t w1 = words[4];
      int i3 = ((int32_t*)words)[126];
    }
}

struct tile_registers {
  uint32_t cpuid;
  uint32_t x;
  uint32_t y;
} *tileregs = (struct tile_registers*)(0xfffe0000u);

uint32_t *cpucount = (uint32_t*)0xffff0040u;
uint32_t *bram = (uint32_t*)0xf8000000;
static void
bram_accesses (void)
{
  int sum = 0;
  if (tileregs->cpuid < *cpucount)
    {
      for (int i = 0; i < 5; i++)
        bram[i] = i;
      for (int i = 0; i < 5; i++)
        sum += bram[i];
    }
  if (sum != 1 + 2 + 3 + 4)
    {
      print_string("failure");
      end_benchmark(false);
    }
}

struct timer_registers {
  uint32_t load, value, ctrl, clr;
} *timer = (struct timer_registers*)(0xfffe1000u);

struct interrupt_controller
{
  volatile uint32_t status;
  volatile uint32_t rawstat;
  volatile uint32_t enableset;
  volatile uint32_t enableclear;
  volatile uint32_t softset;
  volatile uint32_t softclear;
  volatile uint32_t padding[2];
  volatile uint32_t firq_status;
  volatile uint32_t firq_rawstat;
  volatile uint32_t firq_enableset;
  volatile uint32_t firq_enableclear;
} *irq_controller = (struct interrupt_controller*)(0xfffe2000u);


volatile bool softirq_set = false;
volatile bool timerirq_set = false;
static void
irq_handler(void)
{
  switch (irq_controller->status)
    {
    case 1 << 0:
      irq_controller->softclear = 1;
      softirq_set = true;
      return;
    case 1 << 2:
      timer->clr = 1;
      timerirq_set = true;
      print_string(".\n");
      return;
    default:
      print_string("fail (wrong IRQ status)\n");
      end_benchmark(false);
    }
}

static void
irq_tests (void)
{
  register_irq_handler(irq_handler);
  irq_controller->enableset = 5; //timer0 | soft_irq
  timer->load = 1;
  timer->ctrl = 1 << 7 | 1 << 3;
  while (!timerirq_set)
    {
    }
  timer->ctrl = 0;
  irq_controller->softset = 1;
  while (!softirq_set)
    {
    }
}

static int global_result;
static int
alu_test (int arg1, int arg2, int arg3, int arg4, int arg5,
           int arg6, int arg7, int arg8, int arg9, int arg10)
{
  int temp = arg1 + arg2;
  temp = temp - arg3;
  int temp2 = arg3 * arg4;
  unsigned temp3 = (unsigned)arg3 * (unsigned)arg4;
  temp = temp2 ^ arg5;
  temp2 = arg6 + (arg7 >> 3);
  if (temp2 > 3242)
    temp2 = arg8 | temp2 << 2;
  else
    temp2 = arg8 | temp2 << 4;
  
  temp2 = arg9 | temp2 >> 8;
  temp += 23;
  temp -= temp2;
  long long temp4 = (long long)temp * (long long)temp2;
  unsigned long long temp5 = (long long)temp * (long long)temp2;
  temp2 = 5 - (temp5 & 0xaa);
  if (temp4 & 0xffffff000000lu)
    temp4 -= 42;
  
  int temp6 = arg10 - (int)(temp4 >> 16);
  temp2 ^= temp6;
  temp = (temp << 3) | temp2;
  temp |= temp3;
  return temp;
}

static void
syscall_handler(void)
{
  int syscall_result = alu_test(0, 1, 0xaa, 0xff, 0xafff, 0xd887a,
                                0x881231, 0xffff00, 0x10000, 0x7fffffff);
  if (syscall_result != global_result)
    {
      print_string("failure");
      end_benchmark(false);
    }
}

static void
syscall (void)
{
  register_syscall_handler(syscall_handler);
  __asm__("swi #0");
}


int main(void)
{
  set_user_mode();

  print_string("1\n");
  memory_operations();
  enable_cache();
  print_string("2\n");
  memory_operations();

  print_string("3\n");
  bram_accesses();
  
  print_string("4\n");
  irq_tests();

  print_string("5\n");
  global_result = alu_test(0, 1, 0xaa, 0xff, 0xafff, 0xd887a,
                           0x881231, 0xffff00, 0x10000, 0x7fffffff);

  print_string("6\n");
  syscall();

  return 0;
}
