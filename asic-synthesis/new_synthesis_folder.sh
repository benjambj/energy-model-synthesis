n=`ls ../syn | awk '/pass/' | grep -o "[[:digit:]]" | sort -r | head -n1`
n=${n:-0}
next=`expr $n + 1`
echo "$(readlink -f ../syn/pass${next})"
