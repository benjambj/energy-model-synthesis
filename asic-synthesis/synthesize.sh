if [ -z $1 ]; then
	echo "Usage: $0 <build directory>"
	exit 1
fi

mkdir -vp $1
synscriptdir=`pwd`
cd $1
echo "Running synthesis in $1"
cp -v ${synscriptdir}/localfolder_.synopsys_dc.setup .synopsys_dc.setup

dc_shell -64bit -f ${synscriptdir}/scripts/complete_synthesis.tcl 2> synthesis_errors | tee synthesis_output

