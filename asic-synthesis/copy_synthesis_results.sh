if [ -z $1 ]; then
	echo "Usage: $0 <build directory>"
	exit 1
fi

ln -sfv $1/Top.{vg,sdc,spef} .

