set power_enable_analysis TRUE
set power_analysis_mode time_based

set_host_options -num_processes 1 -max_cores 8
set LIBDIR $::env(LIBDIR)

set synthetic_library $::env(SYNTHETIC_LIB)
set target_library $::env(TARGET_LIB)
set link_library [concat "*" $target_library $synthetic_library]

source ptconfig.tcl
source ios.tcl

foreach {module instance constraints} $modules {
    # We need to remove all designs to start the next module analysis with a
    # clean slate. We do it here instead of the bottom to allow issuing commands
    # with the power analysis results still loaded after the final module
    # analysis.

    read_verilog $netlist
    # Have to set current design right after reading the verilog, at some point down
    # the path only the design we have set is remembered
    current_design $module
    link

    # We have to generate a per-module sdc file. Which really is not too bad -
    # this file is quite short, and only specifies units (pF, V, mW etc) and
    # clocks. One problem is that constraints apparently are not inherited, so
    # we do not get the clock constraint for sub-modules of the compilation
    # modules. We solve this by reading two sdc scripts: one with the
    # compilation unit clock constraint, and one with units (and potentially
    # other things) generated for each module.
    read_sdc scripts/$constraints
    read_sdc ${module}.sdc

    # Reading the parasitics file of a higher-level module gives warnings for
    # each signal which is not present, which encourage using independent
    # parasitics files. However, higher-level parasitics files also contain
    # sub-module parasitic information, so this strategy leads to redundant
    # storage if top-level modules are also analysed. The more fundamental issue
    # is that power analysis of higher-level modules accumulates power from all
    # sub-modules, which makes it harder to analyze the top-level internal logic
    # power consumption isolated from sub-module power consumption. The
    # parasitics file granularity will most likely mirror the solution to this
    # issue.
    read_parasitics ${module}.spef

    check_timing
    # PTPX manual page 32: "Before specifying the switching activity
    # for power, perform timing analysis using the 'update_timing'
    # command. This maximizes performance by preventing any additional
    # timing updates triggered by the activity annotation commands.'
    #
    # Perhaps we can skip the 'check_timing' command? No point, it is
    # run by update_timing if it has not already been run.
    update_timing 
    report_timing



    # We get out_of_range warnings for a large number of cells, due to ramp and
    # load values being set to zero (outside of range (0.00, xx.yy). This sounds
    # like it might yield inaccurate power measurements; especially the
    # load-value of zero should yield too optimistic results.
    check_power

    # Moved this before timing checks, to avoid having to wait for
    # timing checks before getting feedback on any errors concerning
    # the activity file.
    foreach { input output } $ios {
        read_vcd -strip_path ${top_path}${instance} ${sim_mode} ${input}
        
        set_power_analysis_options \
            -waveform_output ${output} \
            -include all_without_leaf \

        echo "Calculating power consumption from RTL activity ${input}"
        update_power
        report_power -include_estimated_clock_network

        save_session ${output}_session
    }

    remove_design -all
}

quit
