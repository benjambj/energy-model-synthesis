# EDA Tools Scripts

This folder contains scripts for synthesising a Verilog design using
Synopsys DesignCompiler, and calculating time-based power consumption
using Synopsys PrimeTime. Its contents should be uploaded to a server
with these tools and a cell library installed.

## Synthesis

Before synthesising anything, you should install the file
`homefolder_.synopsys_dc.setup` as `$HOME/.synopsys_dc.setup`. Modify
`<repo-root>/env-setup.sh` to accommodate your synthesis
environment. You must also upload the HDL sources of the design you
want to synthesize to the synthesis server. If using the same design
as is included in this repository (the Rocket core), this is the file
`<repo-root>/hdl/vsim/generated-src/Top.DefaultVLSIConfig.v` which is
generated using rules in the folder `<repo-root>/hdl/` (see the folder
README for details).

Synthesis is run using the makefile target `synthesize`. The target
will use the folder `../syn` as a container for output files from
different synthesis runs (e.g. `../syn/pass1`, `../syn/pass2` etc),
which enables a multi-pass synthesis flow (or just experimenting with
synthesis parameters without overwriting the build directory every
time). The makefile target uses the following three scripts to
orchestrate the synthesis:

- `./new_synthesis_folder.sh`: This script creates a new folder
  `../syn/passY` with Y set to the lowest number for which there is no
  folder `../syn/passX` where X < Y. This folder will be the working
  directory for the synthesis tool.

- `./synthesize.sh`: This script copies the project setup file
  `localfolder_.synopsys_dc.setup` to the working directory. It then
  runs synthesis using the script
  `scripts/complete_synthesis.tcl`. Console output is stored in the
  files `synthesis_errors` and `synthesis_output` in the working
  directory.

- `./copy_synthesis_results.sh`: This script creates a symbolic link
  to the netlist, design constraint file and parasitic capacitance
  file in this directory.

The synthesis tool instructions are found in the folder `scripts/`. As
mentioned, the entry point is the file
`scripts/complete_synthesis.tcl`. The flow in this script is:

1. Read the design sources using `scripts/read_hdl.tcl`. 

2. Synthesize the design, using a custom script for each module which
   is separately synthesized. For small designs, a single script
   suffices, but for bigger designs it can be beneficial in terms of
   synthesis time to decompose the design and do a coarse-grained
   bottom-up synthesis (cf. Synopsys DesignCompiler User Guide).

3. Write output files.

To use the synthesis scripts for a different design, one must
therefore:

1. Modify `scripts/read_hdl.tcl` to refer to one's own Verilog
   sources.

2. Create a new synthesis script for each module which is separately
   synthesized (at least one, for the top-level component). The
   template of the existing `scripts/compile_*.tcl` files can be used
   as inspiration.

## Power Calculation

To calculate power consumption for a set of activity (VCD) files, use
the appropriate Makefile target in
`<repo-root>/regression-benchmarks/` (see the corresponding
README). That target uses a script in this directory, namely
`analyze_power.tcl`, to drive Synopsys PrimeTime. The script sources
the file `ptconfig.tcl`, which specifies the modules for which power
calculation should be run. If using a separate design, `ptconfig.tcl`
must be updated accordingly.
