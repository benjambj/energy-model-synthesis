# Design compiler TCL script
# Write parasitics for each module in the modules.tcl file.
read_verilog -net shmac_toplevel.vg
source modules.tcl
foreach {module instance constraints} $modules {
    current_design $module
    write_sdc ${module}.sdc
    write_parasitics -o ${module}.spef
}

quit
