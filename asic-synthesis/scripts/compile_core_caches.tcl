elaborate asic_sram_line_en -param "DATA_WIDTH = 21, ADDRESS_WIDTH = 8"
source $constraints/core_cache_tag_array.tcl
compile -gate_clock
write -format ddc -hierarchy -output asic_sram_line_en_DATA_WIDTH21_ADDRESS_WIDTH8.ddc
set_dont_touch asic_sram_line_en_DATA_WIDTH21_ADDRESS_WIDTH8

elaborate asic_sram_256x128_byte_en -param "DATA_WIDTH = 128, ADDRESS_WIDTH = 8"
source $constraints/core_cache_tag_array.tcl
compile -gate_clock
write -format ddc -hierarchy -output asic_sram_256x128_byte_en_DATA_WIDTH128_ADDRESS_WIDTH8.ddc
set_dont_touch asic_sram_256x128_byte_en_DATA_WIDTH128_ADDRESS_WIDTH8

