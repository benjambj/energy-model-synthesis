saif_map -start
source $scripts/read_hdl.tcl

#set clockgate "-gate_clock"
#set clockgate ""

source $scripts/compile_srams.tcl
source $scripts/compile_rocket_chip.tcl
#source $scripts/compile_bram_tile_ram.tcl
#source $scripts/compile_shmac_toplevel.tcl
change_names -rules verilog -hierarchy
write -format verilog -hierarchy -output Top.vg
#write_sdf shmac_toplevel.sdf
write_sdc Top.sdc
write_parasitics -o Top.spef
saif_map -type ptpx -write_map Top.map
