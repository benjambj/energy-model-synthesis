elaborate ram -param "WORDS = 1024"
source $constraints/bram_tile_ram.tcl
compile -gate_clock
write -format ddc -hierarchy -output ram_WORDS1024.ddc
set_dont_touch ram_WORDS1024
