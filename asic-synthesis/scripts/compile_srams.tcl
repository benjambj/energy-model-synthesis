elaborate MetadataArray_tag_arr
source $constraints/sram.tcl
compile
write -format ddc -hierarchy -output MetadataArray_tag_arr.ddc
set_dont_touch MetadataArray_tag_arr

elaborate ICache_tag_array
source $constraints/sram.tcl
compile
write -format ddc -hierarchy -output ICache_tag_array.ddc
set_dont_touch ICache_tag_array

elaborate HellaFlowQueue_ram
source $constraints/sram.tcl
compile
write -format ddc -hierarchy -output HellaFlowQueue_ram.ddc
set_dont_touch HellaFlowQueue_ram

elaborate DataArray_T6
source $constraints/sram.tcl
compile
write -format ddc -hierarchy -output DataArray_T6.ddc
set_dont_touch DataArray_T6

elaborate ICache_T159
source $constraints/sram.tcl
compile
write -format ddc -hierarchy -output ICache_T159.ddc
set_dont_touch ICache_T159
