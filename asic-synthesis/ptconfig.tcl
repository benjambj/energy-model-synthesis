set netlist Top.vg

# top_path is the path to the top module for which RTL activity has
# been captured, e.g. "rocketTestHarness/dut/" (or
# "rocketTestHarness/"?) For the chisel C++ emulator, there is no such
# top path to strip away.
set top_path ""

set sim_mode -rtl

set modules {
    Top Top constraints/Top.tcl
}
