package rocketchip //Why not... (

import Chisel._
import scala.language.reflectiveCalls

class SubModule extends Module {
  val io = new Bundle {
    val in = Bool(INPUT)
    val out = Bool(OUTPUT)
  }

  val R4: UInt = Reg(init = io.in);
  R4 := R4 ^ io.in
  val x = Valid(io.in)
  x.valid := Bool(true)
  x.bits := io.in

  val testpipe = Pipe(x)

  io.out := io.in ^ (UInt(1) + !testpipe.bits & R4)
}

class TestModule extends Module {
  val io = new Bundle {
    val in = Bool(INPUT)
    val out = Bool(OUTPUT)
  }

  val myreg = Reg(UInt(width=2), init=UInt(3))
  val submod = Module(new SubModule)
  submod.io.in := Bool(true)//myreg(0)

  myreg :=  (submod.io.out & io.in) | (((myreg(1) << UInt(1)) + myreg(1)) - (myreg(0) ^ UInt(1) + myreg(0)))

  val testNest = Mux(myreg === UInt(0), submod.io.out & io.in,
                 Mux(myreg === UInt(1), submod.io.out | io.in,
                 Mux(myreg === UInt(2), submod.io.out ^ io.in,
                 !(submod.io.out & io.in))))

  val testLookup = MuxLookup(myreg, !(submod.io.out & io.in), Vector(
    UInt(0) -> (submod.io.out & io.in),
    UInt(1) -> (submod.io.out | io.in),
    UInt(2) -> (submod.io.out ^ io.in)))

  val testCase = MuxCase(!(submod.io.out & io.in), Vector(
    (myreg === UInt(0)) -> (submod.io.out & io.in),
    (myreg === UInt(1)) -> (submod.io.out | io.in),
    (myreg === UInt(2) || myreg === UInt(3)) -> (submod.io.out ^ io.in)))

  io.out := Mux(io.in, testNest, testLookup | testCase)
}
