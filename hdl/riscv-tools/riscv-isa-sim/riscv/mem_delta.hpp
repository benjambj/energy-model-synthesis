#pragma once

#include "common.h"

#include <cstdint>
#include <iterator>
#include <map>
#include <ostream>
#include <utility>

class mem_delta_t
{
 public:
  void add_access(uint64_t addr, size_t bytes) {
    auto loc = accesses_.insert(std::make_pair(addr, bytes));
    auto before = prev(loc.first);
    auto after = next(loc.first);
    auto overlap_before = loc.first != accesses_.begin() &&
        before->first + before->second >= addr;
    auto overlap_after = after != accesses_.end() &&
        addr + bytes >= after->first;
    if (unlikely(overlap_before))
    {
      if (unlikely(overlap_after))
      {
        // Merge before, new and after.
        auto merged_node = std::make_pair(
            before->first,
            after->first + after->second - before->first);
        accesses_.erase(before, next(after)); //range: [before, after+1)
        accesses_.insert(merged_node);
      }
      else
      {
	// Merge before and new
        auto merged_node = std::make_pair(
            before->first, addr + bytes - before->first);
        accesses_.erase(before, after); //range: [before, after)
        accesses_.insert(after, merged_node);
      }
    }
    else if (unlikely(overlap_after))
    {
      // Merge with new and after
      auto merged_node = std::make_pair(
          addr, after->first + after->second - addr);
      accesses_.erase(loc.first, next(after));
      accesses_.insert(merged_node);
    }
  }
  void reset() {
    accesses_.clear();
  }
  void dump_to(std::ostream &out) const;
  void set_mem_ptr(const char *mem) { mem_ = mem; }
 private:
  std::map<uint64_t, size_t> accesses_;
  const char *mem_;
};
