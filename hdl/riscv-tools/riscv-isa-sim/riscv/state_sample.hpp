#pragma once

#include "mem_delta.hpp"
#include "memtracer.h"

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>

class processor_t;
class sim_t;
class mmu_t;

class state_sample_t : public memtracer_t
{
 public:
  explicit state_sample_t(uint32_t sample_period)
      : sample_period_{sample_period},
    sample_countdown_{sample_period_},
    mem_delta_{} { /*empty */}

  /*
    The whole "memtracer" idea is slightly flawed:
    - It reports accesses on a physical-page granularity
    - It does not report accesses when the TLB lookup is successful
      (from what I can tell)

    It may be better to add a separate hook into the store-macro in
    mmu.h. If we maintain a set of addresses in this state sample, at
    the sample snapshot stage we can access all these modified
    addresses and write their contents as the diff.

    The advantage of the memtracer is that since the granularity is
    larger, there will be fewer calls to our state sample hook and
    less storage required. If we stored the set of pages which have
    been accessed, at snapshot-time we could dump the contents of
    these pages. The downside is larger sample files. I think it is
    better to prioritize smaller sample files than simulation speed,
    so we should start with this.

    Does not require adding a handle to our sample_state in the mmus
    separately from the existing tracer scheme, though. Maybe try it,
    should be simple :)
   */
    

  void countdown(sim_t *sim);

  void notify_store(std::ptrdiff_t addr, size_t bytes) {
    mem_delta_.add_access(static_cast<uint64_t>(addr), bytes);
  }
      
  virtual bool interested_in_range(
      uint64_t begin, uint64_t end, bool store, bool fetch) override {
    // return (end - begin) == PGSIZE;
    return false;
  }
  virtual void trace(
      uint64_t addr, size_t bytes, bool store, bool fetch) override {
  //   if (fetch)
  //     itlb_.add_access(addr, bytes, store, fetch);
  //   else
  //     dtlb_.add_access(addr, bytes, store, fetch);
  }
  void set_mem_ptr(const char *mem) { mem_delta_.set_mem_ptr(mem); }

  void dump_snapshot(const std::vector<processor_t*> &procs);

 private:
  uint32_t sample_period_, sample_countdown_;
  mem_delta_t mem_delta_;
  unsigned counter_{0};

  std::string new_snapshot_filename() const;
  void dump_state_of_processors(
      std::ostream &out, const std::vector<processor_t*> &procs);
  void dump_processor_state(std::ostream &out, processor_t *proc);
  void dump_processor_register_state(std::ostream &out, processor_t *proc);
  void dump_processor_tlb_state(std::ostream &out, const mmu_t *mmu);
};
