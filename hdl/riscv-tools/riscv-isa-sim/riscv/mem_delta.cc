#include "mem_delta.hpp"

#include <iomanip>
#include <iostream>

void mem_delta_t::dump_to(std::ostream &out) const
{
  std::cout << "Dumping " << accesses_.size() << " accesses\n";
  auto sum = 0u;
  for (const auto &item: accesses_) {
    out << '[' << item.first << ", " << item.first + item.second << "): \n";
    out.write(&mem_[item.first], item.second);
    sum += item.second;
  }
  std::cout << "Total memory written: " << sum << std::endl;
}
