#include "decode.h"
#include "processor.h"
#include "sim.h"
#include "state_sample.hpp"

#include <fstream>
#include <iostream>

void state_sample_t::dump_snapshot(
    const std::vector<processor_t*> &procs)
{
  auto snapshot_filename = new_snapshot_filename();
  std::ofstream out{snapshot_filename};

  if (!out) {
    std::cerr << "Could not open snapshot file "
              << snapshot_filename << std::endl;
    return;
  }
    
  dump_state_of_processors(out, procs);
  mem_delta_.dump_to(out);
  mem_delta_.reset();

  ++counter_;
}

std::string state_sample_t::new_snapshot_filename() const
{
  return "state_sample_snapshot_" + std::to_string(counter_);
}

void state_sample_t::countdown(sim_t *sim)
{
  sample_countdown_ -= 1;
  if (sample_period_ != 0 && sample_countdown_ == 0)
  {
    dump_snapshot(sim->procs);
    sample_countdown_ = sample_period_;
  }
}

void state_sample_t::dump_state_of_processors(
    std::ostream &out, const std::vector<processor_t*> &procs)
{
  std::cout << "Dumping processor register state" << std::endl;
  for (auto proc : procs)
    dump_processor_state(out, proc);
}

void state_sample_t::dump_processor_state(
    std::ostream &out, processor_t *proc)
{
  dump_processor_register_state(out, proc);
  if (proc->get_state()->sr & SR_VM) //Virtual memory enabled
    dump_processor_tlb_state(out, proc->get_mmu());
}

void state_sample_t::dump_processor_register_state(
    std::ostream &out, processor_t *proc)
{
  auto state = proc->get_state();
  out.write(reinterpret_cast<char*>(state->XPR.data), NXPR*sizeof(*state->XPR.data));
  out.write(reinterpret_cast<char*>(state->FPR.data), NFPR*sizeof(*state->FPR.data));
  out.write(reinterpret_cast<char*>(&state->epc), sizeof(state->epc));
  out.write(reinterpret_cast<char*>(&state->badvaddr), sizeof(state->badvaddr));
  out.write(reinterpret_cast<char*>(&state->evec), sizeof(state->evec));
  out.write(reinterpret_cast<char*>(&state->ptbr), sizeof(state->ptbr));
  out.write(reinterpret_cast<char*>(&state->pcr_k0), sizeof(state->pcr_k0));
  out.write(reinterpret_cast<char*>(&state->pcr_k1), sizeof(state->pcr_k1));
  out.write(reinterpret_cast<char*>(&state->cause), sizeof(state->cause));
  out.write(reinterpret_cast<char*>(&state->tohost), sizeof(state->tohost));
  out.write(reinterpret_cast<char*>(&state->fromhost), sizeof(state->fromhost));
  out.write(reinterpret_cast<char*>(&state->count), sizeof(state->count));
  out.write(reinterpret_cast<char*>(&state->compare), sizeof(state->compare));
  out.write(reinterpret_cast<char*>(&state->sr), sizeof(state->sr));
  out.write(reinterpret_cast<char*>(&state->fflags), sizeof(state->fflags));
  out.write(reinterpret_cast<char*>(&state->frm), sizeof(state->frm));
  out.write(reinterpret_cast<char*>(&state->load_reservation), sizeof(state->load_reservation));
  out.write(reinterpret_cast<char*>(&state->pc), sizeof(state->pc));
}

void state_sample_t::dump_processor_tlb_state(
    std::ostream &out, const mmu_t *mmu)
{
}


