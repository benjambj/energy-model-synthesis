#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#

training_c_src = \
	training.c \
	syscalls.c \
	myvmcopy.c \

training_riscv_src = \
	crt.S \
	myvmsupport.S \

incs += -I${CURDIR}/../env/v
training_c_objs     = $(patsubst %.c, %.o, $(training_c_src))
training_riscv_objs = $(patsubst %.S, %.o, $(training_riscv_src))

training_host_bin = training.host
$(training_host_bin) : $(training_c_src)
	$(HOST_COMP) $^ -o $(training_host_bin)

# vm.o: ${CURDIR}/../env/v/vm.c
# 	$(RISCV_GCC) $(RISCV_GCC_OPTS) $(bmark_defs) $(incs) -I../env/v $< -c -o $@
# entry.o: ${CURDIR}/../env/v/entry.S
# 	$(RISCV_GCC) $(RISCV_GCC_OPTS) $(bmark_defs) $(incs) -I../env/v $< -c -o $@

training_riscv_bin = training.riscv
$(training_riscv_bin) : $(training_c_objs) $(training_riscv_objs)
	$(RISCV_LINK) $(training_c_objs) $(training_riscv_objs) -o $(training_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(training_c_objs) $(training_riscv_objs) \
        $(training_host_bin) $(training_riscv_bin)

RISCV_GCC_OPTS := -static -Wa,-march=RVIMAFDXhwacha -std=gnu99 -O0  #Do not optimize
