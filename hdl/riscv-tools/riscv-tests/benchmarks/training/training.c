#include "common.h"
#include "util.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

uint8_t bytes[128];
uint16_t halfwords[128];
uint32_t words[128];

static void
memory_operations (void)
{
  for (unsigned i = 0; i < 4; ++i)
    {
      bytes[88] = 1;
      uint16_t temp = (256 | 255);
      bytes[13] = (uint8_t)temp;
      halfwords[23] = 23423;
      uint32_t temp2 = 0xcafead;
      halfwords[38] = (uint16_t)temp2;
      words[4] = 0xdaa;
      words[126] = 0xffffffff;
      uint8_t b1 = bytes[88];
      int i1 = ((int8_t*)bytes)[13];
      uint16_t hw1 = halfwords[23];
      int i2 = ((int16_t*)halfwords)[38];
      uint32_t w1 = words[4];
      int i3 = ((int32_t*)words)[126];
    }
}

static int global_alu_result;
static bool alu_result_set = false;
static int
alu_test (int arg1, int arg2, int arg3, int arg4, int arg5,
           int arg6, int arg7, int arg8, int arg9, int arg10)
{
  int temp = arg1 + arg2;
  temp = temp - arg3;
  int temp2 = arg3 * arg4;
  unsigned temp3 = (unsigned)arg3 * (unsigned)arg4;
  temp = temp2 ^ arg5;
  temp2 = arg6 + (arg7 >> 3);
  if (temp2 > 3242)
    temp2 = arg8 | temp2 << 2;
  else
    temp2 = arg8 | temp2 << 4;
  
  temp2 = arg9 | temp2 >> 8;
  temp += 23;
  temp -= temp2;
  long long temp4 = (long long)temp * (long long)temp2;
  unsigned long long temp5 = (long long)temp * (long long)temp2;
  temp2 = 5 - (temp5 & 0xaa);
  if (temp4 & 0xffffff000000lu)
    temp4 -= 42;
  
  int temp6 = arg10 - (int)(temp4 >> 16);
  temp2 ^= temp6;
  temp = (temp << 3) | temp2;
  temp |= temp3;
  return temp;
}

float global_fpu_result;
bool fpu_result_set = false;
static float
fpu_test (float arg1, float arg2, float arg3, float arg4, 
          float arg5, float arg6, float arg7)
{
  float t1 = arg1 + arg2;
  float t2 = arg2 - arg3;
  float t3 = t1 * arg4;
  float t4 = 1.0 - arg5;
  float t5 = t3 - t4 + arg6;
  float t6 = arg6 > 100.003f ? arg7 : t5;
  return t6;
}

static void
run_memory_operations (void)
{
  memory_operations();
}

static void
run_alu_operations (void)
{
  int result = alu_test(0, 1, 0xaa, 0xff, 0xafff, 0xd887a,
                        0x881231, 0xffff00, 0x10000, 0x7fffffff);
  if (!alu_result_set)
    {
      global_alu_result = result;
      alu_result_set = true;
    }
  else
    {
      if (result != global_alu_result)
        {
          printf("ERROR: New alu result %d != %d\n",
                 result, global_alu_result);
        }
    }
    
}

static void
run_fpu_operations (void)
{
  float result = fpu_test(0.1f, 2.2f, 3.3f, 4.4f,
                          128238.2398239f, -23.0f, 1337.42f);
  if (!fpu_result_set)
    {
      global_fpu_result = result;
      fpu_result_set = true;
    }
  else
    {
      if (fabs(global_fpu_result - result) > fabs(1e-6*result))
        {
          //ERROR
          printf("ERROR: New fpu result %f != %f\n",
                 result, global_fpu_result);
        }
    }
}

static void
run_stimuli_computations (void)
{
  run_memory_operations();
  run_alu_operations();
  run_fpu_operations();
}

void vm_boot(long test_addr);

static void
run_computations_with_vm (void)
{
  run_stimuli_computations();
  exit(0);
}

static void
enable_vm (void)
{
  vm_boot((uintptr_t)run_computations_with_vm);
}

int main(void)
{
  /*
    What do we need?
    - Some memory operations
    - Some arithmetic operations
    - Some control flow.
    - Floating point
    - Some stuff pre-VM
    - Some stuff post-VM
   */

  printf("%d: Start computation.\n", 1);
  run_stimuli_computations();
  printf("%d: Enable VM... ", 2);
  enable_vm();
  printf("ok. \n%d: Compute again.\n", 3);
  run_stimuli_computations();
  printf("%d: Done.\n", 4);

  return 0;
}
