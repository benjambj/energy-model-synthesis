package Chisel

import ChiselError._
import Node._
import PartitionIslands._
import Reg._
import scala.annotation.tailrec
import scala.collection.immutable.Stack
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable
import scala.collection.immutable

import scala.language.experimental.macros
import scala.reflect.macros.Context


class ModelTermBackend extends Backend {
  import ModelTermBackend._ //Our private helper classes 
  override val keywords = Set[String]()
  //isEmittingComponents is required to support Node::componentOf.
  override val isEmittingComponents: Boolean = true 

  private val eimInputValues = mutable.Map[String, EnergyImpactMatrix]()

  override def elaborate(c: Module): Unit = {
    super.elaborate(c)
    addBindings
    nameBindings
    //We want bindings to get well-named handles to submodule outputs
    //for our EIM and term generation, since submodule outputs are
    //roots in the connectivity DAG.

    ChiselError.info("Running my superpass: ModelTermBackend!")

    ChiselError.info("Propagating literal values")
    ModelTermBackend.performLiteralPropagation(c)
    ChiselError.info(s"${lit.size} literal nodes detected")

    ChiselError.info("Pruning dead subgraphs")
    val numPruned = ModelTermBackend.pruneDeadSubgraphs(c)
    ChiselError.info(s"Pruned $numPruned nodes")

    val tempSignalFile = createTempSignalFile()
    val memInstanceFile = createMemoryInstanceFile()
    //We want to traverse the instances in sorted order, because the
    //EIM of a parent depends on the EIM of the I/Os of children.
    for (c <- Driver.sortedComps) {
      val moduleOutput = process(c)
      tempSignalFile.addModuleSignals(c, moduleOutput.tempSignals)
      for (mem <- moduleOutput.memoryInstances)
        memInstanceFile.write(s"${mem.componentOf.getPathName(".")}.${mem.name}\n")
    }
    tempSignalFile.close()
    memInstanceFile.close()
  }

  private def createMemoryInstanceFile(): java.io.FileWriter = {
    createOutputFile(Driver.topComponent.name  + ".mem")
  }

  private def createTempSignalFile(): RExpressionWriter = {
    new RExpressionWriter(Driver.topComponent.name + ".R")
  }

  private def process(comp: Module): ModuleOutput = {
    ChiselError.info(s"Processing module ${comp.getPathName(".")}")
    val termFile = createModuleTermFile(comp)
    val eimFile = createModuleEimFile(comp)
    val signalFile = createModuleSignalDependencyFile(comp)
    val inputsFile = createModuleInputsFile(comp)
    val regFile = createModuleRegistersFile(comp)
    val analysis = (new ModuleAnalysis /: ModuleNodeWalk.forward(comp)) {
      case (analysis, node) => analysis.withAnalysisOf(node)
    }
    val eimValues = analysis.calculateEimValues(eimInputValues)
    addEimInputValues(comp, eimValues)
    writeTerms(termFile, analysis.termExpressions)
    termFile.close()
    writeEimValues(eimFile, eimValues)
    eimFile.close()
    writeSignalDependencies(signalFile, analysis.signalDependencies)
    signalFile.close()
    writeInputs(inputsFile, comp)
    inputsFile.close()
    writeRegisters(regFile, comp)
    regFile.close()
    analysis.moduleOutput
  }

  private def createModuleTermFile(comp: Module): java.io.FileWriter = {
    createOutputFile(comp.getPathName(".") + ".terms")
  }

  private def createModuleEimFile(comp: Module): java.io.FileWriter = {
    createOutputFile(comp.getPathName(".") + ".eim")
  }

  private def createModuleSignalDependencyFile(comp: Module): java.io.FileWriter = {
    createOutputFile(comp.getPathName(".") + ".deps")
  }

  private def createModuleInputsFile(comp: Module): java.io.FileWriter = {
    createOutputFile(comp.getPathName(".") + ".inputs")
  }

  private def createModuleRegistersFile(comp: Module): java.io.FileWriter = {
    createOutputFile(comp.getPathName(".") + ".regs")
  }

  private def writeTerms(termFile: java.io.FileWriter,
    termExpressions: TraversableOnce[String]): Unit = {
    termFile.write(termExpressions.mkString("\n") + "\n")
  }

  private def addEimInputValues(
    comp: Module,
    eimValues: EnergyImpactMatrix
  ): Unit = {
    eimInputValues(comp.getPathName(".")) = (for {
      (_, wire) <- componentInputs(comp)
    } yield wire -> eimValues(wire)).toMap
  }

  private def componentInputs(comp: Module) = for {
    in@(_, wire) <- comp.wires
    if wire.dir == INPUT && !wire.prune
    if !isLiteral(wire)
  } yield in

  private def writeEimValues(
    eimFile: java.io.FileWriter,
    eim: EnergyImpactMatrix
  ): Unit = {
    val vars = eim.keys.toVector
    eimFile.write("\t")
    eimFile.write(vars.map(_.name).mkString("\t") + "\n")
    for (v <- vars) {
      eimFile.write(s"${v.name}\t")
      eimFile.write(vars.map(eim(v)(_)).mkString("\t") + "\n")
    }
  }

  private def writeSignalDependencies(
    signalFile: java.io.FileWriter,
    signals: Set[Node]
  ): Unit = {
    signalFile.write(signals.map(_.name).mkString(" "))
  }

  private def writeInputs(file: java.io.FileWriter, mod: Module): Unit = {
    file.write(componentInputs(mod).map(w => w._1 + " " + w._2.needWidth()).mkString("\n") + "\n")
  }

  private def componentRegisters(comp: Module): mutable.Set[Reg] = for {
    node <- comp.nodes
    reg <- node match { case r: Reg => Some(r); case _ => None }
  } yield reg

  private def writeRegisters(file: java.io.FileWriter, mod: Module): Unit = {
    file.write(componentRegisters(mod).map {
      r => s"${r.name}@${r.needWidth()}"
    }.mkString("\n") + "\n")
  }
}

object ModelTermBackend {


  private class RExpressionWriter(
    filename: String
  ) extends FileSystemUtilities {
    private val out = createOutputFile(filename)
    private val genFunPrologue =
"""
add.temp.signals <- function(signals, module) {
    module.list <- list(
"""
    private val genFunEpilogue =
"""
    )
    module.list[[module]](signals)
}"""

    private val genericFunction = new StringBuilder(genFunPrologue)

    def addModuleSignals(c: Module,
      tempSignals: Vector[NodeExpression]): Unit = {
      createModuleFunction(c, tempSignals)
      addModuleFunctionToGenericFunction(c)
    }

    private def createModuleFunction(c: Module,
      tempSignals: Vector[NodeExpression]): Unit = {
      out.write(s"add.temp.signals.${c.getPathName(".")} <- function(signals) {\n");
      out.write("    within(signals, {\n")
      for (signal <- tempSignals)
        out.write("        " + signal.name +  " <- " + signal.expression + "\n");
      out.write("    })\n}\n");
    }

    private def addModuleFunctionToGenericFunction(c: Module): Unit = {
      genericFunction ++= s"        ${c.getPathName(".")}=add.temp.signals.${c.getPathName(".")},\n"
    }

    def close(): Unit = {
      genericFunction.delete(genericFunction.length - 2, genericFunction.length)
      genericFunction ++= genFunEpilogue
      out.write(genericFunction.toString())
      out.close()
    }
  }

  val lit: mutable.Map[Node, Chisel.Literal] = mutable.HashMap.empty
  private def performLiteralPropagation(top: Module): Unit = {
    for (n <- LiteralNodeWalk) { 
//      ChiselError.info(s"BFS node: ${n.name}")
      n match {
      case l: Chisel.Literal =>
        lit += l -> l
      case n =>
        if (n.inputs.size > 0 && n.inputs.forall(isLiteral)) {
//          ChiselError.info(s"Found derived literal ${n.name} [${n.getClass.getSimpleName}]in ${n.componentOf.getPathName(".")}")
          lit += n -> (n match {
            case o: Op if n.inputs.size == 2 =>
              val op1 = lit(o.inputs(0))
              val op2 = lit(o.inputs(1))
              if (Op.logicalChars.findFirstIn(o.op).nonEmpty)
                LogicalOp(op1, op2, o.op).litOf
              else
                BinaryOp(op1, op2, o.op).litOf
            case o: Op if n.inputs.size == 1 =>
              UnaryOp(lit(o.inputs(0)), o.op).litOf

            case m: Mux => Multiplex(lit(m.inputs(0)),
              lit(m.inputs(1)), lit(m.inputs(2))).litOf

            case b: Bits => lit(b.inputs(0))

            case e: Extract if e.inputs.size == 3 =>
              NodeExtract(lit(e.inputs(0)), lit(e.inputs(1)),
                lit(e.inputs(2))).litOf

            case r: Reg => r.next.litOf

            case b: Binding => lit(b.targetNode)

            case n =>
              throw new UnsupportedOperationException(
                s"Literal propagation NYI for node ${n.name} [${n.getClass.getSimpleName}, ${n.inputs.size} inputs]")
          }).ensuring { _ != null }
        }}
    }
  }
  private def isLiteral(n: Node) = lit.contains(n)

  private def pruneDeadSubgraphs(comp: Module): Int = {
    var i = 0
    var j = 0

    def nodeInfo(n: Node): String = s"${n.name} [${n.getClass.getSimpleName}] in ${n.componentOf.getPathName(".")}"

    val revTopSort = mutable.Stack.empty[Node]
    DesignNodeWalk.forward.foreach( n => {
      // if (n.driveRand) {
      //   if (n.name == "T592")
      //     ChiselError.info(s"Pruning ${nodeInfo(n)}")
      //   n.prune = true
      //   n.consumers.foreach(_.prune = true)
      //   i = i + n.consumers.size
      // }
      revTopSort.push(n)
    })
    j = revTopSort.size

    for (n <- revTopSort.find(_.name == "pcr_io_status_ef")) {
      ChiselError.info(s"Visited ${nodeInfo(n)}")
      ChiselError.info(s"Its outputs: ${n.consumers.map(n2 => nodeInfo(n2)).mkString("[", ", ", "]")}")
      ChiselError.info(s"Its outputs' inputs: ${n.consumers.map(n2 => n2.name + n2.inputs.map(n3 => nodeInfo(n3)).mkString("[", ", ", "]")).mkString("[", ", ", "]")}")
    }
    for {
      node <- revTopSort
      if node.consumers.isEmpty || node.consumers.forall(_.prune)
    } node match {
      case b: Bits if b.dir == OUTPUT && b.componentOf == Driver.topComponent =>
        //We do not prune top-level outputs! :)
      case _ =>
//        ChiselError.info(s"Pruning node ${nodeInfo(node)}")
        node.prune = true
        i = i + 1
    }


    // val unconnectedInputs = for {
    //   comp <- Driver.components
    //   (_, w) <- comp.wires if w.dir == INPUT && w.driveRand
    // } yield w

    // val walk = new NodeWalk with ForwardNodeWalk {
    //   def getRoots = unconnectedInputs.toVector
    //   def canVisit(n: Node) = true
    // }
    // for (node <- walk) {
    //   if (!node.prune) {
    //     ChiselError.info(s"Pruning node ${nodeInfo(node)} with an unconnected ancestor")
    //     node.prune = true
    //     i = i + 1
    //   }
    // }



    // for (node <- DesignNodeWalk.forward) {
    //   j = j + 1
    //   ChiselError.info(s"Visiting node ${node.name} in ${comp.getPathName(".")}")
    //   node match {
    //     case b: Bits if b.dir == OUTPUT && b.componentOf == Driver.topComponent =>
    //       //We do not prune top-level outputs! :)
    //     case _ =>
    //       if (node.consumers.isEmpty || node.consumers.forall(_.prune)) {
    //         ChiselError.info(s"Pruning node ${node.name}")
    //         node.prune = true
    //         i = i + 1
    //       }
    //   }
    // }
    ChiselError.info(s"Checked $j nodes")
    i
  }

  private class NodeExpression(
    val name: String, val expression: String)

  private abstract sealed class NodeFactor(val n: Node) {
    def fullExpression(s: String): String
  }
  private case class HD(override val n: Node) extends NodeFactor(n) {
    override def toString = s"HD(${n.name})"
    override def fullExpression(s: String) = s"HD($s)"
  }
  private case class Change(override val n: Node) extends NodeFactor(n) {
    override def toString = s"change(${n.name})"
    override def fullExpression(s: String) = s"change($s)"
  }
  private case class Literal(override val n: Node) extends NodeFactor(n) {
    override def toString = n.name
    override def fullExpression(s: String) = s
  }

  private implicit def getFactor(node: Node): NodeFactor = {
    if (node.needWidth() == 1) Literal(node)
    else HD(node)
  }

  //Rename to 'ModuleAnalysis'?
  private class ModuleOutput(
    val tempSignals: Vector[NodeExpression] = Vector.empty,
    val memoryInstances: Vector[Node] = Vector.empty) {
    def updatedWith(
      newTempSignals: Traversable[NodeExpression],
      removedTempSignals: Set[String],
      na: NodeAnalysis
    ): ModuleOutput = {
//      tempSignals = 
        new ModuleOutput(
          tempSignals.filterNot(removedTempSignals contains _.name) ++ newTempSignals,
//          tempSignals ++ newTempSignals,
        if (na.isMemInstance) memoryInstances :+ na.n
        else memoryInstances)
    }
  }

  private class EimData(
    val impactFactors: Map[Node, Double],
    val energyWeights: Map[Node, Double],
    val energyValue: Int)


  private type Term = Vector[NodeFactor]
  private def term(f: NodeFactor*) = Vector(f: _*)
  private def termExpression(term: Term): String =
    term.mkString(":") + term.map("@" + _.n.needWidth()).mkString
  private def fullExpression(t: Term,
    expressionMap: Node => String): String = {

    t.map(f => try {
      f.fullExpression(expressionMap(f.n))
    } catch {
      case ex: NoSuchElementException =>
        ChiselError.info(s"No expression set for ${f.n.name} [${f.n.getClass.getSimpleName}]")
        throw ex
    }).mkString(":")
  }
  private type TermList = Vector[Term] //Map[String, (String, Vector[Node])]
  private def noTerms = Vector.empty
  private def terms(t: Term*): TermList = Vector(t: _*)
  private type ImpactFactorList = Seq[(String, String, Double)]
  private type EnergyImpactMatrix = Map[Node, Map[Node, Int]]

  private class NodeAnalysis(
    val n: Node,
    val nodeExpression: String,
    val terms: Either[Node, TermList],
    val eimData: EimData,
    val isMemInstance: Boolean,
    val dependencies: Set[Node]
  ) {
    def termExpressions =
      for (term <- terms.right.get) yield termExpression(term)
    def nodesUsed =
      for (term <- terms.right.get; factor <- term) yield factor.n
  }

  private class ModuleAnalysis(
    val termExpressions: Set[String] = Set.empty, 
    nodeExpressions: Map[Node, String] = Map.empty,
    eimData: Map[Node, EimData] = Map.empty, 
    allNodesUsed: Set[Node] = Set.empty,
    aliases: Map[Node, Node] = Map.empty,
    expressionDefiningNodes: Map[String, Vector[Node]] = Map.empty,
    reverseOrderedNodes: immutable.Stack[Node] = immutable.Stack.empty,
    namedNodeDependencies: Map[Node, Set[Node]] = Map.empty,
    val moduleOutput: ModuleOutput = new ModuleOutput
  ) {

    def withAnalysisOf(n: Node): ModuleAnalysis = {
      this.updatedWith(nodeAnalysis(n))
    }

    private def nodeAnalysis(n: Node): NodeAnalysis = {
      val na = new NodeAnalysis(n, getNodeExpression(n), getNodeTerms(n),
        getEimData(n), isExternalMemoryInstance(n), getNodeDependencies(n))
//      ChiselError.info(s"Node expression = ${na.nodeExpression}\n")
      na
    }

    private def getNodeExpression(n: Node): String = {
      if (isLiteral(n)) lit(n).litValue().toString
      // if (n.isLit) n.litValue().toString
      else if (isTemporaryNode(n)) {
        val namedOuts = namedOutputs(n)
        if (namedOuts.isEmpty) createTemporaryNodeExpression(n)
        else namedOuts.head.name
      } else n.name
    }

    private def createTemporaryNodeExpression(n: Node): String = {
      val fun = n match {
      //TODO: Map different node operations to R expressions.
      // This looks like it is going to get ugly...
        case o: Op if o.inputs.length == 2 => o.op match {
          case "+" => "lsum"
          case "-" => "lsub"
          case "*" => "lmul"
          case "/" => "ldiv"
          case "&" => "land"
          case "|" => "lor"
          case "^" => "lxor"
          case "##" => "lcat"
          case "<<" => "lshiftl"
          case ">>" => "llshiftr"
          case "s>>" => "lashiftr"
          case "==" => "leq"
          case "!=" => "lneq"
          case "<" => "llt"
          case "<=" => "lle"
          case "s*u" => "lsignedmul"
          case "s<" => "lsignedlt"
          case x =>
            throw new UnsupportedOperationException(s"NYI: binary op '$x'")
        }
        case o: Op if o.inputs.length == 1 => o.op match {
          case "~" => "lneg"
          case "Log2" => "llog2"
          case "OHToUInt" => "lonehot2uint"
          case x => 
            throw new UnsupportedOperationException(s"NYI: unary op '$x'")
        }
        case e: Extract if e.inputs.length == 2 => "lbit"
        case e: Extract if e.inputs.length == 3 => "lbits"

        case m: Mux => "lmux"

        case i@(_: UInt | _: SInt) if i.inputs.size == 1 => "identity"

        case (_: MemAccess | _: Mem[_] | _: ROMData) =>
          return s"stop('invalid use of memory I/O ${n.getClass.getSimpleName}')"

        case romr: ROMRead =>
          return s"romread(${nodeExpressions.getOrElse(romr.addr, getNodeExpression(romr.addr))}, ${romr.rom.sparseLits.map(_._2).mkString(",")})"

        case _: Assert =>
          return "stop('invalid use of assertion node')"

        case _ =>
          throw new UnsupportedOperationException(
            s"NYI: ${n.getClass.getName} with num inputs ${n.inputs.size}!")
      }
      s"$fun(${n.inputs.map(n => nodeExpressions.getOrElse(n, getNodeExpression(n))).mkString(", ")})"
    }

    private def getNodeTerms(n: Node): Either[Node, TermList] = n match {
      case reg: Reg =>   generateRegTerms(reg)
      case bits: Bits => generateBitsTerms(bits)
      case mux: Mux => generateMuxTerms(mux)
      case op: Op => generateOpTerms(op)
      case extract: Extract => generateExtractTerms(extract)
      case binding: Binding => generateBindingTerms(binding)

        //      case read@(_: MemSeqRead | _: MemRead) =>

        //No terms for memory reads! They are not included in
        //VCD. Just pray that they will be represented at some point
        //eventually... Fix if it turns out to be a gruesome omission.

      case _ => Right(noTerms) //(Set.empty, Set.empty)
      //TODO: Copy existing code here.
      //TODO: Implement the MUX coalescence.
//        Map.empty
    }

    private def generateRegTerms(reg: Reg): Either[Node, TermList] = {
      Right(terms(term(getFactor(reg))))
//      Right(Map(getFactor(reg) -> Vector(reg)))
    }

    private def generateBitsTerms(bits: Bits): Either[Node, TermList] = {
      if (bits.prune || isTemporaryNode(bits)) Right(noTerms)
      else Right(terms(term(getFactor(bits))))
//Map(getFactor(bits) -> Vector(bits))
    }

    private def generateMuxTerms(mux: Mux): Either[Node, TermList] = {
      // TODO:
      val selection = mux.inputs(0)
      if (selection.name == "reset") Right(noTerms)
      else if (isUnaliasedTemporaryNode(mux)) Right(noTerms)
      else Right(terms(term(getFactor(mux), Change(findSelectionLub(mux)))))

//      Map(s"${getFactor(mux)}:change(${selection.name})" -> Vector(mux, selection))
    }

    private def findSelectionLub(mux: Mux): Node = {
      /*
       1. Find all direct-path ancestor muxes. 
       2. Find all their sources.

       3. Walk backwards in some fashion along the select lines,
       trying to find out whether the MUX tree is controlled by a
       single select signal, and if so which.
       
       MUX trees appear to become identical through either MuxCase,
       MuxLookup or nested Mux calls. Possible MUX tree selection
       drivers:
       
       1. A boolean vector (wire)
       2. Comparison instructions using some same variable in each
       comparison.
       3. A mix
       4. Comparison instructions with different variables.

       The MUX tree will always be described through a set of
       booleans. If the boolean vector is one-hot by construction, it
       would be beneficial to find the source. Otherwise, changes
       further up would be dependent on the bottom-most control signal
       for full propagation out of the MUX. Then, changes in the
       vector would have different outcome depending on what changed,
       and using change in the vector would not be that
       meaningful. The only way it could be meaningful is if we assume
       that MUX trees are created by the designer in such a way that
       two cases will not happen at the same time, so that order does
       not matter. This is common, I guess---for instance, there is a
       Chisel object Mux1H to construct these trees---but it seems
       risky (for quality) to assume this always holds (and Chisel IR
       does not).

       Therefore, I think recognizing the second case is the most
       important. Let's give that a go.

       Found another case: multiple values applicable for one
       branch. This adds a second layer: an 'or'. To find this, we
       must recognize the LUB at several layers.
 */
      
      val selectionSignals = mutable.ArrayBuffer[(Node, Int)]()
      val unprocessedMuxes = mutable.Queue[Node](mux)
      var i = 0
      while (!unprocessedMuxes.isEmpty) {
        val curMux = unprocessedMuxes.dequeue()
        selectionSignals += ((curMux.inputs(0), i))
        i += 1
        unprocessedMuxes.enqueue(
          curMux.inputs.drop(1).filter(_.isInstanceOf[Mux]): _*)
      }

      val muxCount = selectionSignals.size
      val lubCandidates =
        mutable.Map.empty[Node, Set[Int]].withDefaultValue(Set.empty)
      var lub: Option[(Node, Set[Int])] = None
      while (lub.isEmpty && !selectionSignals.isEmpty) {
        for ((sel, i) <- selectionSignals) lubCandidates(sel) += i
        val parents = for {
          (sel, i) <- selectionSignals
          if !sel.isInstanceOf[Reg] && !sel.isInstanceOf[Mem[_]]
          in <- sel.inputs
          if !isLiteral(in) && in.componentOf == mux.componentOf
        } yield (in, i)

        selectionSignals.clear
        selectionSignals ++= parents
        lub = lubCandidates.find(_._2.size == muxCount)
      }
      if (lub.isEmpty) mux.inputs(0)
      else lub.get._1
    }

    private def generateOpTerms(op: Op): Either[Node, TermList] = {
      if (isUnaliasedTemporaryNode(op)) Right(noTerms)//Map.empty
      else Right(terms(term(getFactor(op))))
        //Map(getFactor(op) -> Vector(op))
    }


/*  Food for thought:
       Extract - does that also represent memory access?
        ( I guess MemAccess does that... )
 */
    private def generateExtractTerms(extract: Extract): Either[Node, TermList] = {
//      if (isUnaliasedTemporaryNode(extract)) (Vector.empty, Set.empty)
//      else

      //Include temporary extract nodes: I would suspect there are
      //only temporary ones anyway. To make EIM analysis accurate, we
      //need separate variables for extract signals, so in this sense
      //temporary nodes saves us the work.

      //Small challenge: we would like to replace the lbits call-term
      //with just the node name to let EIM represent the bit
      //extraction explicitly as a separate node. This makes it
      //possible to differentiate between the extracted bits and the
      //entire signal when selecting terms. However, there may be
      //several identical bit-extractions, so we would end up with a
      //large number of temporaries for the same expression if
      //we generated a term for each duplication.

      //This may be solvable by using a list of term expressions for
      //which we already have generated a temporary signal. A map
      //should also be kept which links nodes to any representative
      //node, to assist EIM computation in associating weight with
      //the appropriate node.

      //Damnit - alias detection based on equal term expressions is
      //not possible when we swap out the extract term to be the name
      //of the temporary signal. We need to map aliases through
      //expressions. Or do we, perhaps, need to detect aliases based
      //on the node itself?  For instance, extraction from signals
      //creates aliases. So, two scenarios:
      // - Two nodes create the same expression. If the expression
      // uses just a single node, then these nodes are aliases. If the
      // expression is composed, nodes are not necessarily aliases?
      // E.g. mux: lbits(lcat(x, 0), 2, 3):change(lbits(land(y, z), 4,
      // 5) created twice. T1:change(T2) == T3:change(T4) => T1 == T3,
      // T2 == T4. So, we need:
      //     - Actual expression (T1:change(T2)), to emit
      //      - Full expression (lbits(...):lnum(...), to identify
      //      aliases.
      //      - A sequence of nodes used, to connect aliases.io_out
      //  We need to store:
      //     - Full expressions, for later comparisons.
      //     - The sequence of nodes used, which defines alias canon.
      //
      // This is getting a bit ugly..  Instead of generating a
      // separate string expression for expanded values and unexpanded
      // values, we may be able to generate the two expression from
      // the same source of nodes. Vector of nodes should be combined
      // (with ':'), using HD if the arguments have width > 1. The
      // term to use in termlist should use node names, whereas the
      // term to check for equivalence should use the R
      // expression. Potential problem: The R expression of
      // extractions which are stored in named nodes will be the name
      // of the node. This technique will therefore not identify
      // aliases between temporaries and named nodes.

      //Maybe we need to edit the flow a bit: the generateTerm can
      //return an Either[Node, TermList]. A Left(Node) result will
      //identify an alias of the current node, if any. A
      //Right(TermList) result indicates that the Node is unique, and
      //returns the terms it generates.
      //
      // Problem with this strategy: Is the result necessarily
      // either/or? Can we have identified an alias for this node, and
      // still want to generate some other terms? For instance, for a
      // MUX - if the output is an alias to another output, but the
      // selection is different, we would want a new term but with one
      // alias. On the other hand, in this case the output cannot
      // really be an alias of anything yet - the MUX produces this
      // signal. Something else would have to be the alias of THIS
      // output. If the selection was an alias, this should have been
      // detected while processing the selection node (which happens
      // before due to the ordered node walk). So - production nodes
      // DO NOT yield aliases (unless we apply more "sophisticated"
      // analysis like recognizing B = A + 0). uuunless both inputs
      // are aliases, and have been inputs to an identical node
      // before. I think I will just focus on supporting what I KNOW
      // is an issue - extraction of bits. This should be well
      // supported using the Either-strategy.
      //

      if (extract.inputs(0).needWidth() > 1) {
//          Map(getFactor(extract) -> Vector(extract))
        if (allNodesUsed.contains(extract.inputs(0)))
          Right(terms(term(getFactor(extract))))
        else
          Right(noTerms)
//         if (extract.inputs.length == 3) {
//           Right(term
//           //Map(getFactor(extract) -> (extract.name, Vector(extract)))
// //          (Set(lbitsCall(extract.inputs)), Set(extract.inputs(0)))
//         } else {
// //          (Set(lbitCall(extract.inputs)), Set(extract.inputs(0)))
//         }
      } else if (isTemporaryNode(extract)) Left(extract.inputs(0))//Map.empty
      else Right(terms(term(getFactor(extract.inputs(0)))))//???? WHat?
    }

    // private def lbitsCall(inputs: IndexedSeq[Node]): String = {
    //   s"lbits(${inputs(0).name}, ${inputs(1).litValue()}, ${inputs(2).litValue()})"
    // }
    // private def lbitCall(inputs: IndexedSeq[Node]): String = {
    //   s"lbit(${inputs(0).name}, ${inputs(1).litValue()})"
    // }

    private def generateBindingTerms(binding: Binding): Either[Node, TermList] = {
//      val s: TermList = Map((binding.name, (binding.name, Vector(binding))))
      // ChiselError.info(s"Generating binding terms $s")
      // Right(s)
      Right(terms(term(getFactor(binding))))
    }

    private def getEimData(n: Node): EimData = {
      //TODO: Calculate impact factors, weights, and energy values.
      SimpleEimAnalysis.analyse(n)
    }

    private def isExternalMemoryInstance(n: Node): Boolean = n match {
      case m: Mem[_] if !m.isInline => true
      case _ => false
    }

    private def getNodeDependencies(n: Node): Set[Node] = {
      if (isTemporaryNode(n)) {
        val namedAliases = namedOutputs(n)
        if (namedAliases.isEmpty)
          n.inputs.filter(i => !isLiteral(i) && i.name != "reset").
            flatMap(namedNodeDependencies).toSet
        else namedAliases.toSet
      } else Set(n)
    }

    private def updatedWith(na: NodeAnalysis): ModuleAnalysis = {

      na.terms match {
        case Left(alias) =>
          ChiselError.info("Found alias ${alias.name} -> ${na.n.name}")
          if (shouldReverseAlias(nodeExpressions)(na.n, alias)) {
            ChiselError.info(s"Reversing alias ${alias.name} -> ${na.n.name}")
           val (aliases, exprDefs, termExprs) = reverseAlias(na.n, alias)
            new ModuleAnalysis(termExprs, nodeExpressions, eimData,
              allNodesUsed, aliases, exprDefs,
              na.n +: reverseOrderedNodes,
              updatedNodeDependencies(na), moduleOutput)
          } else {
            new ModuleAnalysis(
              termExpressions, nodeExpressions, eimData, allNodesUsed,
              aliases + (na.n -> aliases.getOrElse(alias, alias)),
              expressionDefiningNodes,
              na.n +: reverseOrderedNodes,
              updatedNodeDependencies(na),
              moduleOutput)
          }

        case Right(termlist) =>
          val newNodeExpressions = updatedNodeExpressions(na)
          val (newAliases, newExpressionDefiningNodes, newTerms) =
            updatedAliasStructures(termlist, newNodeExpressions)
          val newModuleOutput = updatedModuleOutput(
            newNodeExpressions, na, newAliases)
           new ModuleAnalysis(
             newTerms,
             newNodeExpressions,
             updatedEimData(na),
             updatedNodesUsedList(na, newAliases),
             newAliases,
             newExpressionDefiningNodes,
             na.n +: reverseOrderedNodes,
             updatedNodeDependencies(na),
             newModuleOutput)
      }//termlist.partition(hasAlias)
    }
    //       val newNodeExpressions = updatedNodeExpressions(na)
    //       val newModuleOutput = updatedModuleOutput(newNodeExpressions, na)
    //       new ModuleAnalysis(
    //         updatedTermExpressions(na),
    //         newNodeExpressions,
    //         updatedEimData(na),
    //         updatedNodesUsedList(na),
    //         updatedAliases(na),
    //         updatedExpressionDefiningNodes(na),
    //         newModuleOutput)
    // }

    private def updatedAliasStructures(termlist: TermList,
      expressions: Map[Node, String]
    ): (Map[Node, Node], Map[String, Vector[Node]], Set[String]) =
      ((aliases, expressionDefiningNodes, termExpressions) /: termlist) {
        case ((aliases, exprDefs, termExprs), term) =>
          findAliases(term, exprDefs, expressions) match {
            case Left(nodes) =>
//              ChiselError.info("Found aliases")
              if (term.size == 1 && nodes.size == 1 &&
                shouldReverseAlias(expressions)(term(0).n, nodes(0).n)) {
                //We found an alias, but the expression definition is
                //this node! This happens when a temporary node has a
                //named output, where we want to include the temporary
                //(because it is named) and not just the named node
                //(because the temporary could be driving other
                //signals as it is higher in the hierarchy, so would
                //want its EIM values). However, we would rather emit
                //a dependency on the named signal than on the
                //temporary node. Thus, we alter the alias definitions
                //to make the temporary node an alias of the named
                //node. EIM calculations should then attribute the
                //values to the named node instead.

                val temp = nodes(0).n
                val named = term(0).n
                reverseAlias(named, temp)
                //(aliases, exprDefs, termExprs)

                // (aliases ++ (nodes zip term.map(_.n)),
                //   exprDefs + (named.name -> term.map(_.n)),
                //   termExprs.map(
                //     _.replace(temp.name, named.name)))
              } else {
                (aliases ++ (term.map(_.n) zip nodes),
                  exprDefs, termExprs)
              }
            //              term zip nodes foreach { aliases(_._1) = _._2 }
            case Right(fullExpression) =>
//              ChiselError.info(s"Adding term ${termExpression(term)}")
              (aliases, exprDefs + (fullExpression -> term.map(_.n)),
                termExprs + termExpression(term))
              //              expressionDefiningNodes(fullExpression) = term
          }
      }

    private def findAliases(
      term: Term,
      expressionDefinitions: Map[String, Vector[Node]],
      nodeExpressions: Map[Node, String]
    ): Either[Vector[Node], String] = {
      //Create a function from our nodeExpressions map
      val nodeExpr = nodeExpressions.applyOrElse(_: Node, getNodeExpression)
      val expr = fullExpression(term, nodeExpr)
      expressionDefinitions.get(expr) match {
        case Some(nodes) => Left(nodes)
        case None => Right(expr)
      }
    }

    private def shouldReverseAlias(expressions: Map[Node, String])
                                  (node: Node, alias: Node): Boolean = {
      !isTemporaryNode(node) && isTemporaryNode(alias) &&
      expressions(alias) == node.name
    }

    private def reverseAlias(
      node: Node, alias: Node
    ): (Map[Node, Node], Map[String, Vector[Node]], Set[String]) = {
      (aliases + (alias -> node) - node,
        expressionDefiningNodes + (node.name -> Vector(node)),

//        termExpressions.map(_.replace(alias.name, node.name)))
        //OH NO
        //By replacing for instance "T3" with "io_a", uses of the
        //signal "T35" will be swapped out with "io_a5".
        //But stand back.
        //I know regular expressions!
        termExpressions.map(
          s"\\b${alias.name}\\b".r.replaceAllIn(_, node.name)))
    }

    // private def hasAlias(term: Term): Boolean = {
    //   expressionDefiningNodes.contains(
    //     fullExpression(term, nodeExpressions))
    // }
    private def updatedNodeExpressions(na: NodeAnalysis) = {
      // if (aliases contains na.n) nodeExpressions else
      //nodeExpressions + (na.n -> na.nodeExpression) Why was the
      //above check included? For one thing, aliases are not
      //identified by parents so the check would always be
      //false. Additionally, why would we not want the node expression
      //included? Maybe to support both named and unnamed aliases....
      //Update: I think maybe it was there because we want to use the
      //alias defining node expression? Still seems erroneous, though.
      nodeExpressions + (na.n -> na.nodeExpression)
    }
    private def updatedModuleOutput(
      exprs: Map[Node, String],
      na: NodeAnalysis,
      aliases: Map[Node, Node]
    ): ModuleOutput = {
      val newTempSignals = for {
        s <- na.nodesUsed
        if !aliases.contains(s)
        if isTemporaryNode(s)
      } yield new NodeExpression(s.name, exprs(s))
      val removedTempSignals = for {
        s <- aliases.keys
        if isTemporaryNode(s)
      } yield s.name

      moduleOutput.updatedWith(newTempSignals,
        removedTempSignals.toSet, na)
    }

    // private def updatedTermExpressions(na: NodeAnalysis): Set[String] = {
    //   //termExpressions ++ na.termExpressions
    //   //HOHO: Sjekk Top.RocketTile.core.FPU.dfma.eim, fra modul
    //   //FPUFMAPipe_1. io_out_bits_data er et alias av R25, men begge
    //   //to er med som variabler i ledd. Slik bør det ikke være! 

    //   //Hvis na.n er et alias for noe, burde leddet strengt kanskje
    //   //endres slik at navnet på na.n byttes ut med na.n sitt alias.
    //   aliases.get(na.n) match {
    //     case Some(alias) =>
    //       termExpressions ++ na.termExpressions.map(
    //         _.replace(nodeExpressions(na.n), nodeExpressions(alias.n)))

    //     case None => termExpressions ++ na.termExpressions
    //   }
    // }

    private def updatedEimData(na: NodeAnalysis) = {
      eimData + (na.n -> na.eimData)
    }

    private def updatedNodesUsedList(na: NodeAnalysis, aliases: Map[Node, Node]) = {
      allNodesUsed ++ na.nodesUsed.filter(!isLiteral(_)) -- aliases.keys
//        n => !isLiteral(n) && !aliases.contains(n))
    }

    private def updatedNodeDependencies(na: NodeAnalysis
    ): Map[Node, Set[Node]] = {
      namedNodeDependencies + (na.n -> na.dependencies)
      // if (na.dependencies.isEmpty) namedNodeDependencies
      // else namedNodeDependencies + (na.n -> na.dependencies)
    }

    // private def updatedAliases(na: NodeAnalysis) = {
    //   aliases ++ (for {
    //     (expr, nodes) <- na.terms
    //     if (expressionDefiningNodes.contains(expr))
    //  } yield expressionDefiningNodes(expr).zip(nodes._2)).flatten.toMap
    // }

    // private def updatedExpressionDefiningNodes(na: NodeAnalysis) = {
    //   expressionDefiningNodes ++ na.terms.filter(!expressionDefiningNodes.contains(_._1)).map(_._1 -> _._2._2)
    // }

    def calculateEimValues(
      eimInputValues: mutable.Map[String, EnergyImpactMatrix]
    ): EnergyImpactMatrix = {
      //TODO: Traverse nodes, and build EIM.
      /*
       Things to keep in mind:
       - Detection of aliases among children. Especially, this is
         important for bit extraction. 
       - Submodule input handling. If a signal has a path to a
         submodule, we really should include the importance this input
         has in covering its submodule. 

       

       Really, I need a plan before plunging ahead.

       What do I want?
       - The EIM value of each node. This is calculated as:
           E(n) = E'(n) + sum{all children c}(impact(nc)*E(c))

       - The amount of that EIM value which comes from the children
       must also be stored as separate entries in the matrix.

       - The contribution from nodes who are aliases of other nodes
       should be added to the defining node. 
       

       How do I go about it?
       - I process each node in backwards direction. That way, all
       dependent node results will be available.  UPDATE: With parents
       perhaps being aliased to their children, there may be some
       issues with assumptions on traversal order..

       - To handle nodes which are not included in terms, we calculate
       their energy impact like every other node and just avoid
       creating mappings between it and its parents when they are
       processed. 

       - Children c of node n must be processed in one of four ways:
         1. The child is included in a term. In this case, we add its
         contribution to eim(n)(c) and eim(n)(n)

         2. The child is an alias of another node in a term. In this
         case, we add its contribution to eim(n)(alias(c)) and
         eim(n)(n).

         3. The child has no connection with any term. In this case,
         we do not add a mapping for the edge, but just add its
         contribution to eim(n)(n).
       
         4. The child is the input of a submodule.
       
         5. The child is an output of this module.
      */
      
      val eimValues = mutable.Map.empty[Node, mutable.Map[Node, Double]]
      val deadEnds = mutable.Set.empty[Node]
      for (n <- reverseOrderedNodes) {
//        ChiselError.info(s"calc EIM for node ${n.name}")
        val eimd = eimData(n)
        if (n.consumers.isEmpty)
          deadEnds += n
        val opValue = if (n.consumers.forall(deadEnds.contains(_))) 0.0
                      else if (aliases contains n) 0.0
                      else eimd.energyValue.toDouble
        val defN = aliases.getOrElse(n, n)
        // ChiselError.info(s"E(${n.name}) = $opValue")
        // ChiselError.info(s"\tconsumers: ${n.consumers.map(_.name).mkString(", ")}")
        if (!eimValues.contains(defN))
          eimValues(defN) = mutable.Map(defN -> opValue).withDefaultValue(0.0)
        else
          eimValues(defN)(defN) += opValue
//        eimValues(n) = mutable.Map(n -> opValue).withDefaultValue(0)
//        eimValues(n)(n) += eimd.energyValue
        for {
          c <- n.consumers
          if !c.prune
          defC = aliases.getOrElse(c, c)
          if defC != defN //If defC == defN, then the processing of
                          //defC has already added values to defN.
          if !defC.isInstanceOf[Delay] && !defC.isInstanceOf[PrintfBase]
          if defC.inputs.contains(defN) //When component outputs are
                                        //directly connected to
                                        //component inputs, inputs are
                                        //no longer roots. For now we
                                        //disregard such children as
                                        //contributing to the EIM of
                                        //the parent.
        } {
          // ChiselError.info(s"${n.name} -> ${c.name}")
          // ChiselError.info(s"${defN.name} -> ${defC.name}")
          val contrib = if (defC.componentOf == defN.componentOf) {
            //HOHO: Sjekk Top.RocketTile.core.FPU.dfma.eim, fra modul
            //FPUFMAPipe_1. io_out_bits_data, som er et alias av R25,
            //bidrar dobbelt til EIM for R25 enn dens faktiske
            //kost. Henger kanskje sammen med at både R25 og dets
            //alias er tatt med som variabler, se kommentar over (søk
            //etter HOHO).

            //UPDATE: NEIE! Ny teori. Både impactFactors og
            //energyWeights er 1, så eimValues(c) og
            //eimData(c).energyValue telles fullt. io_out_bits_data
            //brukes bare som output og har ingen andre etterfølgere,
            //så eimValues(c) == eimData(c).energyValue. Dermed blir
            //bidraget herfra til R25  2*eimData(c).energyValue.

            //Dette tyder på en mer fundamental glipp i hvordan
            //node-bidrag bør regnes ut. 

            try {            eimd.impactFactors(c) * eimValues(defC)(defC)
            } catch {
              case ex: NoSuchElementException =>
                if (eimValues.contains(defC)) {
                  if (eimValues(defC).contains(defC)) {
                    ChiselError.info(s"Could not find ${c.name} in impact factors")
                  } else {
                    ChiselError.info(s"Could not find ${defC.name} in eimValues(defC)")
                  }
                } else {
                  ChiselError.info(s"Could not find ${defC.name} (alias ${c.name}) in eimValues")
                  ChiselError.info(s"(for calculation of ${defN.name} (alias ${n.name}))")
                  ChiselError.info(s"prune: ${defC.prune}, driveRand: ${defC.driveRand}")
                }
                throw ex
            }
//            (eimd.impactFactors(defC) * eimValues.get(c).map(_(c)).getOrElse(0.0))
//             eimd.energyWeights(c) * eimData.get(c).map(_.energyValue).getOrElse(0)).toInt
          } else defC match {
            case w: Bits if w.dir == INPUT && w.name != "reset" && !w.prune =>
              eimInputValues.get(w.componentOf.getPathName(".")) match {
                //If the consumer is an input wire belonging to a
                //component for which we have already calculated EIM
                //values, it must be an input to a submodule. If so,
                //we use the submodule input value as the contribution.
                case Some(m) => m(w)(w)
                case _ => 0
              }
            case _ => 0
          }
          eimValues(defN)(defN) += contrib
          eimValues(defN)(defC) = contrib
//          if (!defC.isInstanceOf[Delay]) {
//             if (allNodesUsed.contains(defC)) {
// //                          ChiselError.info(s"Adding EIM edge ${n.name} -> ${c.name}")
//               eimValues(n)(c) += contrib
//             } else { aliases.get(c) match {
//               case Some(alias) if allNodesUsed.contains(alias) =>
//                 if (alias != n) {
// //                  ChiselError.info(s"Adding EIM edge ${n.name} -> ${alias.name}")
//                   eimValues(n)(alias) += contrib
//                 }
//               case _ if eimValues.contains(c) =>
//                 //The child is not used as a term. This means that we
//                 //should add all its used contributors as contributors
//                 //to this node, scaled with the appropriate impact
//                 //factor.
//                 for (n2 <- allNodesUsed) {
//                   if (!(eimValues(c).get(n).isEmpty || eimValues(c)(n) == 0.0)) //Assert correct order
//                     ChiselError.info(s"Whaaat, ${c.name} -> ${n.name} = ${eimValues(c)(n)}??")
// //                  ChiselError.info(s"Passing on EIM edge ${n.name} -> ${n2.name}")
//                   val inherit = eimd.impactFactors(c) * eimValues(c).getOrElse(n2, 0.0)
//                   if (inherit != 0.0) {
// //                    ChiselError.info(s"Node ${n.name} inheriting ${c.name} -> ${n2.name}")
//                     eimValues(n)(n2) += inherit
//                   }
//                 }
//               case _ =>
//             }}
// //          }
// //             for {
// //               alias <- aliases.get(c) //: Option[Node]
// //               if allNodesUsed contains alias
// //               if alias != n
// //             } {
// // //              ChiselError.info(s"Adding EIM edge ${n.name} -> ${alias.name}")
// //               eimValues(n)(alias) += contrib
//             // }
//           // }
// //          ChiselError.info(s"EIM(${n.name}) = ${eimValues(n)(n)}")
//         }
//       }
        }}
//       for ((alias, defNode) <- aliases)
//         eimValues(defNode)(defNode) += eimValues(alias)(alias)
      val seq = allNodesUsed.toVector
      seq.map(n1 =>
        n1 -> seq.map(n2 =>
          n2 -> math.round(eimValues(n1)(n2)).toInt).toMap).toMap

      // (for {
      //   n <- allNodesUsed
      //   c <- allNodesUsed
      // } yield eimValues(n)(c))

      // (for ((n, row) <- eimValues; if allNodesUsed.contains(n)) yield {
      //   ChiselError.info(s"Adding EIM row for node ${n.name}")
      //   n -> (for ((c, v) <- row; if allNodesUsed.contains(c)) yield {
      //     ChiselError.info(s"Adding EIM column for node ${c.name}")
      //     c -> v
      //   }).toMap
      // }).toMap





      //   case (tEim, n) =>
      //     tEim + (n -> ((Map(n -> eimd.energyValue) /: n.consumers) {
      //       case (eimRow, c) =>

      //         val diagUpdate = n -> (eimRow(n) + contrib)
      //         val childUpdate = c -> (
      //           if (allNodesUsed.contains(c)) {
      //             eimRow.getOrElse(c, 0) + contrib
      //           } else aliases.get(n) match {
      //             case Some(alias) if allNodesUsed contains alias =>
      //               eimRow.getOrElse(alias, 0) + contrib
      //             case _ =>
      //               0
      //           }
      //         )
      //         eimRow + diagUpdate + childUpdate
      //     }))
//          tEim + (n -> eimRow)
      // }

      // val eimValues = 
      // for {
      //   n <- reverseOrderedNodes
      //   child <- n.consumers
      // } {
      //   if (child.componentOf != n.componentOf) child match {
      //     case b: Bits if b.dir == INPUT =>
      //       // Handle a child input!
      //       eimInputValues(b.name)
      //   } else 

        
      // ) {
      //   for (child <- n.consumers
      // }
      // allNodesUsed.map(n1 => n1 -> allNodesUsed.map(n2 => n2 -> 0).toMap).toMap
    }

    def signalDependencies: Set[Node] = {
      allNodesUsed.flatMap(namedNodeDependencies)
    }

  }


    // val impactFactors: ImpactFactorList,
    // val energyWeights: Map[Node, Double],
    // val energyValue: Int)

  private def namedOutputs(node: Node): Traversable[Node] = {
    for {
      c <- node.consumers
      if (c match {
        case _: Bits => c.inputs.size == 1
//true
        case e: Extract => e.inputs(0).needWidth() == 1 //== e.needWidth()
        case _ => false
      })
      if c.componentOf == node.componentOf
      o <- if (isTemporaryNode(c)) { namedOutputs(c) } else Vector(c)
    } yield o
  }

  private def isTemporaryNode(n: Node): Boolean = {
    !n.named || n.name.isEmpty || isTemporaryNodeName(n.name)
  }

  private def isTemporaryNodeName(name: String): Boolean = {
    name.matches("^T\\d+");
  }

  private def isUnaliasedTemporaryNode(n: Node): Boolean = {
    isTemporaryNode(n) && namedOutputs(n).isEmpty
  }

  abstract class NodeWalk extends Iterator[Node] {
    protected val unvisitedNodes = mutable.Stack[Node](getRoots: _*)
    protected val visitedNodes = mutable.Set[Node]()

    override def hasNext = !unvisitedNodes.isEmpty
    override def next(): Node = {
      val n = unvisitedNodes.pop()
   //   if (n.name == "take_pc_wb5") {
   //     ChiselError.info(s"Visiting ${n.name} [${n.getClass.getSimpleName}]")
   // }
      visitedNodes.add(n)
      for {
        c <- nextNodes(n)
        if !visitedNodes.contains(c)
        if canVisit(c)
      } {
        // ChiselError.info(s"Pushing ${c.name}")
        unvisitedNodes.push(c)
      }

      // if (unvisitedNodes.size < 20)
      //   ChiselError.info(s"Stack(${unvisitedNodes.map(_.name).mkString(", ")})")
      n
    }

    protected def getRoots: Vector[Node]
    protected def canVisit(n: Node): Boolean

    protected def nextNodes(n: Node): Traversable[Node]
    protected def previousNodes(n: Node): Traversable[Node]
    protected def forwardDir: IODirection
    protected def backwardDir: IODirection
  }

  trait ForwardNodeWalk {
    protected def nextNodes(n: Node) = n.consumers.map(
      cons => cons.inputs.find{
        case b: Binding if b.targetNode == n =>
//          ChiselError.info(s"Replacing ${cons.name} with binding ${b.name}")
          true
        case _ => false
      }.getOrElse(cons))
    protected def previousNodes(n: Node) = n.inputs
    protected def forwardDir = INPUT
    protected def backwardDir = OUTPUT
  }

  trait BackwardNodeWalk {
    protected def nextNodes(n: Node) = n.inputs
    protected def previousNodes(n: Node) = n.consumers
    protected def forwardDir = OUTPUT
    protected def backwardDir = INPUT
  }


  abstract private class ModuleNodeWalk(protected val comp: Module) extends NodeWalk {
    override protected def getRoots: Vector[Node] = {

      val inputs = for ((_, w) <- comp.wires if w.dir == forwardDir) yield w
//         ChiselError.info(s"Inputs: ${inputs.map(_.name).mkString(", ")}")
      val childrenWires =
        for (child <- comp.children; (_, w) <- child.wires) yield w
      val childrenOutputs = childrenWires.filter(_.dir == backwardDir)
      val coConsumers = childrenOutputs.flatMap(comp.findBinding)
      val regs = comp.nodes.filter(n => n.isInstanceOf[Reg])
      val mems = comp.nodes.filter(n => n.isInstanceOf[Mem[_]])
      val roms = comp.nodes.filter(n => n.isInstanceOf[ROMData])
      // val registers = comp.nodes.filter(n => n.isInstanceOf[Reg])
      // val memOutput = comp.nodes.filter(n => n.isInstanceOf[MemRead])
      (inputs ++ coConsumers ++ regs ++ mems ++ roms).toVector.
        filter(n => !n.prune && !isLiteral(n))
    }

    override protected def canVisit(n: Node): Boolean = {
      n.componentOf == comp && !n.isInstanceOf[Delay] &&
      !n.prune && 
      previousNodes(n).forall(
        i => visitedNodes.contains(i) || i.name == "reset" ||
          isLiteral(i)) //Only visit a node if all parents are visited or literals
    }
  }

  private object ModuleNodeWalk {
    def forward(comp: Module) = new ModuleNodeWalk(comp) with ForwardNodeWalk
    def backward(comp: Module) = new ModuleNodeWalk(comp) with BackwardNodeWalk
  }

  abstract private class DesignNodeWalk extends ModuleNodeWalk(Driver.topComponent) {
    override protected def getRoots: Vector[Node] = {
      val inputs = for ((_, w) <- comp.wires if w.dir == INPUT) yield w
      val unconnectedWires = Driver.components.flatMap { comp =>
        for ((_, w) <- comp.wires if w.driveRand) yield w
      }
      val seqElems = Driver.components.flatMap { _.nodes.filter {
        case (_: Reg | _: Mem[_] | _: ROMData) => true
        case _ => false
      }}
      ChiselError.info(s"top comp. wires: ${comp.wires.map(_._1).mkString("[", ", ", "]")}")

      (inputs ++ unconnectedWires ++ seqElems).toVector
    }

//     override def next(): Node = {
// //      ChiselError.info(s"Num unvisited: ${unvisitedNodes.size}")
//       val n = super.next()
// //      ChiselError.info(s"Visiting ${n.name} in ${n.componentOf.getPathName(".")}")
//       n
//     }

    override protected def canVisit(n: Node) = {
      previousNodes(n).forall(i => visitedNodes.contains(i) ||
        i.name == "reset" || isLiteral(i))
    }
  }

  private object DesignNodeWalk {
    def forward = new DesignNodeWalk with ForwardNodeWalk
    def backward = new DesignNodeWalk with BackwardNodeWalk
  }

  private object LiteralNodeWalk extends ModuleNodeWalk(Driver.topComponent) with ForwardNodeWalk {
    override protected def getRoots: Vector[Node] = {
      val literals = Driver.components.flatMap {
        comp => comp.nodes.filter(_.isLit)
      }
      literals.toVector
    }

    override protected def canVisit(n: Node) = {
      !n.prune && n.inputs.forall(i => visitedNodes.contains(i) || i.name == "reset")
    }
  }

 //OIOIOI: resolvesToLiteral can gå baklengs, hvilket betyr
          //at vi åpner for å besøke noder HVIS FORELDRE IKKE HAR
          //BLITT BESØKT!
          // eks: 1'h1 -> T0 -> T1
        //Vi kan da besøke T1 - men T0 vil ikke ha blitt besøkt!!!! 


//   private def resolvesToLiteral(n: Node): Boolean = {
// //      n.isLit || (n.inputs.size == 1 && resolvesToLiteral(n.inputs(0)))
//     resolveLiteral(n) != None
//   }

//   //May be worth memoizing this information.
//   @scala.annotation.tailrec
//   private def resolveLiteral(n: Node): Option[Node] = {
//     if (n.isLit) Some(n)
//     else if (n.inputs.size == 1) resolveLiteral(n.inputs(0))
//     else None
//   }

  private trait EimAnalysisStrategy {
    def analyse(n: Node): EimData
  }

  private object SimpleEimAnalysis extends EimAnalysisStrategy {
    def analyse(n: Node): EimData = {
      val children = n.consumers
      val value = n match {
        case r: Reg =>
          r.next.needWidth()
        case _ => (for {
          in <- n.inputs
          if in.componentOf == n.componentOf
        } yield in.needWidth()).sum
      }
      val width = n.needWidth().toDouble
      val weights = (for (c <-children) yield { 
        c -> (width / c.inputs.map(_.needWidth()).sum)
      }).toMap
//      ChiselError.info(weights.map(_._2).mkString(", "))
      val impactFactors = weights
      new EimData(impactFactors, weights, value)
    }
  }


  /*
   This class just contains a sketch of an analysis strategy which
   differentiates between node types when calculating values, weights
   and impact factors. The sketch also contains notes about different
   ways in which the analysis might be differentiated.
   */
  private object NodeSpecificEimAnalysis extends EimAnalysisStrategy {
    def analyse(n: Node): EimData = {
      n match {
        case o: Op if o.inputs.length == 2 => o.op match {
          case "&" | "|" => 
            /* 
             What do we need to estimate for each node?
             - Some notion of the cost of the node, i.e. the energy
             value.
             - The weight with which each input contributes to this
             node's dissipated energy.
             - How highly each input correlates with the
             input, i.e. the impact factor, so that we can estimate to
             what extent using the input in a term in the model will
             alleviate the need to use this particular node.
             
             These values will be guesstimates of some quality. The
             simplest possible solution, I guess, would be:
             - Use the combined bit width of inputs as the energy.
             - Use the fraction of an input's bits to the total input
             bits as both the weight and the impact factor.

             Refinements would take node-specific qualities into
             account. For instance:
             - Multipliers are much more expensive than adders. My
             best guess at cost hierarchy (between operation nodes) is
             indicated by the order in which they are listed here,
             with the least costly node first.
             
             - Nodes can inequally split cost contribution between
             inputs. For instance, the selection line into a MUX will
             be more significant for the energy consumption of the MUX
             itself than will the inputs.

             - The correlation between an input and the output is
             highly specific to each node. Multipliers, for instance,
             often cause large changes in the output given even small
             changes in the input. For a multiplexer, there will be no
             change in the output unless there is a change in the
             selection - however, the amount of change in the output
             is completely independent of the amount of change in the
             selection (instead being perfectly given by the input to
             which the selection switched). I have had several ideas
             about how to approach the impact factor estimation:
               1. Use boolean difference for each input, and calculate
               the fraction of 1s in the truth table of the resulting
               expression.

               2. Use statistical analysis, assuming universal
               probabilities for each input value, to calculate the
               expected difference between amount of change between
               input and output.

               3. Use simulation of the different units under some
               random stimuli, and use regression modelling to somehow
               relate the change in different inputs to the change in
               the output. Set weights accordingly.

               4. Enhance the aforementioned techniques by using the
               distribution of inputs to a node from the training
               benchmark signal data. 

           The take-away from this rambling is that I should code this
           in a way which makes it simple to experiment with different
           implementations of each strategy. Splitting on the nodes,
           and delegating to functions for each metric for each node
           kind, may be the way to go. Unless we want to gather nodes
           in a different way. Maybe a class hierarchy is the thing -
           an EimAnalysis class? Swap implementation by plugging in a
           different class, instead of swapping out the contents of
           this method.

             */
          case "^" =>

          case "+" | "-" => 

          case "==" | "!=" | "<" | "<=" | "s<" => 

          case "<<" | ">>" | "s>>" =>
            if (o.inputs(1).isLit) 0 //do something
            else 0 //do something else

          case "*" | "s*u" => 

          case "/" => "ldiv"

          case x =>
            throw new UnsupportedOperationException(s"EIM NYI: binary op '$x'")
        }
        case o: Op if o.inputs.length == 1 => o.op match {
          case "~" =>

          case "Log2" =>

          case "OHToUInt" => 

          case x => 
            throw new UnsupportedOperationException(s"EIM NYI: unary op '$x'")
        }

        case (_: Extract | _: UInt | _: SInt) =>
          /* Nodes such as extracts and concatenations basically create aliases,
            to which no computation is associated. Should we still
            have an EIM cost related to wire transfers? There is no
            explicit need for more transfers, but splitting and
            joining wires may indicate greater distances from source
            to computation?? I do not think so, but best keep an open
            mind. */

        case m: Mux => "lmux"

        case r@(_: MemRead) if shouldInclude(r.mem) => 
          /* What to do about reads? */
        case r@(_: MemSeqRead) if shouldInclude(r.mem) => 

        case w@(_: MemWrite) if shouldInclude(w.mem) => 
          /* What to do about writes? */


        case m: Mem[_] =>

        case _ =>
          throw new UnsupportedOperationException(
            s"EIM NYI: ${n.getClass.getName} with num inputs ${n.inputs.size}!")
      }
      new EimData(Map.empty, Map.empty, 1)
    }
    private def shouldInclude(m: Mem[_]) = m.isInline
  }

}



//   private type VariableGraph = Vector[Node]
//   // To use a NodeWrapper, where the graph contains only nodes which
//   // have been used in terms, or to use the entire graph and check a
//   // private Set-field for node usage... that is the question. I guess
//   // the second solution is simpler, since we do not have to create a
//   // graph, but it is likely to make the handling of children more
//   // complex (eimValue = childrenFactors * childrenValues does not
//   // work, we must keep track of what the currently last-used parent
//   // is while traversing the graph and add this value to the matrix
//   // once the path is resolved into a )


//   def generateTermsForModule(c: Module): VariableGraph = {
//     val instanceName = c.getPathName(".")
//     ChiselError.info(s"** Generating terms for instance: $instanceName")
//     val inputs = for ((_, w) <- c.wires if w.dir == INPUT) yield w
// //    ChiselError.info(s"Inputs: ${inputs.map(_.name).mkString(", ")}")
//     val childrenWires =
//       for (child <- c.children; (_, w) <- child.wires) yield w
//     val (childrenInputs, childrenOutputs) =
//       childrenWires.partition(_.dir == INPUT)
// //    ChiselError.info(s"Children inputs: ${childrenInputs.map(fullyQualifiedName).mkString(", ")}")
// //    ChiselError.info(s"Children outputs: ${childrenOutputs.map(fullyQualifiedName).mkString(", ")}")


//     val registers = c.nodes.filter(n => n.isInstanceOf[Reg])
// //    ChiselError.info(s"Registers: ${registers.map(_.name).mkString(", ")}")

//     def isEndNode(n: Node): Boolean =
//       childrenInputs.contains(n) || registers.contains(n)

//     val variableGraph =
//       (inputs ++ childrenOutputs ++ registers).toVector
//     //...
//     val terms = generateTermsForGraph(variableGraph)
//     val out = createOutputFile(instanceName + ".terms")
//     out.write(terms.mkString("\n"))
//     out.close()
//     ChiselError.info(s"Completed module $instanceName")
//     variableGraph
//   }

//   case class NodeTerms(expressions: TraversableOnce[String],
//     nodesUsed: TraversableOnce[Node]);

//   def generateTermsForGraph(
//     variableGraph: VariableGraph): mutable.Set[String] = {
//     val terms = mutable.Set[String]()
//     val unvisitedNodes =
//       mutable.Stack[Node](variableGraph.filter(!_.prune): _*)
//     val visitedNodes = mutable.Set[Node]()
//     for (n <- variableGraph) {
//       parents(n) = Set.empty
//       for (p <- n.inputs)
//         parents(p) = Set.empty
//     }
//     while (!unvisitedNodes.isEmpty) {
//       val currentNode = unvisitedNodes.pop()
//       ChiselError.info(s"Visiting ${currentNode.name} [${currentNode.getClass.getSimpleName}]")
//       visitedNodes.add(currentNode)
//       val nextParentSet =
//         if (shouldGenerateTermsForNode(currentNode)) {
// //          val NodeTerms(expressions, nodesUsed) = generateTermsForNode(currentNode)
//           // terms ++= expressions
//           // variablesUsed( ++= nodesUsed.map{_.name}
//           // for (node <- nodesUsed; if isTemporaryNode(node))
//           terms ++= generateTermsForNode(currentNode)
//           Set(currentNode)
//         } else {
//           //The parents contributed to output nodes by this node is
//           //the union of all parents of this node.
//           (Set[Node]() /: currentNode.inputs) { 
//             case (s, i) => if (i.isLit) s else s ++ parents(i)
//           }
//         }
//       for {
//         c <- currentNode.consumers
//         if !visitedNodes.contains(c) //Do not visit already visited nodes: 
//         if c.componentOf == currentNode.componentOf //So does submodule inputs
//         if c.inputs.forall(
//           i => i.isLit || visitedNodes.contains(i)) //Only visit a node if all parents are visited or literals
//       } { 
//         // We should only do this if all parents of c have been
//         // visited! If not, the parent set will be incomplete. 
//           parents(c) = nextParentSet
//           unvisitedNodes.push(c)
//       }

// //      generateTermsForNode(nextNode)
// //      unvisitedNodes.pushAll(nextNode.consumers)
//     }
//     terms
//   }

  // Immediate TODOs:
  // - Do not emit terms including clocks and resets
  // - In general, emit Hamming distance factors for buses and the
  // signal value for single-bit signals.
  // - Determine how the chisel IR actually looks. Are there separate
  // nodes for each wire? For the code A = B + C, do we get
  //        B--\
  //      A --> + --> C
  // or
  //        B--\
  //       A --> C
  // Appeaers to be the last one: each node is named by its output.

//   private def shouldGenerateTermsForNode(node: Node): Boolean = {
//     //    node.name != "" ==> 'The node output is named' (not really
//     // sure how it works for Bits..)  Should we consider whether all
//     // inputs and all outputs are named?  What, then, about
//     // multiplexers, where we would like to detect a change in the
//     // selection signal? This may be a temporary signal. Is it not
//     // more efficient to use this temporary signal, rather than
//     // creating a cross-terms between several other signals?
//     // Maybe we should not filter out any nodes for term generation...

//     // The name of a node refers to its output. If the node is
//     // temporary, then this means that the output is temporary. 

//     !isTemporaryNode(node) || !namedOutputs(node).isEmpty
//     // !isTemporaryNode(node) &&
//     // node.consumers.exists(_.componentOf == node.componentOf) //Should be driving something in this module
// //    true
//   }

//   def generateTermsForNode(node: Node): TraversableOnce[String] = node match {
//     case reg: Reg =>
//       generateRegTerm(reg)

//     case bits: Bits =>
//       generateBitsTerm(bits)

//     case mux: Mux =>
//       generateMuxTerm(mux)

//     case op: Op =>
//       generateOpTerm(op)

//     case extract: Extract =>
//       generateExtractTerm(extract)

//     case mem: Mem[_] =>
//       None

//     case memRead: MemRead =>
//       None

//     case romRead: ROMRead =>
//       None

//     case s: Sprintf =>
//       None

//     case p: PrintfBase =>
//       None

//     case binding: Binding =>
//       None

//     case clock: Clock =>
//       None

//     case p: proc =>
//       None

//     case delay: Delay =>
//       None

//     case memWrite: MemWrite =>
//       None

//     case literal: Literal =>
//       None

//     case romData: ROMData =>
//       None

// //    case a: Assert =>  Assert extends Delay, so not necessary

//     case memSeqRead: MemSeqRead =>
//       None

//     case memReadWrite: MemReadWrite =>
//       None

//   }

//   // We may need this if we are to generate terms which use temporary signals.
//   // override def emitRef(n: Node): String = {
//   //   val r = super.emitRef(n)
//   //   ChiselError.info(s"For node ${r} with name ${n.name}: isNamed = ${n.named}")
//   //   r
//   // }

//   private def generateMuxTerm(mux: Mux): TraversableOnce[String] = {
//     // TODO: 
//     val selection = mux.inputs(0).name
//     if (selection == "reset") Vector.empty
//     else Vector(s"${getFactor(mux)}:change($selection)")
//   }

//   private def generateOpTerm(op: Op): TraversableOnce[String] = {
//     // TODO: Make this HD-call dependent on the width of op?
//     //       Extract this variable-to-model-factor calculation to a separate method, maybe
//     // out.write(getNodeVariable(op))
//     Vector(getFactor(op))
//   }

//   private def generateExtractTerm(extract: Extract): TraversableOnce[String]= {
//     val gotWidth = extract.inputs(0).needWidth()
//     if (gotWidth > 1) {
//       if (extract.inputs.length == 3) {
//         Vector(s"lbits(${extract.inputs(0).name}, ${extract.inputs(1).litValue()}, ${extract.inputs(2).litValue()})")
//       } else {
//         Vector(s"lbit(${extract.inputs(0).name}, ${extract.inputs(1).litValue()})")
//       }
//         // out.write("lbits(
//         // if (extract.inputs.length < 3) {
//         //   if(gotWidth > 1) {

//         //     "  assign " + emitTmp(extract) + " = " + emitRef(extract.inputs(0)) + "[" + emitRef(extract.inputs(1)) + "];\n"
//         //   } else {
//         //     "  assign " + emitTmp(extract) + " = " + emitRef(extract.inputs(0)) + ";\n"
//         //   }
//         // } else {
//         //   if(gotWidth > 1) {
//         //     "  assign " + emitTmp(extract) + " = " + emitRef(extract.inputs(0)) + "[" + emitRef(extract.inputs(1)) + ":" + emitRef(extract.inputs(2)) + "];\n"
//         //   } else {
//         //     "  assign " + emitTmp(extract) + " = " + emitRef(extract.inputs(0)) + ";\n"
//         //   }
//     } else {
//       Vector.empty
//     }
//   }

//   private def generateBitsTerm(bits: Bits): TraversableOnce[String] = {
//     Vector(getFactor(bits))
//   }

//   private def generateRegTerm(reg: Reg): TraversableOnce[String] = {
//     Vector(getFactor(reg))
//   }

//   private def getFactor(node: Node): String = {
//     if (node.needWidth() == 1) node.name
//     else s"HD(${node.name})"
//   }


//   def generateEnergyImpactMatrix(graph: VariableGraph): Unit = {
//     //...
//   }

/*

 - So, how should we proceed?

   What we want to do is generate a list of terms.

   These terms are supposed to be our best guesses for what may best
   describe the energy consumption caused by some part of the RTL
   circuit.

   So far, I have the following points on what terms should be included:
     - All inputs and registers should be included as terms, since
       these denote the roots of our Energy Impact Matrix (EIM).  (Put
       another way: these variables are the most fundamental source of
       change propagating through the circuit in one cycle.)

     - Submodule outputs should also be included, as they also
       represent EIM-roots (especially?) if the submodule is modelled
       separately.

     - Multiplexers are often well represented as
       HD(out):change(selection), since the high fan-in of the output
       makes it represent a large number of other variables. Does that
       make sense? It made sense to me at one point.

     - Intermediate signals may be better suited than EIM roots for
       energy consumption modelling if the EIM tree is either
       imbalanced or deep. Thus, we should emit terms for intermediate
       signals as well. Emitting a moderately large number of terms
       should be okay, as we rely on EIM heuristics to make smart
       selections for fast modelling. What we are supposed to gain
       from this pass is a simplification of variable interactions: if
       terms are created based on how variables interact in the RTL,
       we can improve upon how PrEsto checks all combinations within a
       limited combination space. 

       HOWEVER: we will not want to emit terms for every temporary
       signal. One possible check is whether the nodes in question
       have names (cf Verilog:emitRef check `node.name` != ""). 

       One potential problem is that when we run our elaboratino after
       super.elaborate, the RTL consists of nodes which have
       apparently at most three inputs and two outputs. There are two
       possible solutions:
       - Perform a reverse traversal to find all 'named drivers' of a
         given node (or store this list in each node as the RTL is
         elaborated)
       - Perform the variable interaction analysis at an earlier stage
         using custom transforms. A disadvantage may be that nodes or
         terms are altered by further generic backend transformations,
         resulting in discrepancies between term names we output and
         those created by the Verilog backend (dots are replaced with
         underscores when bundles are flattened?).


     - One potential strategy which should fulfill all the above
       points would be to generate a term for all:
       - Inputs, registers and module outputs.
       - Variable interactions, where all inputs and the output are
         named variables. Generate two terms:
         * product(all inputs), precise description of interaction.
         * HD(output), 'summarizes' the interaction.

   Additionally, I have the following general points:

     - Bus variables should never be included in models as raw
       factors. Typically, we will want to compute their hamming
       distance which represents the amount of change.

     - 
 
   Our implementation strategy should be as follows:
     - We generate output on a per-module basis, as we wish to create
       one term list file for each module. Each module is as such
       almost completely decoupled from others, with the algorithm
       proceeding as follows:
        1. for (c <- Driver.sortedComps)
        2.     generateTermsForModule(c)
       ... where generateTermsForModule is not recursive. This mirrors
       the Verilog backend method doCompile. Or does it?


     - For the output generation for each module, we should consider
       each expression as a potential term as it expresses variable
       interaction. It may be a good idea to traverse the RTL for each
       tree in the EIM forest, since this permits keeping track of the
       set of 'named parents' along each node path. We could
       potentially also build a graph which the EIM generation 'pass'
       could work on afterwards. Cf. Node class documentation in
       Chisel, the node has the field 'consumers' which allows
       forwards traversal (and the field 'inputs' for backwards
       traversal, if desirable). The Node is also suppposed to be the
       generic top-level class in a Composite pattern.


     - We should probably calculate the EIM in the same pass, since
       this will be dependent on our selection of variables used in
       terms. Unlike module term selection, the EIM should consider
       submodules since the submodule inputs can increase the impact
       of their drivers. When each module is completed, the impact
       from its inputs should therefore be stored for access by parent
       modules.
 */
  // We extend the Verilog backend so that:
  // - We can determine, based on module.moduleName, which module
  // instances are equivalent to each other.
  // - A single Chisel invocation is sufficient to generate both
  //  Verilog and modelling input files.  (since module equivalence is
  //  checked based on the generated Verilog, the only way to
  //  implement the ModelTerm backend without Verilog generation is to
  //  create my own scheme for gauging whether module inputs will
  //  yield the same module implementations. Maybe I could use my
  //  graph? But still, I need to follow the Verilog module names for
  //  the output files since this is used for modelling.)
  /* 
   UPDATE: I just realized that the output from primetime is not power
   per module, but power per instance. Thus, during modelling we will
   want to treat seperate instances of the same module differently. In
   some ways, it will therefore be easier for us to merely generate
   terms for each instance which may be read from an
   appropriately-named file. The other option is to maintain the
   current strategy of creating per-module term lists, which saves
   storage. If so, we must find some scheme for mapping instance names
   in primetime to module names. Of course, we could just generate
   another file containing this mapping... but is there any point if
   the C++ simulator name incompatibility forces us to ditch the
   Verilog backend? 

   Simple scheme: emit a file for each module, and for each instance
   create a symbolic link file to the appropriate module. Clumsy or
   nice?

   Another note on naming: the nameIt pass by the target-agnostic
   backend sets the name of all temporary signals. This hampers our
   ability to separate explicitly named signals from temporary signal
   values. How can we avoid this? We can do a traversal before calling
   super.elaborate where we mark which nodes are temporaries. 


   About naming incompatibilities: for simulation speed and generality
   purposes I have decided to use pre-Verilog-backend Chisel RTL
   names. Since the main reason for using the Verilog backend was to
   get module names and temporary signal names, both of which we do
   not need anymore, is there any point in extending the Verilog
   backend? Let us try without, and see what happens!
   
   */


