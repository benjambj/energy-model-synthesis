# Modelling Scripts

This directory contains R scripts used to create energy models. The
subdirectory `fpga-eval/` contains scripts which synthesizes models
using Xilinx Vivado.

The R scripts rely on the following data sources:

  - `test-data/` should contain data frames with per-benchmark
  per-module signal activity and power consumption for the training
  benchmark.
  - `validation-data/` should contain data frames with per-benchmark
  per-module signal activity and power consumption for the validation
  benchmarks.
  - `terminfo/` should contain files describing the hardware design
    being modelled (as described in the `<repo-root>/hdl/` README).

These directories with the data content we used can be created by
extracting the archives in `<repo-root>/data/` into this
directory. **WARNING: Total data size is 66 GiB.**

## R Source Code

The R source code is divided into three files. These files are sourced
by `.Rprofile` when running R in this directory.

### 1. `automatic.modelling.R`

This file contains the majority of the modelling code. Major parts of
the file is legacy (legacy rate is particularly high in the first 1k
lines or so). If using the script to run any of the implemented
models, you will want to look at the following functions (most
parameters omitted):

- `get.genser.results(bm.list)`: generates an energy model using Bachmann's
method.
- `get.svd.results(bm.list)`: generates an energy model using Yang's method.
- `evaluate.hierarchical.model(module,
get.model.results=calculate.presto.results(bm.list))`: generates an
energy model using Sunwoo's method.

The models all accept two parameters (in some way): `bm.list` and
`module`. The `bm.list` is an object containing pointers to benchmark
data locations. Such an object is initialized in the file
`create.modelling.objects.R`, which is sourced by `.Rprofile` when
starting R in this directory.  The `module` parameter describes the
hardware hierarchy. If omitted, the top-level module is used
(derivable from per-module signal lists in the benchmarks). In
addition, the functions can take model-specific parameters.

The two first functions return an evaluation object which contains the
model, execution time information, and validation benchmark evaluation
results. The final function only returns a list of evaluation
results - but all functions cache their results, so the model created
can be retrieved by calling `get.presto.module.results(bm.list)`
directly afterwards. In addition to returning these results, the
functions generate Verilog implementations of the models which are
created.

Other useful functions include:

- `divide.regs.into.flip-flops(signal.list, reg.widths)`: can be used
to separate a data frame with bus values into ones with one column per
bit.

- `plot.evaluation.results()`: generates per-cycle plots of predicted
and actual error. Support averaging values over an interval (which
produces more legible graphs when the cycle count is high).

- `plot.per.cycle.error.histogram()`: plots the distribution of error
values as a histogram.

- `plot.per.cycle.error.comparison()`: distribution of multiple
results in one figure.

- `plot.per.interval.error()`: plots a development of error quantities
over a varying interval size.

- `compare.sample.plot()`: plots per-cycle estimates from multiple
models and the actual value, for visual comparison of model quality.

- `write.model.as.verilog()`: constructs a Verilog implementation of a
model object.

### 2. `evaluation.R`:

Primarily legacy code, but still used to manage benchmark data file
retrieval. 

### 3. `dataconv.R`:

Contains code which implements various calculations on different data
types. Example operations include hamming distance calculation,
left-shift, and bitwise operations such as and, or and xor. These
operations support string operands, and numeric sizes above 32 bits
(up to R numeric integer precision). Be warned that some functions
here only partially work, as some functions were developed to support
code which was not used in the end.

## R Library Dependencies

To run the modelling code, you should first run the following to
ensure that all library dependencies are installed:

```r
install.packages(c("gtools", "stringr", "plyr", "formula.tools", "pryr", "nnls", "ggplot2", "grid"))
```
