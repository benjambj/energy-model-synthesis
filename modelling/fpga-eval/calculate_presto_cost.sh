#!/bin/sh

if [ -z $1 ]; then
    echo "Usage: $0 <presto hierarchy file>" >&2
    exit 1
fi

cat $1 | while read -e module; do
    make summaries/${module}.summary;
done

cat $1 | awk 'BEGIN {OFS="";} {print "summaries/", $0, ".summary"}' | ./sum_presto_costs.py
