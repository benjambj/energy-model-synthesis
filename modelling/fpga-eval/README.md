# FPGA Cost Evaluation

This folder contains scripts to compute the LUT cost of generated
energy models. The setup is as follows:

- Run `make` to build one summary file for each verilog file in the
  parent directory.

- The make target creates a build script and constraints from the
  template files `build.template.tcl` and `constraints.template.tcl`.

- Then, `vivado` is run with the appropriate script using the `build/`
  folder as a working directory.

- The output from vivado is piped into the script
  `./summarize_vivado_output.sh`. This script scrapes information on
  LUT cost, register cost, BRAM cost etc. The output from this script
  is stored as a summary file.
