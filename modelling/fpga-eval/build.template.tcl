## A build template.
## Replace <module name> with the name of the mode, and <clock period> with the clock period

## New project
create_project -in_memory -part xc7v2000tflg1925-1

## Read files
read_verilog -library work change.v
read_verilog -library work HDlut.v
read_verilog -library work HD.v
read_verilog -library work <module name>.v

## Create constraints
read_xdc scripts/constraints-<module name>.tcl

## Synthesize
synth_design -part xc7v2000tflg1925-1 -top <module name> -flatten_hierarchy none
opt_design

## Reports
report_timing_summary -nworst 1
report_utilization
