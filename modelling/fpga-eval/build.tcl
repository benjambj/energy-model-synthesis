## A build template.
## Replace <model name> with the name of the mode, and <clk period> with the clock period

## New project
create_project -in_memory -part xc7v2000tflg1925-1

## Read files
read_verilog -library work <model name>.v

## Create constraints
create_clock -name {clk} -period <clk period> [get_ports {clk}]
set_max_delay -from [get_input_ports] -to [get_ports {o_core_energy}] <clk period>

## Synthesize
synth_design -part xc7v2000tflg1925-1 -top <model name> -flatten_hierarchy none

## Reports
report_timing_summary
report_resource
report_area
