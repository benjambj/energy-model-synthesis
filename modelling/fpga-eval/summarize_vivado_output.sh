#!/bin/sh

awk '/^Slack (.*) :/
     /^\| Slice LUTs/ 
     /^\|   LUT as Logic/
     /^\|   LUT as Memory/ 
     /^\| Slice Registers/ 
     /^\| Block RAM Tile \|/ 
     /^\| DSPs/' 
