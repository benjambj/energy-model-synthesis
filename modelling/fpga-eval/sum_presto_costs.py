#!/usr/bin/env python3

import sys
import operator
import re

class ImplementationCost:
    def __init__(self, 
                 slack1=float('Inf'), 
                 slack2=float('Inf'), 
                 lut_totals=(0, 0, 0, 0), 
                 lut_logic=(0, 0, 0, 0), 
                 lut_mem=(0, 0, 0, 0),
                 slice_regs=(0, 0, 0, 0),
                 bram_tiles=(0, 0, 0, 0), 
                 dsps=(0, 0, 0, 0)):
        self.slack1 = slack1
        self.slack2 = slack2
        self.lut_totals = lut_totals
        self.lut_logic = lut_logic
        self.lut_mem = lut_mem
        self.slice_regs = slice_regs
        self.bram_tiles = bram_tiles
        self.dsps = dsps

    def add_module_cost(self, module_cost):
        lut_count = max(self.lut_totals[2], module_cost.lut_totals[2])
        reg_count = max(self.slice_regs[2], module_cost.slice_regs[2])
        bram_count = max(self.bram_tiles[2], module_cost.bram_tiles[2])
        dsp_count = max(self.dsps[2], module_cost.dsps[2])
        slacks = sorted([self.slack1, self.slack2, module_cost.slack1, module_cost.slack2])
        self.slack1 = slacks[0]
        self.slack2 = slacks[1]
        self.lut_totals = list(map(operator.add, self.lut_totals, module_cost.lut_totals))
        self.lut_logic = list(map(operator.add, self.lut_logic, module_cost.lut_logic))
        self.lut_mem = list(map(operator.add, self.lut_mem, module_cost.lut_mem))
        self.slice_regs = list(map(operator.add, self.slice_regs, module_cost.slice_regs))
        self.bram_tiles = list(map(operator.add, self.bram_tiles, module_cost.bram_tiles))
        self.dsps = list(map(operator.add, self.dsps, module_cost.dsps))
        self.lut_totals[2] = lut_count
        self.lut_logic[2] = lut_count
        self.lut_mem[2] = lut_count
        self.slice_regs[2] = reg_count
        self.bram_tiles[2] = bram_count
        self.dsps[2] = dsp_count

    def __str__(self):
        return """
Slack ({timing_met1}) :   {slack1}ns  (required time - arrival time)
Slack ({timing_met2}) :   {slack2}ns  (required time - arrival time)
| Slice LUTs*             | {lut_totals[0]:7} | {lut_totals[1]:7} | {lut_totals[2]:7} | {lut_totals[3]:7} |
|   LUT as Logic          | {lut_logic[0]:7} | {lut_logic[1]:7} | {lut_logic[2]:7} | {lut_logic[3]:7} |
|   LUT as Memory         | {lut_mem[0]:7} | {lut_mem[1]:7} | {lut_mem[2]:7} | {lut_mem[3]:7} |
| Slice Registers         | {slice_regs[0]:7} | {slice_regs[1]:7} | {slice_regs[2]:7} | {slice_regs[3]:7} |
| Block RAM Tile          | {bram_tiles[0]:7} | {bram_tiles[1]:7} | {bram_tiles[2]:7} | {bram_tiles[3]:7} |
| DSPs                    | {dsps[0]:7} | {dsps[1]:7} | {dsps[2]:7} | {dsps[3]:7} |""".format(
    timing_met1 = "MET" if self.slack1 >= 0 else "VIOLATED", 
    timing_met2 = "MET" if self.slack2 >= 0 else "VIOLATED", 
    **self.__dict__)
    
        
def main():
    if len(sys.argv) != 1:
        print("Usage: cat <modulefile> |", sys.argv[0], file=sys.stderr)
        exit(1)
    
    total_cost = ImplementationCost()
    modules = sys.stdin.readlines()
    for module_summary in modules:
        module_cost = calculate_module_cost(module_summary.strip())
        total_cost.add_module_cost(module_cost)

    print(total_cost)

def calculate_module_cost(module):
    with open(module) as module_summary_file:
        module_summary_lines = module_summary_file.readlines()
        if len(module_summary_lines) != 8:
            if len(module_summary_lines) != 7:
                if len(module_summary_lines) != 6:
                    print("[WARNING]: The module summary file", module, "has", 
                          len(module_summary_lines), "lines, expected 6 or 8!")
                else:
                    # TODO: Some files do not have slack lines, if the models are
                    # constants. They only have six lines. Should handle this..
                    slack1 = float('Inf')
                    slack2 = float('Inf')
                    module_summary_lines = ['', ''] + module_summary_lines
            else:
                slack1 = get_slack(module_summary_lines[0])
                slack2 = float('Inf')
                module_summary_lines = [''] + module_summary_lines
        else:
            slack1 = get_slack(module_summary_lines[0])
            slack2 = get_slack(module_summary_lines[1])

        lut_total = get_row_values(module_summary_lines[2])
        lut_logic = get_row_values(module_summary_lines[3])
        lut_mem = get_row_values(module_summary_lines[4])
        slice_registers = get_row_values(module_summary_lines[5])
        bram_tiles = get_row_values(module_summary_lines[6])
        dsps = get_row_values(module_summary_lines[7])
        return ImplementationCost(slack1, slack2, lut_total, lut_logic, 
                                  lut_mem, slice_registers, bram_tiles,
                                  dsps)
                                  
def get_slack(line):
    slack_p = re.compile(r'^Slack \((VIOLATED|MET)\)\s*:\s+(.+)ns\s+\(required time - arrival time\)$')
    match = re.search(slack_p, line)
    if match:
        return float(match.group(2))
    else:
        print('Error: could not find slack in the following slack line:\n\t"', 
              line, '"', sep="", file=sys.stderr)
        exit(1)


def get_row_values(line):
    row_p = re.compile(r'^\|[^|]*\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|$')
    mr = re.match(row_p, line)
    if mr:
        if mr.group(4).strip().startswith('<'):
            return (int(mr.group(1)), int(mr.group(2)), int(mr.group(3)), 0.0)
        else:
            return (int(mr.group(1)), int(mr.group(2)), int(mr.group(3)), float(mr.group(4)))
    else:
        print('Error: could not find row values in the following row line:\n\t"', 
              line, '"', sep="", file=sys.stderr)
        exit(1)

# - create function get_row_values(line)

if __name__ == '__main__':
    main()
    
    

