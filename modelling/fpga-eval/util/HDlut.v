module HDlut
  (
   input [5:0]            x,
   output reg [2:0] numbits
   );

   always @*
     begin
        case (x) 
          6'd0:
            numbits <= 0;
          6'd1:
            numbits <= 1;
          6'd2:
            numbits <= 1;
          6'd3:
            numbits <= 2;
          6'd4:
            numbits <= 1;
          6'd5:
            numbits <= 2;
          6'd6:
            numbits <= 2;
          6'd7:
            numbits <= 3;
          6'd8:
            numbits <= 1;
          6'd9:
            numbits <= 2;
          6'd10:
            numbits <= 2;
          6'd11:
            numbits <= 3;
          6'd12:
            numbits <= 2;
          6'd13:
            numbits <= 3;
          6'd14:
            numbits <= 3;
          6'd15:
            numbits <= 4;
          6'd16:
            numbits <= 1;
          6'd17:
            numbits <= 2;
          6'd18:
            numbits <= 2;
          6'd19:
            numbits <= 3;
          6'd20:
            numbits <= 2;
          6'd21:
            numbits <= 3;
          6'd22:
            numbits <= 3;
          6'd23:
            numbits <= 4;
          6'd24:
            numbits <= 2;
          6'd25:
            numbits <= 3;
          6'd26:
            numbits <= 3;
          6'd27:
            numbits <= 4;
          6'd28:
            numbits <= 3;
          6'd29:
            numbits <= 4;
          6'd30:
            numbits <= 4;
          6'd31:
            numbits <= 5;
          6'd32:
            numbits <= 1;
          6'd33:
            numbits <= 2;
          6'd34:
            numbits <= 2;
          6'd35:
            numbits <= 3;
          6'd36:
            numbits <= 2;
          6'd37:
            numbits <= 3;
          6'd38:
            numbits <= 3;
          6'd39:
            numbits <= 4;
          6'd40:
            numbits <= 2;
          6'd41:
            numbits <= 3;
          6'd42:
            numbits <= 3;
          6'd43:
            numbits <= 4;
          6'd44:
            numbits <= 3;
          6'd45:
            numbits <= 4;
          6'd46:
            numbits <= 4;
          6'd47:
            numbits <= 5;
          6'd48:
            numbits <= 2;
          6'd49:
            numbits <= 3;
          6'd50:
            numbits <= 3;
          6'd51:
            numbits <= 4;
          6'd52:
            numbits <= 3;
          6'd53:
            numbits <= 4;
          6'd54:
            numbits <= 4;
          6'd55:
            numbits <= 5;
          6'd56:
            numbits <= 3;
          6'd57:
            numbits <= 4;
          6'd58:
            numbits <= 4;
          6'd59:
            numbits <= 5;
          6'd60:
            numbits <= 4;
          6'd61:
            numbits <= 5;
          6'd62:
            numbits <= 5;
          6'd63:
            numbits <= 6;
        endcase // case (x)
     end
endmodule
