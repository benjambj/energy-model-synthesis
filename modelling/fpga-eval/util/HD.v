module cycle_wise_xor
  #(
    parameter WIDTH=32)
  (
    input wire              i_clk,
    input wire              i_rst,
    input wire [WIDTH-1:0]  value,
    output wire [WIDTH-1:0] result
   );

   reg [WIDTH-1:0] prevvalue;
   always @(posedge i_clk or posedge i_rst)
     begin
        if (i_rst)
          prevvalue <= 0;
        else
          prevvalue <= value;
     end

   assign result = value ^ prevvalue;
endmodule   

module HD
  #(
    parameter WIDTH=32,
    parameter HDWIDTH=$clog2(WIDTH + 1),
    parameter N = (WIDTH/6),
    parameter M = (WIDTH/36))
   (
    input wire                i_clk,
    input wire                i_rst,
    input wire [WIDTH-1:0]    value,
    output wire [HDWIDTH-1:0] hd
    );
   
   wire [WIDTH-1:0] diff;
   cycle_wise_xor #(.WIDTH(WIDTH)) xor_and_reg (i_clk, i_rst, value, diff);
   
//`define USE_ADDER_TREE
`ifdef USE_ADDER_TREE
   genvar           i, j;

   

   wire [2:0]       hds[0:N-1];
   wire [HDWIDTH-1:0] sums[0:N-1];

   HDlut LUT1(.x(diff[5:0]), .numbits(hds[0]));
   generate
      for (i = 1; i < N; i = i + 1) begin
         HDlut loopLUT (.x(diff[(6*(i+1) - 1):(6*i)]), .numbits(hds[i]));
      end
   endgenerate
      
   generate
      for (i = 0; i < N/2; i = i + 1) begin
         assign sums[i + (N+1)/2] = hds[2*i] + hds[2*i + 1];
      end

      if (N/2 < (N+1) / 2) begin
        if (N*6 < WIDTH) begin
           wire [2:0] tophd;
           wire [5:0] topdiff;
           assign topdiff = 6'd0 | diff[WIDTH-1:N*6];
           HDlut topLUT(.x(topdiff), .numbits(tophd));
           assign sums[N/2] = hds[N-1] + tophd;
        end else begin
           assign sums[N/2] = hds[N-1];
        end
         assign sums[N/2 - 1] = sums[N-1] + sums[N-2];
      end else begin // if (N/2 < (N+1) / 2)
        if (N*6 < WIDTH) begin
           wire [2:0] tophd;
           wire [5:0] topdiff;
           assign topdiff = 6'd0 | diff[WIDTH-1:N*6];
           HDlut topLUT(.x(topdiff), .numbits(tophd));
           assign sums[N/2-1] = sums[N-1]+ tophd;
        end else begin
           assign sums[N/2-1] = sums[N-1];
        end
      end

      for (i = 0; i < N/2 - 1; i = i + 1) begin
         assign sums[i] = sums[2*i+1] + sums[2*(i + 1)];
      end
   endgenerate

   assign hd = sums[0];

`else
   generate
      if (M == 0)
        begin
           wire [5:0] hw;
           HW_36 singleHW (.bus(36'd0 | diff), .weight(hw));
           assign hd = hw[HDWIDTH-1:0];
        end
      else
        begin
           genvar           i, j;

           wire [5:0]       hws[0:M-1];
           wire [HDWIDTH-1:0] sums[0:N-1];

           
           for (i = 0; i < M; i = i + 1) begin
              HW_36 loopHW (.bus(diff[(36*(i+1) - 1):(36*i)]), .weight(hws[i]));
           end

           for (i = 0; i < M/2; i = i + 1) begin
              assign sums[i + (M+1)/2] = hws[2*i] + hws[2*i + 1];
           end

           if (M/2 < (M+1) / 2) begin
              if (M*36 < WIDTH) begin
                 wire [5:0] tophw;
                 wire [35:0] topdiff;
                 assign topdiff = 36'd0 | diff[WIDTH-1:M*36];
                 HW_36 topWeight(.bus(topdiff), .weight(tophw));
                 assign sums[M/2] = hws[M-1] + tophw;
              end else begin
                 assign sums[M/2] = hws[M-1];
              end
              assign sums[M/2 - 1] = sums[M-1] + sums[M-2];
           end else begin // if (N/2 < (N+1) / 2)
              if (M*36 < WIDTH) begin
                 wire [5:0] tophw;
                 wire [35:0] topdiff;
                 assign topdiff = 36'd0 | diff[WIDTH-1:M*36];
                 HW_36 topWeight(.bus(topdiff), .weight(tophw));
                 assign sums[M/2-1] = sums[M-1]+ tophw;
              end else begin
                 assign sums[M/2-1] = sums[M-1];
              end
           end

           for (i = 0; i < M/2 - 1; i = i + 1) begin
              assign sums[i] = sums[2*i+1] + sums[2*(i + 1)];
           end

           assign hd = sums[0];
        end // else: !if(M == 0)
   endgenerate
`endif

endmodule




module HW_36
  (
   input [35:0] bus,
   output [5:0] weight
   );

   wire [5:0]   layer1_input  [0:5];
   wire [2:0]   layer1_result [0:5];
   wire [5:0] layer2_input [0:2];
   wire [2:0] layer2_result [0:2];
   genvar       i;
   generate
      for (i = 0; i < 6; i = i + 1)
        begin
           assign layer1_input[i] = bus[6*(i+1)-1:6*i];
           HDlut layer1lut ( .x(layer1_input[i]), .numbits(layer1_result[i]));
           assign layer2_input[0][i] = layer1_result[i][0];
           assign layer2_input[1][i] = layer1_result[i][1];
           assign layer2_input[2][i] = layer1_result[i][2];
        end
   endgenerate

   generate
      for (i = 0; i < 3; i = i + 1)
        begin
           HDlut layer2lut ( .x(layer2_input[i]), .numbits(layer2_result[i]));
        end
   endgenerate

   wire [4:0] first_adder_result = {layer2_result[2], 1'b0} + layer2_result[1];
   wire [4:0] second_adder_result = first_adder_result + layer2_result[0][2:1];
   assign weight = { second_adder_result, layer2_result[0][0]};
endmodule // HW_36

/* -----\/----- EXCLUDED -----\/-----
module HW_8
  (
   input [7:0] bus,
   output [3:0] weight
   );

   wire [2:0]   first_lut_result;
   HDlut ( .x(bus[5:0], .numbits(first_lut_result)));

   always @*
     case ({bus[7:6], first_lut_result}) 
       {2'd0, 3'b???}: 
         weight <= first_lut_result;
       {2'd1, 3'd0}:
         weight <= 4'd1;
       {2'd1, 3'd1}:
         weight <= 4'd2;
       {2'd1, 3'd2}:
       
       
       
   endcase // case ({bus[7:6], first_lut_result})
   
   case ({bus[7:6], first_lut_result})
 -----/\----- EXCLUDED -----/\----- */
     
