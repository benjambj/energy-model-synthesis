module change 
  #(
    parameter WIDTH=32)
   (           
               input wire             i_clk,
               input wire             i_rst, 
               input wire [WIDTH-1:0] a,
               output wire            z
               );

   reg [WIDTH-1:0]                    prev;
   always @(posedge i_clk or posedge i_rst)
     begin
        if (i_rst)
          prev <= 0;
        else
          prev <= a;
     end   

   assign z = a != prev;

endmodule // change
