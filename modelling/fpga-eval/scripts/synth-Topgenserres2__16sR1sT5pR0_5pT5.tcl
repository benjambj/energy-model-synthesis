## A build template.
## Replace Topgenserres2__16sR1sT5pR0_5pT5 with the name of the mode, and 10 with the clock period

## New project
create_project -in_memory -part xc7v2000tflg1925-1

## Read files
read_verilog -library work Topgenserres2__16sR1sT5pR0_5pT5.v

## Create constraints
read_xdc scripts/constraints-Topgenserres2__16sR1sT5pR0_5pT5.tcl
#create_clock -name {clk} -period 10 [get_ports {clk}]
#set_max_delay -from [get_input_ports] -to [get_ports {o_core_energy}] 10

## Synthesize
synth_design -part xc7v2000tflg1925-1 -top Topgenserres2__16sR1sT5pR0_5pT5 -flatten_hierarchy none

## Reports
report_timing_summary
report_resource
report_area
