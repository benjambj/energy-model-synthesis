## A build template.
## Replace Topgenserres2__16sR1sT5pR0_4pT5 with the name of the mode, and 10 with the clock period

## New project
create_project -in_memory -part xc7v2000tflg1925-1

## Read files
read_verilog -library work Topgenserres2__16sR1sT5pR0_4pT5.v

## Create constraints
read_xdc scripts/constraints-Topgenserres2__16sR1sT5pR0_4pT5.tcl

## Synthesize
synth_design -part xc7v2000tflg1925-1 -top Topgenserres2__16sR1sT5pR0_4pT5 -flatten_hierarchy none
opt_design

## Reports
report_timing_summary -nworst 1
report_utilization
