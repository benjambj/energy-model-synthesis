# Signal and Power Data

This directory contains compressed tar files with signal and power
data. There are three tar files:

- `test-data.tar.xz` contains the activity and power data used to
  train the model. Should be extracted into the
  `<repo-root>/modelling/` directory (produces the folder test-data).

- `validation-data.tar.xz` contains the activity and power data used to
  validate the accuracy of models which are created from the training
  data.

- `terminfo.tar.xz` contains information used to generate terms during
  modelling (see the README in `<repo-root>/hdl/` for an explanation).

**WARNING: The amount of data is substantial.** The decompressed file
  sizes are as follows:

- test-data.tar: 2 GiB
- validation-data.tar: 64 GiB
- terminfo.tar: 2.9 MiB

The tar files' contents should be extracted in the
`<repo-root>/modelling` directory.
