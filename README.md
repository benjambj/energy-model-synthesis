# Energy Model Synthesis Framework

This repository contains the source code framework and data used to
produce the results reported in the paper "A Systematic Approach to
Automated Construction of Power Emulation Models", presented at the
DATE 2016 conference. The first commit, tagged "date16", contains code
in the state it was in when data was generated for this article. The
repository requires 1.4 GiB of disk space with compressed data.

For any questions, contact benjambj@idi.ntnu.no.

## Disclaimers

1. No guarantees are given concerning the state of the code and data
   in this repository. Download and use the repository contents at
   your own risk.

2. The source code and data folders have been copied
   exactly[*](#markdown-header-footnotes) as they were at the point
   when the results in the aforementioned article were
   generated. Being a snapshot of a developing project, the repository
   is somewhat chaotic:

    - Code may be found in illogical locations.

    - There is bad and at times inconsistent naming for
      concepts/variables. Particular feints to be wary of include "test
      data" which should have been called "training data", and "term
      info" which should have been called "module info".

    - There is an abundance of unnecessary resources from development
      dead-ends, generated files, and project dependencies.

  The READMEs in this repository have been written in an attempt to
  mitigate these horrors, pointing out what useful bits you may in
  general expect to find in various folders. However, be warned that
  even the README files may be unintentionally lying.

  I am intending to perform a crude clean-up in a separate branch, to
  get rid of the bits which are most obviously unnecessary. This
  activity may break the framework in unforeseen ways. If you check
  out the (atm hypothetical) clean-up branch, be on your guard. 

## Repository Overview

The repository is organized into folders as follows:

- `hdl/`: Contains HDL source, analysis backend and benchmark activity generator.

- `regression-benchmarks/`: Contains the benchmark sources, and
  Makefile targets for generating signal activity (VCD) files and
  converting one kind of power data file to another (the binary format
  FSDB to text format OUT).

- `asic-synthesis/`: Contains synthesis scripts targetting Synopsys
  DesignCompiler, which create a netlist of the HDL design and
  calculate power consumption using the netlist and supplied activity
  files.

- `conv-tools/`: Tools used to transform signal and data files into a
  R data frame files.

- `modelling/`: R scripts used to generate power models,
  Verilog-implementations of the power models, and scripts using
  Xilinx Vivado to estimate hardware cost of the models.

- `data/`: Pre-generated signal and power data frames.


All following file references will be given relative to the repository
root, unless otherwise noted.

## Framework flow

### If Using Supplied Data Files

If you are using our generated data for experiments, only the `data/`
and `modelling/` folders are of interest. To get started experimenting
with modelling:

1. Extract the `data/*.tar.xz` files into the `modelling/`
   directory. **WARNING**: Complete data size is approximately 66 GiB.

2. Change directory to `modelling`, and launch R. The file
   `modelling/.Rprofile` will create two objects:
    - `bm.list`, which is a handle to all benchmark activity.
    - `top`, which is a handle to the HDL design hierarchy.

3. Install any missing R libraries, and start experimenting! For more
   information on the R functions, see the README in `modelling/` and
   comments in `modelling/automatic.modelling.R`.

### If Using a Custom HDL Design

If you are using a different HDL design, you need to generate new
activity and power data files. Proceed as follows:

1. Develop training benchmarks. Also develop a set of validation
   benchmarks if you are researching modelling methods. 
2. Generate VCD files from the benchmarks. If your design is written
   in Chisel, the emulator-based sampling support in the file
   `hdl/csrc/emulator.cc` may be of use. Example uses of this emulator
   can be found in `regression-benchmarks/Makefile`.
3. Synthesize your design. The scripts in the `asic-synthesis/` may
   serve as a good starting point if you are using the Synopsys
   toolchain.
4. Calculate power data corresponding to the VCD files generated in
   point 2 above. Again, if using Synopsys PrimeTime the scripts in
   `asic-synthesis/` may be useful.
5. If using any existing modelling methods, or if your design contains
   on-chip memory resources you do not want to account for with your
   energy models, generate design description files as those in
   `data/terminfo.tar.xz`. Not all files are needed for all modelling
   methods, so you may wish to do this on a by-need basis (revisit
   this step if you get errors such as "file not found: terminfo/**"
   from the R code). If your design is written in Chisel, the backend
   in `hdl/chisel/src/main/scala/ModelTerm.scala` may be
   useful. Example uses of this backend can be found in
   `hdl/Makefrag`, target `terms`.
6. Transform the activity and power data files into a format suitable
   for the R modelling scripts using the conversion utilities in the
   `conv-tools/` directory. You can select signal subsets
   automatically if you generated terminfo/<module>.deps files from
   the previous point. Similarly, module subsets for which power data
   is collected can be automatically determined based on which
   terminfo/<module>.deps files exist. Finally, on-chip memory power
   consumption can be automatically subtracted based on
   terminfo/<module>.mem files.

Having created the required data frames, you can follow the steps in
the [previous section](#markdown-header-if-using-supplied-data-files)
to begin modelling.



## Footnotes

Some modifications have been made, with potentially unknown
consequences to the functioning of the system. In particular:

1. References to server names and server-specific configuration files
   have been removed. They have been replaced by environment
   variables, which are explained in the `<repo-root>/env-setup.sh`
   file.

2. Data files have been archived and compressed due to the large
   amount of data (66 GiB). The compressed archives are located in the
   `data/` directory, although the files within them used to be
   located in the `modelling/` directory.

3. Some files (the benchmarks and modelling data) used to be symbolic
   links. I have instead included copies of the files in this
   repository.
