## This file contains configuration environment variables. The
## variables should be set to appropriate values, and the script
## sourced (i.e. run `. ./env-setup.sh`) before using framework code.
## 
## (Note: the use this script has not actually been tested
## (configurations used to be hard-coded strings in various
## Makefiles), so some debugging may be required).
##
## The file should be sourced on all relevant development machines
## (could be one machine or several, e.g. HDL/benchmark development
## servers EDA tool servers, and R modelling servers).


## -----------------------------------------
## Variables used by conv-tools/
## ----------------------------------------
# The directory in which your VCD files, OUT files and terminfo files
# are stored.
export DATADIR= 

## -----------------------------------------
## Variables used by asic-synthesis/
## -----------------------------------------
# The directory which contains your cell library.
export LIBDIR=

# The names of target library and symbolic library files in your cell
# library. Template included below: replace "MISSING_*_NAME" below
# with your values.
export TARGET_LIB=${LIBDIR}/MISSING_TARGET_LIB_NAME.db
export SYMBOL_LIB=${LIBDIR}/MISSING_SYMBOL_LIB_NAME.sdb

# Your Synopsys toolchain install location.
export SYNOPSYS_DIR=

# The name of the Synopsys DesignWare synthetic library file. Default
# value set.
export SYNTHETIC_LIB=${SYNOPSYS_DIR}/libraries/syn/dw_foundation.sldb

# The location of your HDL synthesis project directory. Could for
# instance be the location of the <repo-root>.
export PROJECTDIR=

# The location of the Verilog source used to synthesize the test
# design. Default value set, based on the entire repository being
# uploaded to the EDA tool server.
export SRCDIR=${PROJECTDIR}/hdl/vsim/generated-src

# The location of ASIC synthesis scripts. Default set.
export SCRIPTDIR=${PROJECTDIR}/asic-synthesis/scripts

## -----------------------------------------
## Variables used by regression-benchmarks/
## -----------------------------------------

# Installation directory of Synopsys PrimeTime.
export PRIMETIME_DIR=

# Directory containing PrimeTime support binaries, such as
# fsdb2ns. Default value set.
export PT_SUPPORT_DIR=${PRIMETIME_DIR}/eteam/platform/LINUX/bin
