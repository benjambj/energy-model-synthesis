name := "regression-utilities"

scalacOptions ++= Seq("-deprecation", "-feature")

scalaVersion := "2.11.6"

unmanagedSourceDirectories in Compile += baseDirectory.value / "util"
