# Regression Benchmark Data Generation

This folder contains a Makefile with targets for generating signal and
power data from benchmarks, using the emulator in `<repo-root>/hdl/`
or the Synopsys PrimeTime scripts in `<repo-root>/asic-synthesis/`,
respectively. It also contains the benchmarks used to generate
validation data for modelling.

(Note: all references to 'snapshots' are relics).

The subfolder `validation/` contains the source for the
[MiBench](https://github.com/embecosm/mibench) and
[MachSuite](https://github.com/breagen/MachSuite) benchmarks.

The archive `results.tar.xz` contains the VCD files generated from
these benchmarks. Decompressed size is 6.4 GiB.

## Dependencies

To generate VCD files, one typically needs three files:

- `bin/bmark/<binary>`. This file is the benchmark binary (compiled
  using a RISC-V toolchain). Often a symlink to the actual binary (the
  Makefile has a target for generating these by building the
  appropriate benchmark under `validation/`, and creating a symlink).

- `hex/pk.hex`: The RISC-V project includes a program called a proxy
  kernel, which allows libc-dependent benchmarks to run using newlib
  (the proxy kernel implements the system calls newlib is dependent
  on). The PK must be compiled (using RISC-V toolchain) and a .hex
  dump file must be created. This file is included under `hex/pk.hex`
  in this repository. The sources are not included, however, so if you
  need to rebuild it you must clone the
  [proxy kernel repository](https://github.com/riscv/riscv-pk).

- `<repo-root>/hdl/emulator/emulator-DefaultCPPConfig-debug`: Activity
  is gathered using a Chisel-generated C++ emulator for the Rocket
  design. This emulator can be built by running `make debug -C
  <repo-root>/hdl/emulator`

## Makefile targets

### To be run on development machine:

- `results/%_vcd`: Generates validation benchmark VCD files (see
  [Dependencies](#markdown-header-dependencies)). The list of
  validation benchmarks is hard-coded in the Makefile, along with
  execution parameters such as inputs, fast-forward time, periods
  etc. The target is a directory (e.g. `aes_vcd`), into which all
  sampled VCD files are stored (see the README in `<repo-root>/hdl/`
  for details on the emulator sampling support).

- `bmarks`: Generates all specified validation benchmark binaries (see
  [Dependencies](#markdown-header-dependencies)).

- `results/training.vcd`: Builds the training benchmark VCD file. The
  reason we need a different target for this is that the training
  benchmark runs bare-metal, while the others runs under the RISC-V
  "proxy kernel" (pk).

### To be run on ASIC synthesis machine:

Before running any of these targets, upload all the VCD files you want
to generate power data for along with this Makefile to the EDA tool
machine (Synopsys PrimeTime, the design netlist and other synthesis
artefacts are required).

- `ios.tcl`: Depends on all VCD files. Creates a file which tells the
  power calculation script what VCD files exist, and what to call the
  corresponding power output file.

- `training_ios.tcl`: Similar purpose to the above, but a separate
  file is used for the training benchmark.

- `bmark_fsdbs`: Runs primetime for all validation benchmarks (new
  Makefile knowledge makes me believe that this target may re-run the
  script needlessly; it may be better to select a single FSDB file and
  make that instead (e.g. `make results/aes_fsdb`))

- `train_fsdb`: Creates power file for the training benchmark.

- `out`: Converts all FSDB (binary) power files to OUT (text) power
  files, to enable further processing by the tools in
  `<repo-root>/conv-tools`.
