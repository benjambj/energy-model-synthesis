FFT strided:
==============================
"main() entry" at cycle 350486
In-file: /home/benjambj/skole/phd/papers/energy-model-synthesis/work/regression-benchmarks/validation/MachSuite/fft/strided/input.data
Read 32768 bytes
"input read, bm begin" at cycle 649045
"bm complete" at cycle 2114577
"output written" at cycle 2267844
"check read" at cycle 2434493
Success.

(BM length in cycles: 1465532)


FFT:
==============================
"main() entry" at cycle 398986
In-file: /home/benjambj/skole/phd/papers/energy-model-synthesis/work/regression-benchmarks/validation/MachSuite/fft/transpose/input.data
Read 8192 bytes
"input read, bm begin" at cycle 592256
"bm complete" at cycle 2343853
"output written" at cycle 2398444
"check read" at cycle 2455567
Success.

(BM length in cycles: 1751597)

BULK:
==============================
"main() entry" at cycle 341858
In-file: /home/benjambj/skole/phd/papers/energy-model-synthesis/work/regression-benchmarks/validation/MachSuite/bfs/bulk/input.data
Read 37208 bytes
"input read, bm begin" at cycle 663685
"bm complete" at cycle 869000
"output written" at cycle 1039797
"check read" at cycle 1230743
Success.

(BM length in cycles: 205315)

QUEUE:
==============================
"main() entry" at cycle 350461
In-file: /home/benjambj/skole/phd/papers/energy-model-synthesis/work/regression-benchmarks/validation/MachSuite/bfs/queue/input.data
Read 37208 bytes
"input read, bm begin" at cycle 661971
"bm complete" at cycle 903556
"output written" at cycle 1074148
"check read" at cycle 1263644
Success.

(BM length in cycles: 241585)

AES:
==============================
"main() entry" at cycle 350033
In-file: /home/benjambj/skole/phd/papers/energy-model-synthesis/work/regression-benchmarks/validation/MachSuite/aes/aes/input.data
Read 144 bytes
"input read, bm begin" at cycle 525776
"bm complete" at cycle 574347
"output written" at cycle 588783
"check read" at cycle 605554
Success.

(BM length in cycles: 48571)

STENCIL3D:
==============================
"main() entry" at cycle 341936
In-file: /home/benjambj/skole/phd/papers/energy-model-synthesis/work/regression-benchmarks/validation/MachSuite/stencil/stencil3d/input.data
Read 131080 bytes
"input read, bm begin" at cycle 1072375
"bm complete" at cycle 5640409
"output written" at cycle 6179459
"check read" at cycle 6772237
Success.

(BM length in cycles: 4568034)
