#!/usr/bin/zsh

list_state_snapshots() {
    ls -v state_sample_snapshot* 2>/dev/null | tr '\n' ' '
}

state_snapshots=(`list_state_snapshots`);
if [ ${#state_snapshots} -eq 0 ]; then
    program_args=${@[4,-1]}
    echo "program args: ${program_args}"
    if [ -z ${program_args} ]; then
        echo "A program invocation is required to generate samples" >&2
        echo "Usage: $0 [sample period=50000] [pk hex file=pk.hex] [snapshot dir=snapshots] [program invocation...]" >&2
        exit 1
    fi


    sample_period=${1:-50000}
    echo "Gathering samples periodically with period ${sample_period}"
    spike -m512 -s${sample_period} pk ${program_args}
    state_snapshots=`list_state_snapshots`;
else
    echo "Using existing state samples"
fi

prev_mem_file=${2:-"pk.hex"}
snapshot_dir=${3:-"snapshots"}
mkdir -p ${snapshot_dir}

for snapshot in ${=state_snapshots}; do
    num=${snapshot##*_}
    mem_out_file=${prev_mem_file%_*}_${num}
    cp ${prev_mem_file} ${mem_out_file}
    reg_out_file=init_regs_${num}
    `dirname $0`/create_total_state_snapshot \
        ${mem_out_file} ${snapshot} ${reg_out_file}
    prev_mem_file=${mem_out_file}
done
