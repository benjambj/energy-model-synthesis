import scala.sys

import java.io.{PrintStream => Writer, File}
import scala.io.Source

class VcdFile(in: Source) extends Iterator[String] {
  private val lines = in.getLines

  override def hasNext = lines.hasNext
  override def next = lines.next
}

sealed trait VcdFilter {
  protected var bytesWritten: Long = 0
  protected var inHeader: Boolean = true
  protected var currentLine: String = ""
  protected var currentTime: Long = 0
  protected var includeHeader: Boolean = true

  import scala.collection.mutable
  protected val filters: mutable.ArrayBuffer[() => Boolean] = mutable.ArrayBuffer.empty
  protected def outsideTime(startTime: Option[Long]=None,
    endTime: Option[Long]=None): () => Boolean = { () =>
    val bottom = startTime.getOrElse(currentTime)
    val top = endTime.getOrElse(currentTime)
    currentTime < bottom || currentTime > top
  }

  protected def sizeLimitTickGranularity(limit: Long): () => Boolean = {
    var sizeLimitExceededTick: Option[Long] = None
      () => sizeLimitExceededTick match {
      case Some(t) => currentTime > t
      case None =>
        if (bytesWritten > limit) {
          sizeLimitExceededTick = Some(currentTime)
        }
        false
    }
  }

  final def apply(in: VcdFile, out: Writer): Unit = {
    try {
      if (includeHeader) {
        val oldFilters = filters.clone
        filters.clear
        while (in.hasNext && inHeader) {
          processNextLine(in, out)
        }

        filters += (() => currentLine != "$dumpvars")
        while (in.hasNext && currentLine != "$dumpvars")
          processNextLine(in, out)
        filters.clear
        filters += (() => currentLine == "$end")
        while (in.hasNext && currentLine != "$end")
          processNextLine(in, out)
        writeLine(out, "$end\n")

        filters.clear
        filters ++= oldFilters
      }
      while (in.hasNext) {
        processNextLine(in, out)
      }
    } catch {
      case _: java.io.EOFException =>
    }
  }

  private def processNextLine(in: VcdFile, out: Writer): Unit = {
    currentLine = in.next
    if (currentLine(0) == '#')
      currentTime = currentLine.substring(1).toLong

    if (filters.forall(! _())) {
      writeLine(out, s"${currentLine}\n")
    }

    inHeader = inHeader && !currentLine.startsWith("$enddefinitions")
  }

  private def writeLine(out: Writer, string: String): Unit = {
    val bytes = string.getBytes
    bytesWritten += bytes.size
    out.write(bytes)
  }
}

object VcdFilter {
  def apply(startTime: Long, maxSize: Long, args: Array[String]): VcdFilter = {
    if (args.isEmpty) new ContinuousFilter(startTime, maxSize)
    else {
      val samplePeriod = args(0).toLong
      val sampleLength = args(1).toLong
      new PeriodicFilter(startTime, maxSize, samplePeriod, sampleLength)
    } 
  }

  final private class ContinuousFilter(startTime: Long, maxSize: Long) extends VcdFilter {
    filters += outsideTime(startTime=Some(startTime))
    filters += sizeLimitTickGranularity(maxSize)
  }

  final private class PeriodicFilter(startTime: Long, maxSize: Long,
    samplePeriod: Long, sampleLength: Long) extends VcdFilter {
    filters += outsideTime(startTime=Some(startTime))
    filters += sizeLimitTickGranularity(maxSize)
    filters += outsideCurrentPeriod _

    private var currentPeriodStart = startTime
    private var currentPeriodEnd = startTime + sampleLength

    private def outsideCurrentPeriod: Boolean = {
      if (currentTime > currentPeriodEnd) {
        currentPeriodStart += samplePeriod
        currentPeriodEnd = currentPeriodStart + sampleLength
      }
      !inHeader && (currentTime < currentPeriodStart || currentTime > currentPeriodEnd)
    }
  }
}

object VcdFilterApp extends App {
  type Reader = Source

  val filter = parseArguments
  val out = Console.out
  val vcdFile = new VcdFile(Source.stdin)

  filter(vcdFile, out)

  def parseArguments: VcdFilter = {
    if (args.length != 2 && args.length != 4) {
      printUsage
      sys.exit(1)
    }
    val startTime = args(0).toLong
    val maxSize = args(1).toLong
    VcdFilter(startTime, maxSize, args.drop(2))
  }

  def printUsage: Unit = {
    Console.err.println("Usage: scala VcdFilter <start time> <max output size> [<sample period> <sample length>]")
  }

  // private def writeVcdHeader(in: Reader, out: Writer): Unit = {
  //   for (line <- nextTick(in) ++ nextTick(in))
  //     out.write(s"${line}\n")
  // }
}
