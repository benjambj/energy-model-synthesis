#include <array>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>

struct MemoryUpdate
{
  uint64_t start, size;
  std::unique_ptr<char> memory;
};

class Snapshot
{
 public:
  explicit Snapshot(const std::string &snapshotFile);
  void DumpRegisterState(const std::string &regstateFile);
  bool HasMoreMemoryUpdates() const { return hasMoreMemoryUpdates_; }
  MemoryUpdate GetNextMemoryUpdate();
 private:
  void ReadRegisters();
  void ReadNextMemoryUpdate();
  void ParseMemoryRange(const std::string &rangeStr);

  struct RegisterState
  {
    std::array<uint64_t, 32> xptr, fptr;
    struct CSR {
      uint64_t epc,
        badvaddr,
        evec,
        ptbr,
        pcr_k0,
        pcr_k1,
        cause,
        tohost,
        fromhost,
        count,
        load_reservation;

      uint32_t compare,
        sr,
        fflags,
        frm;
    } csr;
    uint64_t pc;
  } regState_;
  std::ifstream in_;

  bool hasMoreMemoryUpdates_;
  uint64_t currentRangeStart_, currentRangeSize_;
};

class MemoryState
{
 public:
  explicit MemoryState(const std::string &stateFile);
  void ApplyMemoryUpdate(Snapshot &snapshot);
 private:
  std::ofstream out_;
};

static void WriteToHexFile(std::ostream &out, char *bytes,
                           uint64_t size);

int main(int argc, char *argv[])
{
  if (argc != 4)
  {
    std::cerr << "Usage: "
              << argv[0]
              << "<memory state file> <snapshot> <register init file>"
              << std::endl;
    std::exit(EXIT_FAILURE);
  }
  
  auto memstateFile = argv[1];
  auto snapshotFile = argv[2];
  auto regstateFile = argv[3];

  std::cout << "Transforming memory state file " << memstateFile
            << " and creating register init file " << regstateFile
            << " using snapshot " << snapshotFile << std::endl;

  MemoryState memstate{memstateFile};
  Snapshot snapshot{snapshotFile};
  memstate.ApplyMemoryUpdate(snapshot);
  snapshot.DumpRegisterState(regstateFile);

  return 0;
}

Snapshot::Snapshot(const std::string &snapshotFile)
    : in_{snapshotFile}
{
  if (!in_)
    throw std::runtime_error{
      "Could not open snapshot file " + snapshotFile};

  ReadRegisters();
  ReadNextMemoryUpdate();
}

void Snapshot::ReadRegisters()
{
  in_.read(reinterpret_cast<char*>(regState_.xptr.data()),
           regState_.xptr.size() * sizeof(*regState_.xptr.data()));
  in_.read(reinterpret_cast<char*>(regState_.fptr.data()),
           regState_.fptr.size() * sizeof(*regState_.fptr.data()));
  in_.read(reinterpret_cast<char*>(&regState_.csr.epc), sizeof(regState_.csr.epc));
  in_.read(reinterpret_cast<char*>(&regState_.csr.badvaddr), sizeof(regState_.csr.badvaddr));
  in_.read(reinterpret_cast<char*>(&regState_.csr.evec), sizeof(regState_.csr.evec));
  in_.read(reinterpret_cast<char*>(&regState_.csr.ptbr), sizeof(regState_.csr.ptbr));
  in_.read(reinterpret_cast<char*>(&regState_.csr.pcr_k0), sizeof(regState_.csr.pcr_k0));
  in_.read(reinterpret_cast<char*>(&regState_.csr.pcr_k1), sizeof(regState_.csr.pcr_k1));
  in_.read(reinterpret_cast<char*>(&regState_.csr.cause), sizeof(regState_.csr.cause));
  in_.read(reinterpret_cast<char*>(&regState_.csr.tohost), sizeof(regState_.csr.tohost));
  in_.read(reinterpret_cast<char*>(&regState_.csr.fromhost), sizeof(regState_.csr.fromhost));
  in_.read(reinterpret_cast<char*>(&regState_.csr.count), sizeof(regState_.csr.count));
  in_.read(reinterpret_cast<char*>(&regState_.csr.compare), sizeof(regState_.csr.compare));
  in_.read(reinterpret_cast<char*>(&regState_.csr.sr), sizeof(regState_.csr.sr));
  in_.read(reinterpret_cast<char*>(&regState_.csr.fflags), sizeof(regState_.csr.fflags));
  in_.read(reinterpret_cast<char*>(&regState_.csr.frm), sizeof(regState_.csr.frm));
  in_.read(reinterpret_cast<char*>(&regState_.csr.load_reservation), sizeof(regState_.csr.load_reservation));
  in_.read(reinterpret_cast<char*>(&regState_.pc), sizeof(regState_.pc)); 
}

void Snapshot::ReadNextMemoryUpdate()
{
  std::string memoryRangeStr;
  hasMoreMemoryUpdates_ = !!getline(in_, memoryRangeStr);
  if (hasMoreMemoryUpdates_) {
    ParseMemoryRange(memoryRangeStr);
  }
}

void Snapshot::ParseMemoryRange(const std::string &rangeStr)
{
  currentRangeStart_ = stoull(rangeStr.substr(1));
  currentRangeSize_ = stoull(rangeStr.substr(rangeStr.find(',') + 1));
}

MemoryUpdate Snapshot::GetNextMemoryUpdate()
{
  auto buf = std::make_unique<char>(currentRangeSize_);
  in_.read(buf.get(), currentRangeSize_);
  MemoryUpdate update { currentRangeStart_, currentRangeSize_,  std::move(buf)};
  ReadNextMemoryUpdate();
  return update;
}

void Snapshot::DumpRegisterState(const std::string &regstateFile)
{
  std::ofstream out{regstateFile};
  if (!out)
    throw std::runtime_error{
      "Could not open register state output file " + regstateFile};

  out.write(reinterpret_cast<char*>(regState_.xptr.data()),
            regState_.xptr.size() * sizeof(*regState_.xptr.data()));
  out.write(reinterpret_cast<char*>(regState_.fptr.data()),
            regState_.fptr.size() * sizeof(*regState_.fptr.data()));
  out.write(reinterpret_cast<char*>(&regState_.csr.epc), sizeof(regState_.csr.epc));
  out.write(reinterpret_cast<char*>(&regState_.csr.badvaddr), sizeof(regState_.csr.badvaddr));
  out.write(reinterpret_cast<char*>(&regState_.csr.evec), sizeof(regState_.csr.evec));
  out.write(reinterpret_cast<char*>(&regState_.csr.ptbr), sizeof(regState_.csr.ptbr));
  out.write(reinterpret_cast<char*>(&regState_.csr.pcr_k0), sizeof(regState_.csr.pcr_k0));
  out.write(reinterpret_cast<char*>(&regState_.csr.pcr_k1), sizeof(regState_.csr.pcr_k1));
  out.write(reinterpret_cast<char*>(&regState_.csr.cause), sizeof(regState_.csr.cause));
  out.write(reinterpret_cast<char*>(&regState_.csr.tohost), sizeof(regState_.csr.tohost));
  out.write(reinterpret_cast<char*>(&regState_.csr.fromhost), sizeof(regState_.csr.fromhost));
  out.write(reinterpret_cast<char*>(&regState_.csr.count), sizeof(regState_.csr.count));
  out.write(reinterpret_cast<char*>(&regState_.csr.compare), sizeof(regState_.csr.compare));
  out.write(reinterpret_cast<char*>(&regState_.csr.sr), sizeof(regState_.csr.sr));
  out.write(reinterpret_cast<char*>(&regState_.csr.fflags), sizeof(regState_.csr.fflags));
  out.write(reinterpret_cast<char*>(&regState_.csr.frm), sizeof(regState_.csr.frm));
  out.write(reinterpret_cast<char*>(&regState_.csr.load_reservation), sizeof(regState_.csr.load_reservation));
  out.write(reinterpret_cast<char*>(&regState_.pc), sizeof(regState_.pc)); 
}

MemoryState::MemoryState(const std::string &memoryStateFile) 
    : out_{memoryStateFile}
{
  if (!out_)
    throw std::runtime_error{
      "Could not open memory state file " + memoryStateFile};
}

void MemoryState::ApplyMemoryUpdate(Snapshot &snapshot)
{
  while (snapshot.HasMoreMemoryUpdates()) {
    auto update = snapshot.GetNextMemoryUpdate();
    if (!out_.seekp(update.start))
      throw std::runtime_error{
        "Could not seek to location " + std::to_string(update.start)};
    WriteToHexFile(out_, update.memory.get(), update.size);
  }
}

static inline bool IsHexfileAligned(uint64_t n, size_t b)
{
  return ((n+1) % 33 != 0) && ((n - (n/33)) & ~((2*b) - 1)) == 0;
}

static void WriteHexByte(std::ostream &out, char byte)
{
  static char hexchars[] = {
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
  };
  out.put(hexchars[byte & 0xf]);
  out.put(hexchars[(byte >> 4) & 0xf]);
}

static void WriteHexLine(std::ostream &out, char *line)
{
  assert(IsHexfileAligned(out.tellp(), 16));
  for (auto i = 0; i < 16; ++i)
  {
    WriteHexByte(out, line[i]);
  }
  out.seekp(1, std::ios_base::cur);
}

void WriteToHexFile(std::ostream &out, char *bytes, uint64_t size)
{
  size_t pos = out.tellp();
  auto i = 0u;
  assert(IsHexfileAligned(pos, 1));

  //First partial block
  for (; IsHexfileAligned(pos + i, 16); i++)
    WriteHexByte(out, bytes[i]);
  out.seekp(1, std::ios_base::cur);

  //Aligned blocks
  auto numAlignedRowsRemaining = (size - i) / 16;
  for (; numAlignedRowsRemaining > 0; --numAlignedRowsRemaining, i += 16)
    WriteHexLine(out, &bytes[i]);

  out.seekp(1, std::ios_base::cur);
  //Last partial block
  for (; i < size; ++i)
    WriteHexByte(out, bytes[i]);
}
